let ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  getJadwalMapelDetail()
})

function getJadwalMapelDetail(){
  $.ajax({
    async: true,
    url: `${JADWAL_MAPEL_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal medapatkan detail jadwal mata pelajaran!")
    },
    success: function(res) {
      const jadwalMapel = res.data
      renderJadwalMapelDetail(jadwalMapel) 
    }
  })
}

function renderJadwalMapelDetail(data){
  $form = $('#jadwal-mapel-form')
  
  $form.find( "input[name='semester']" ).val(data.semester)
  $form.find( "input[name='kelas']" ).val(data.kelas)
  $form.find( "input[name='mapel']" ).val(data.mapel)
  $form.find( "input[name='hari']" ).val(data.hari)
  $form.find( "input[name='awal']" ).val(data.awal)
  $form.find( "input[name='akhir']" ).val(data.akhir)
}
