const ID_ALIAS  = window.location.pathname.split("/").pop()
let SELECTED_ID
let FILE_NAME, FILE_TYPE, FILE_SIZE

$(document).ready(function(){
    console.log("LOAD SCRIPT")
    get_file_kat()
    getSiswaDetail()

    let siswa_file_table = $('#siswa-file-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,   
      ajax: {
        async: true,
        url: `${SISWA_FILE_API_URL}/by-siswa/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "file_kat",
            orderable: false
          },
          { 
            data: "file_name",
            orderable: false,
            render: function (data, type, row, meta) {
              return `<a href="${WEB_URL}/assets/document/siswa/${data}" target="_blank">${data}</a>`
            }
          },
          { 
            data: "file_type",
            orderable: false
          },
          { 
            data: "file_size",
            orderable: false,
            render: function (data, type, row, meta) {
              return formatBytes(parseInt(data))
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-primary siswa-file-edit-toggle" title="update"  data-id="${data}" data-toggle="modal" data-target="#siswa-file-edit-modal">
                <i class="fa fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger siswa-file-delete-toggle" title="delete" data-id="${data}" data-toggle="modal" data-target="#siswa-file-delete-modal">
                <i class="fa fa-times"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
    });

    $('#kelasis-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,   
      ajax: {
        async: true,
        url: `${KELASIS_API_URL}/list/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "kelas",
            orderable: false
          },
          { 
            data: "tahun_ajaran",
            orderable: false,
          }
      ]
    });

    $('#kamsis-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,   
      ajax: {
        async: true,
        url: `${KAMSIS_API_URL}/list/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "kamar",
            orderable: false
          },
          { 
            data: "tahun_ajaran",
            orderable: false,
          }
      ]
    });

    $("body").delegate(".siswa-file-edit-toggle", "click", function() {
      SELECTED_ID = $(this).data('id')
      get_siswa_file_detail()
    })
    $("body").delegate(".siswa-file-delete-toggle", "click", function() {
      SELECTED_ID = $(this).data('id')
    })

    $('#siswa-document-field').change(function(e) {
      let file = e.target.files[0];
      upload_document(file)
    });

    $('#siswa-file-delete-button').click(function (){
      startLoadingButton('#siswa-file-delete-button')
      
      $.ajax({
          async: true,
          url: `${SISWA_FILE_API_URL}/delete/${SELECTED_ID}`,
          type: 'DELETE',
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#siswa-file-delete-button', 'Iya')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#siswa-file-delete-button', 'Iya')
            $('#siswa-file-delete-modal').modal('toggle')
            siswa_file_table.ajax.reload()
          }
      });
    })

    $('#siswa-file-edit-form').submit(function (e){
      e.preventDefault()
      startLoadingButton('#siswa-file-edit-button')
  
      const request = {
              id: SELECTED_ID,
              id_file: $('#siswa-file-kat-option').find(":selected").val(),
              id_siswa: ID_ALIAS,
              file_name: FILE_NAME,
              file_size: FILE_SIZE,
              file_type: FILE_TYPE
            }
      
      $.ajax({
          async: true,
          url: `${SISWA_FILE_API_URL}`,
          type: 'PUT',
          data: JSON.stringify(request),
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#siswa-file-edit-button', 'Simpan')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#siswa-file-edit-button', 'Simpan')
            $('#siswa-file-edit-modal').modal('toggle')
            siswa_file_table.ajax.reload()
          }
      });
    })
})

function getSiswaDetail(){
    $.ajax({
      async: true,
      url: `${SISWA_API_URL}/by-id/${ID_ALIAS}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else showError("Gagal mendapatkan data siswa!")
      },
      success: function(res) {
        const siswa = res.data
        renderSiswaDetail(siswa) 
      }
    })
}

function get_siswa_file_detail(){
  $.ajax({
    async: true,
    url: `${SISWA_FILE_API_URL}/by-id/${SELECTED_ID}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      renderSiswaFileDetail(res.data)
    }
  });
}

function get_file_kat(){
  $.ajax({
    async: true,
    url: `${FILE_KAT_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const file_kat = res.data
      render_file_kat_option(file_kat) 
    }
  })
}

function upload_document(file){
  $(`#siswa-file-edit-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('document', file);
  
  $.ajax({
      async: true,
      url: `${SISWA_FILE_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload dokumen", "Gagal melakukan upload dokumen!")
          $(`#siswa-file-edit-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response  = res.data
        FILE_NAME       = response.file_name
        FILE_TYPE       = response.file_type
        FILE_SIZE       = response.file_size
        $(`#siswa-file-edit-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}
  
function renderSiswaDetail(data){
  let siswa   = data.siswa,
      ortu    = data.ortu,
      riwayat = data.riwayat,
      info    = data.info

  let $siswaform      = $("#siswa-form"),
      $ortuform       = $("#ortu-form"),
      $riwayatform    = $("#riwayat-form"),
      $infoform       = $("#info-form")

      if(siswa.foto){
        $('#siswa-foto').attr("src", siswa.full_foto)
      }

      //siswa form
      $siswaform.find( "input[name='siswa-nis']" ).val(siswa.nis)
      $siswaform.find( "input[name='siswa-nisn']" ).val(siswa.nisn)
      $siswaform.find( "input[name='siswa-nama']" ).val(siswa.nama)
      $siswaform.find( "input[name='siswa-panggilan']" ).val(siswa.panggilan)
      $siswaform.find( "input[name='siswa-kelamin']" ).val(siswa.kelamin ? siswa.kelamin == 'L' ? 'Laki-laki' : 'Perempuan' : '-')
      $siswaform.find( "input[name='siswa-tmplhr']" ).val(siswa.tmplhr)
      $siswaform.find( "input[name='siswa-tgllhr']" ).val(siswa.tgllhr)
      $siswaform.find( "input[name='siswa-agama']" ).val(siswa.agama)
      $siswaform.find( "input[name='siswa-statdk']" ).val(siswa.statdk)
      $siswaform.find( "input[name='siswa-anakke']" ).val(siswa.anakke)
      $siswaform.find( "input[name='siswa-jmlsdr']" ).val(siswa.jmlsdr)
      $siswaform.find( "textarea[name='siswa-alamat']" ).val(siswa.alamat)
      $siswaform.find( "input[name='siswa-kel']" ).val(siswa.kel)
      $siswaform.find( "input[name='siswa-kec']" ).val(siswa.kec)
      $siswaform.find( "input[name='siswa-kokab']" ).val(siswa.kokab)
      $siswaform.find( "input[name='siswa-prov']" ).val(siswa.prov)
      $siswaform.find( "input[name='siswa-nva']" ).val(siswa.nva)
      $siswaform.find( "input[name='siswa-hp']" ).val(siswa.hp)
      $siswaform.find( "input[name='siswa-email']" ).val(siswa.email)
      $siswaform.find( "textarea[name='siswa-ket']" ).val(siswa.ket)
      //ortu form
      $ortuform.find( "input[name='ortu-ayah_nama']" ).val(ortu.ayah_nama)
      $ortuform.find( "input[name='ortu-ayah_pekerjaan']" ).val(ortu.ayah_pekerjaan)
      $ortuform.find( "input[name='ortu-ayah_hp']" ).val(ortu.ayah_hp)
      $ortuform.find( "input[name='ortu-ibu_nama']" ).val(ortu.ibu_nama)
      $ortuform.find( "input[name='ortu-ibu_pekerjaan']" ).val(ortu.ibu_pekerjaan)
      $ortuform.find( "input[name='ortu-ibu_hp']" ).val(ortu.ibu_hp)
      $ortuform.find( "input[name='ortu-penghasilan']" ).val(ortu.penghasilan)
      $ortuform.find( "input[name='ortu-email']" ).val(ortu.email)
      $ortuform.find( "textarea[name='ortu-alamat']" ).val(ortu.alamat)
      $ortuform.find( "input[name='ortu-wali_nama']" ).val(ortu.wali_nama)
      $ortuform.find( "input[name='ortu-wali_pekerjaan']" ).val(ortu.wali_pekerjaan)
      $ortuform.find( "input[name='ortu-wali_hp']" ).val(ortu.wali_hp)
      $ortuform.find( "input[name='ortu-wali_alamat']" ).val(ortu.wali_alamat)
      $ortuform.find( "input[name='ortu-wali_penghasilan']" ).val(ortu.wali_penghasilan)
      $ortuform.find( "input[name='ortu-wali_email']" ).val(ortu.wali_email)
      //riwayat form
      $riwayatform.find( "input[name='riwayat-tgl_tes']" ).val(riwayat.tgl_tes)
      $riwayatform.find( "input[name='riwayat-asal_sekolah']" ).val(riwayat.asal_sekolah)
      $riwayatform.find( "input[name='riwayat-tahun_lulus']" ).val(riwayat.tahun_lulus)
      $riwayatform.find( "input[name='riwayat-internal_bp']" ).val(riwayat.internal_bp)
      $riwayatform.find( "input[name='riwayat-cabang_bp']" ).val(riwayat.cabang_bp)
      $riwayatform.find( "input[name='riwayat-komp_ak']" ).val(riwayat.komp_ak)
      $riwayatform.find( "input[name='riwayat-komp_nonak']" ).val(riwayat.komp_nonak)
      $riwayatform.find( "input[name='riwayat-komp_mengaji']" ).val(riwayat.komp_mengaji)
      $riwayatform.find( "input[name='riwayat-komp_tularab']" ).val(riwayat.komp_tularab)
      $riwayatform.find( "input[name='riwayat-hafalan']" ).val(riwayat.hafalan)
      //info form
      $infoform.find( "input[name='info-tgl_masuk']" ).val(info.tgl_masuk)
      $infoform.find( "input[name='info-ke_kelas']" ).val(info.ke_kelas)
      $infoform.find( "input[name='info-info_bpibs']" ).val(info.info_bpibs)
      $infoform.find( "textarea[name='info-alasan_ortu']" ).val(info.alasan_ortu)
      $infoform.find( "textarea[name='info-alasan_siswa']" ).val(info.alasan_siswa)
      $infoform.find( "textarea[name='info-keterangan']" ).val(info.keterangan)

}

function renderSiswaFileDetail(file){
  // const $form = $('#siswa-file-edit-form')
  $('#siswa-file-kat-option').val(file.id_file).change()
  FILE_NAME = file.file_name
  FILE_TYPE = file.file_type
  FILE_SIZE = file.file_size
  $(`#siswa-document-field`).dropify()
  let drEvent = $(`#siswa-document-field`).data('dropify')
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.settings.defaultFile = file.full_url;
  drEvent.destroy();
  drEvent.init();
  // $(`#siswa-document-field`).resetPreview()
  // $(`#siswa-document-field`).setFile(file.full_url)
}

function render_file_kat_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id}">${item.file}</option>`
    options_html += option_html
  }

  $('#siswa-file-kat-option').html(options_html)
}