let KAMAR_ID,
    AJARAN = [],
    ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  console.log("LOAD SCRIPT")
  if ($('.kamar-ajaran-option').length > 0) {
    get_ajaran()
  }

  if(!isNaN(ID_ALIAS)){
    get_kamar_detail()
  }

  //DATATABLE
  let kamar_table = $('#kamar-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${KAMAR_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        newObj.id_ajaran = ID_AJARAN_GLOBAL
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "thn_ajaran",
          orderable: false
        },
        { 
          data: "kamar",
          orderable: false
        },
        { 
          data: "tipe",
          orderable: false
        },
        { 
          data: "wali",
          orderable: false
        },
        {
          data: "createdt",
          orderable: false
        },
        {
          data: "id_kamar",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let button = `<a class="btn btn-sm btn-primary" href="${WEB_URL}kamar/update/${data}" title="ubah">
              <i class="fa fa-edit"></i>
            </a>`
            
            return button
          },
          orderable: false
        }
    ]
  });


  //ACTION
  $('#refresh-table').click(function(){
    kamar_table.ajax.reload()
  })

  //FORM
  $('#kamar-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#kamar-create-button')
    
    let $form = $(this),
        request = {
          kamar: $form.find( "input[name='kamar']" ).val(),
          tipe: $('#kamar-create-tipe').find(":selected").val(),
          id_ajaran: $('#kamar-create-ajaran').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: KAMAR_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kamar-create-button', 'Create')
        },
        success: function(res) {
          showSuccess(`Membuat data kamar baru berhasil!`)
          endLoadingButton('#kamar-create-button', 'Create')
          $('#kamar-create-modal').modal('hide')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);
        }
    });
  })
  
  $('#kamar-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#kamar-update-button')

    let $form = $(this),
        request = {
          id_kamar: KAMAR_ID,
          kamar: $form.find( "input[name='kamar']" ).val(),
          tipe: $('#kamar-update-tipe').find(":selected").val(),
          id_ajaran: $('#kamar-update-ajaran').find(":selected").val()
        }

    $.ajax({
        async: true,
        url: KAMAR_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kamar-update-button', 'Update')
        },
        success: function(res) {
          showSuccess(`Mengubah data kamar berhasil!`)
          endLoadingButton('#kamar-update-button', 'Update')
          $('#kamar-update-modal').modal('toggle')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);        
        }
    });
  })
})

function get_ajaran(){
  $.ajax({
    async: true,
    url: `${AJARAN_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_ajaran_option(res.data) 
    }
  })
}

function get_kamar_detail(){
  $.ajax({
    async: true,
    url: `${KAMAR_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kamar   = res.data
      KAMAR_ID      = kamar.id_kamar
      render_kamar_detail(kamar) 
    }
  })
}

function render_kamar_detail(data){
  let $form = $(`#kamar-update-form`)
  $form.find( "input[name='kamar']" ).val(data.kamar)
  $(`#kamar-update-tipe`).val(data.tipe).change()
  $(`#kamar-update-ajaran`).val(data.id_ajaran).change()
}

function render_ajaran_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_ajaran}">${item.thn_ajaran}</option>`
    options_html += option_html
  }

  $('.kamar-ajaran-option').html(options_html)
}