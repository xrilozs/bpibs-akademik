$(document).ready(function(){
  console.log("LOAD SCRIPT")

  get_kelas()
  get_semester()

  $('#calendar-search-form').submit(function(e){
    e.preventDefault()

    startLoadingButton('#calendar-search-button')

    let request = {
          id_ajaran: ID_AJARAN_GLOBAL,
          id_semester: $('#filter-semester-option').find(":selected").val(),
          id_kelas: $('#filter-kelas-option').find(":selected").val(),
        }
        
    $.ajax({
        async: true,
        url: `${JADWAL_MAPEL_API_URL}/calendar`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: request,
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            endLoadingButton('#calendar-search-button', "Cari")
            $('#calendar-alert').show()
            $('#calendar-section').hide()
          }
        },
        success: function(res) {
          console.log(res.data)
          endLoadingButton('#calendar-search-button', "Cari")
          $('#calendar-alert').hide()
          $('#calendar-section').show()
          render_calendar(res.data)
        }
    });
  })
})

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function get_semester(){
  $.ajax({
    async: true,
    url: `${SEMESTER_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const semester = res.data
      render_semester_option(semester) 
    }
  })
}

function render_kelas_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('#filter-kelas-option').html(options_html)
}

function render_semester_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_semester}">${item.semester}</option>`
    options_html += option_html
  }

  $('#filter-semester-option').html(options_html)
}

function render_calendar(data){
  if(data.senin.length > 0){
    let senin_html = ``
    data.senin.forEach((item, id) => {
      const lastId = id == (data.senin.length - 1) ? "mb-1" : 'mb-3'
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-primary text-white ${lastId}">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      senin_html += card
    });
    $('#calendar-monday').html(senin_html)
  }else{
    $('#calendar-monday').html('')
  }
  if(data.selasa.length > 0){
    let selasa_html = ``
    data.selasa.forEach(item => {
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-info text-white">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      selasa_html += card
    });
    $('#calendar-tuesday').html(selasa_html)
  }else{
    $('#calendar-tuesday').html('')
  }
  if(data.rabu.length > 0){
    let rabu_html = ``
    data.rabu.forEach(item => {
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-warning text-white">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      rabu_html += card
    });
    $('#calendar-wednesday').html(rabu_html)
  }else{
    $('#calendar-wednesday').html('')
  }
  if(data.kamis.length > 0){
    let kamis_html = ``
    data.kamis.forEach(item => {
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-danger text-white">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      kamis_html += card
    });
    $('#calendar-thursday').html(kamis_html)
  }else{
    $('#calendar-thursday').html('')
  }
  if(data.jumat.length > 0){
    let jumat_html = ``
    data.jumat.forEach(item => {
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-dark text-white">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      jumat_html += card
    });
    $('#calendar-friday').html(jumat_html)
  }else{
    $('#calendar-friday').html('')
  }
  if(data.sabtu.length > 0){
    let sabtu_html = ``
    data.sabtu.forEach(item => {
      let card = `<div class="col-lg-12">
        <div class="card text-center bg-warning text-white">
          <p class="pt-2 px-1"><b>${item.mapel}</b></p>
          <p>${item.awal} - ${item.akhir}</p>
        </div>
      </div>`
      sabtu_html += card
    });
    $('#calendar-saturday').html(sabtu_html)
  }else{
    $('#calendar-saturday').html('')
  }

  if(data.senin.length <= 0 && data.selasa.length <= 0 && data.rabu.length <= 0 && data.kamis.length <= 0 && data.jumat.length <= 0 && data.sabtu.length <= 0){
    $('#calendar-alert').show()
    $('#calendar-section').hide()
  }
}