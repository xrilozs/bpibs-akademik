let GURU_ID

$(document).ready(function(){
  console.log("LOAD SCRIPT")
  get_guru_edu_lvl()
  get_guru_fam_title()

  //DATATABLE
  let guru_table = $('#guru-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${GURU_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "nid",
          orderable: false
        },
        { 
          data: "npp",
          orderable: false
        },
        { 
          data: "nama",
          orderable: false
        },
        { 
          data: "kelamin",
          render: function (data, type, row, meta) {
            return data.toUpperCase() == 'L' ? 'Laki-laki' : 'Perempuan'
          },
          orderable: false
        },
        { 
          data: "email",
          orderable: false
        },
        { 
          data: "hp",
          orderable: false
        },
        {
          data: "tgljoin",
          orderable: false
        },
        {
          data: "id",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let actionList = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}guru/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>
            <a class="dropdown-item bg-secondary text-white" href="${WEB_URL}guru/detail/${data}">
              <i class="fa fa-search"></i> Detail
            </a>
            <a href="#" class="dropdown-item bg-danger text-white guru-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#guru-delete-modal">
              <i class="fa fa-times"></i> Hapus
            </a>
            <a href="#" class="dropdown-item bg-success text-white guru-add-fam-toggle" data-id="${data}" data-toggle="modal" data-target="#guru-add-fam-modal">
              <i class="fa fa-user"></i> Tambah data keluarga
            </a>
            <a href="#" class="dropdown-item bg-info text-white guru-add-edu-toggle" data-id="${data}" data-toggle="modal" data-target="#guru-add-edu-modal">
              <i class="fa fa-user"></i> Tambah data edukasi
            </a>`

            let button = `<div class="dropdown dropleft">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                Action
              </button>
              <div class="dropdown-menu py-0">
                ${actionList}
              </div>
            </div>`
            
            return button
          },
          orderable: false
        }
    ]
  });

  //ACTION
  $('#refresh-table').click(function(){
    guru_table.ajax.reload()
  })

  $("body").delegate(".guru-delete-toggle", "click", function() {
    GURU_ID = $(this).data('id')
  })
  
  $("body").delegate(".guru-add-fam-toggle", "click", function() {
    clearFamForm()
    GURU_ID = $(this).data('id')
  })
  
  $("body").delegate(".guru-add-edu-toggle", "click", function() {
    clearEduForm()
    GURU_ID = $(this).data('id')
  })

  $('#guru-delete-button').click(function (){
    startLoadingButton('#guru-delete-button')
    
    $.ajax({
        async: true,
        url: `${GURU_API_URL}/delete/${GURU_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#guru-delete-button', 'Yes')
        },
        success: function(res) {
          showSuccess(`Hapus data guru berhasil!`)
          endLoadingButton('#guru-delete-button', 'Yes')
          $('#guru-delete-modal').modal('toggle')
          guru_table.ajax.reload()
        }
    });
  })
  
  $('#guru-add-edu-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#guru-add-edu-button')

    const $form = $(this),
          request = {
            idguru: GURU_ID,
            idlvl: $('#guru-edu-lvl-option').find(":selected").val(),
            sekolah: $form.find( "input[name='sekolah']" ).val(),
            jurusan: $form.find( "input[name='jurusan']" ).val(),
            thnlulus: $form.find( "input[name='thnlulus']" ).val(),
          }
    
    $.ajax({
        async: true,
        url: `${GURU_EDU_API_URL}`,
        type: 'POST',
        data: JSON.stringify(request),
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#guru-add-edu-button', 'Submit')
        },
        success: function(res) {
          showSuccess(`Tambah data edukasi berhasil!`)
          endLoadingButton('#guru-add-edu-button', 'Submit')
          $('#guru-add-edu-modal').modal('toggle')
          guru_table.ajax.reload()
        }
    });
  })

  $('#guru-add-fam-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#guru-add-fam-button')

    const $form = $(this),
          request = {
            idguru: GURU_ID,
            idfam: $('#guru-fam-title-option').find(":selected").val(),
            f_name: $form.find( "input[name='f_name']" ).val(),
            f_dob: convertToDBDateFormat($form.find( "input[name='f_dob']" ).val()),
            f_edu: $form.find( "input[name='f_edu']" ).val(),
            f_ethnic: $form.find( "input[name='f_ethnic']" ).val(),
            f_job: $form.find( "input[name='f_job']" ).val(),
            f_phone: $form.find( "input[name='f_phone']" ).val(),
          }
    
    $.ajax({
        async: true,
        url: `${GURU_FAM_API_URL}`,
        type: 'POST',
        data: JSON.stringify(request),
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#guru-add-fam-button', 'Tambah')
        },
        success: function(res) {
          showSuccess(`Tambah data keluarga berhasil!`)
          endLoadingButton('#guru-add-fam-button', 'Tambah')
          $('#guru-add-fam-modal').modal('toggle')
          guru_table.ajax.reload()
        }
    });
  })
})

function get_guru_edu_lvl(){
  $.ajax({
    async: true,
    url: `${GURU_EDU_LVL_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_guru_edu_lvl_option(res.data) 
    }
  })
}

function get_guru_fam_title(){
  $.ajax({
    async: true,
    url: `${GURU_FAM_TITLE_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_guru_fam_title_option(res.data) 
    }
  })
}

function render_guru_edu_lvl_option(options){
  let options_html = ``
  options.forEach(option => {
      let option_html = `<option value="${option.id}">${option.level}</option>`
      options_html += option_html
  });

  $('#guru-edu-lvl-option').html(options_html)
}

function render_guru_fam_title_option(options){
  let options_html = ``
  options.forEach(option => {
      let option_html = `<option value="${option.id}">${option.hubungan}</option>`
      options_html += option_html
  });

  $('#guru-fam-title-option').html(options_html)
}

function clearFamForm(){
  $("#guru-add-fam-form")[0].reset()
}

function clearEduForm(){
  $("#guru-add-edu-form")[0].reset()
}