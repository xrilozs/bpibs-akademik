let DATA_KELAS = []

$(document).ready(function(){
    get_total_siswa()
    get_total_siswa_per_gender()
    get_total_siswa_per_kelas()

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // Get the ID of the shown tab
        var tabId = $(e.target).attr('href');
        console.log("TAB ID: ", tabId)

        // Check if it's one of your chart tabs
        if (tabId.startsWith('#kelas')) {
            // Extract the item name from the tab ID
            var itemName = tabId.replace('#kelas-', '');
            console.log("ITEM NAME: ", itemName)
            // Find the corresponding chart data
            var chartData = DATA_KELAS.find(item => item.name === itemName);
            console.log("CHART DATA", chartData)

            // If chart data is found, initialize the chart
            if (chartData) {
                initializeChart(chartData);
            }
        }
    });
})

function get_total_siswa(){
    $.ajax({
      async: true,
      url: `${REPORT_SISWA_API_URL}/total?id_ajaran=${ID_AJARAN_GLOBAL}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        const total_siswa = res.data
        render_total_siswa(total_siswa) 
      }
    })
}

function get_total_siswa_per_gender(){
    $.ajax({
      async: true,
      url: `${REPORT_SISWA_API_URL}/total-per-gender?id_ajaran=${ID_AJARAN_GLOBAL}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        const total_siswa_per_gender = res.data
        render_total_siswa_per_gender(total_siswa_per_gender) 
      }
    })
}

function get_total_siswa_per_kelas(){
    $.ajax({
      async: true,
      url: `${REPORT_SISWA_API_URL}/total-per-kelas?id_ajaran=${ID_AJARAN_GLOBAL}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        const total_siswa_per_kelas = res.data
        DATA_KELAS                  = res.data
        render_total_siswa_per_kelas(total_siswa_per_kelas) 
      }
    })
}

function render_total_siswa(data){
    $('#total-siswa').html(data)
}

function render_total_siswa_per_gender(data){
    $('#total-siswa-male').html(data.total_male)
    $('#total-siswa-female').html(data.total_female)

    const dom           = $("#echart-total-siswa-gender-chart")[0]
    let doughnutChart   = echarts.init(dom);

    let option = {        
        legend: {
            orient: 'vertical',
            x: 'left',
            data:['Ikhwan','Akhwat']
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b}: {c} ({d}%)',
        },
        series: [
            {
                name:'Total Siswa',
                type:'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '20',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data:[
                    {value:data.total_male, name:'Ikhwan', itemStyle: {color: '#ffc323',}},
                    {value:data.total_female, name:'Akhwat', itemStyle: {color: '#ff758e',}},
                ]
            }
        ]
    };

    doughnutChart.setOption(option, true);

    $(window).on('resize', function(){
        doughnutChart.resize();
    });
}

function render_total_siswa_per_kelas(data){
    data.forEach((item, i) => {
        let list = item.list
        let list_html = ``

        if (i == 0) {
            initializeChart(item);
        }

        list.forEach(item2 => {
            let item_html = `<div class="col-12">
            <div class="card mb-1">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="fa fa-2x fa-${item2.gender == 'Ikhwan' ? 'male' : 'female'} text-col-blue"></i>
                                </div>
                                <div class="number float-right text-right">
                                    <h6>Total Siswa Kelas ${item2.name}</h6>
                                    <span class="font700">${item2.total}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>`
            list_html += item_html
        });

        $(`#total-siswa-kelas-${item.name}`).html(item.total)
        $(`#list-total-siswa-kelas-${item.name}`).html(list_html)

    });
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function initializeChart(item){
    let list = item.list
    let list_map_name = list.map(x => x.name)
    let list_map_data = list.map(x => {
        return {
            value: x.total, name: x.name, itemStyle: {color: getRandomColor()}
        }
    })

    console.log(`data ${item.name}: `, list_map_data)
    
    const dom           = $(`#echart-total-siswa-kelas-${item.name}`)[0]
    let doughnutChart   = echarts.init(dom);

    let option = {        
        legend: {
            orient: 'vertical',
            x: 'left',
            data: list_map_name
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b}: {c} ({d}%)',
        },
        series: [
            {
                name:`Total Siswa Kelas ${item.name}`,
                type:'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '20',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: list_map_data
            }
        ]
    };

    doughnutChart.setOption(option);
}