let SISWA_ID,
    ORTU_ID,
    RIWAYAT_ID,
    KELAS_ID = "",
    KELASIS_ID,
    KAMSIS_ID,
    FILE_NAME, FILE_TYPE, FILE_SIZE

$(document).ready(function(){
  console.log("LOAD SCRIPT")

  get_kelas()
  get_kamar()
  get_file_kat()

  //DATATABLE
  let siswa_table = $('#siswa-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${SISWA_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number  = d.start > 0 ? (start/size) : 0;
        newObj.page_size    = size
        newObj.search       = d.search.value
        newObj.draw         = d.draw
        newObj.id_kelas     = KELAS_ID 
        newObj.id_ajaran    = ID_AJARAN_GLOBAL
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "nis",
          orderable: false
        },
        { 
          data: "card_no",
          render: function (data, type, row, meta) {
            let color = data ? 'info' : 'secondary'
            let text = data ? data : 'Belum ada'
            let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

            return badge
          },
          orderable: false
        },
        { 
          data: "nama",
          orderable: false
        },
        { 
          data: "kelamin",
          render: function (data, type, row, meta) {
            return data == null ? "-" : data.toUpperCase() == 'L' ? 'Laki-laki' : 'Perempuan'
          },
          orderable: false
        },
        { 
          data: "kelas",
          orderable: false
        },
        { 
          data: "kamar",
          orderable: false
        },
        {
          data: "is_active",
          render: function (data, type, row, meta) {
            let color = parseInt(data) ? 'primary' : 'danger'
            let text = parseInt(data) ? 'ACTIVE' : 'INACTIVE'
            let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

            return badge
          },
          orderable: false
        },
        {
          data: "createdt",
          orderable: false
        },
        {
          data: "id_siswa",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let actionList = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}siswa/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>
            <a class="dropdown-item bg-secondary text-white" href="${WEB_URL}siswa/detail/${data}" title="detail">
              <i class="fa fa-search"></i> Detail
            </a>
            <a class="dropdown-item bg-secondary text-white" href="${WEB_URL}siswa/report/${data}" title="Rapor Nilai">
              <i class="fa fa-book"></i> Rapor Nilai
            </a>
            <a class="dropdown-item bg-info text-white siswa-card-toggle" href="#" title="tambah dokumen" data-id="${data}" data-toggle="modal" data-target="#siswa-document-modal">
              <i class="fa fa-folder-open-o"></i> Tambah Dokumen
            </a>
            <a class="dropdown-item bg-info text-white siswa-card-toggle" href="#" title="ubah id card" data-card="${row.card_no}" data-id="${data}" data-toggle="modal" data-target="#siswa-card-modal">
              <i class="fa fa-id-card"></i> Ubah ID card
            </a>
            <a class="dropdown-item bg-info text-white siswa-kelas-toggle" href="#" title="detail" data-idkelasis="${row.id_kelasis}" data-kelas="${row.kelas}" data-id="${data}" data-toggle="modal" data-target="#siswa-kelas-modal">
              <i class="fa fa-building"></i> Ubah Kelas
            </a>
            <a class="dropdown-item bg-info text-white siswa-kamar-toggle" href="#" title="detail" data-idkamsis="${row.id_kamsis}" data-kamar="${row.kamar}" data-id="${data}" data-toggle="modal" data-target="#siswa-kamar-modal">
              <i class="fa fa-home"></i> Ubah Kamar
            </a>`
            if(parseInt(row.is_active)){
              actionList += ` <a href="#" class="dropdown-item bg-warning text-white siswa-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#siswa-inactive-modal" title="nonaktif">
                <i class="fa fa-times"></i> Nonaktif
              </a>`
            }else{
              actionList += ` <a href="#" class="dropdown-item bg-info text-white siswa-active-toggle" data-id="${data}" data-toggle="modal" data-target="#siswa-active-modal" title="aktif">
                <i class="fa fa-check"></i> Aktif
              </a>`
            }

            let button = `<div class="dropdown dropleft">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                Action
              </button>
              <div class="dropdown-menu py-0">
                ${actionList}
              </div>
            </div>`
            
            return button
          },
          orderable: false
        }
    ]
  });

  //ACTION
  $('#refresh-table').click(function(){
    siswa_table.ajax.reload()
  })

  $('#filter-kelas-option').change(function(){
    KELAS_ID = $(this).val()
    siswa_table.ajax.reload()
  })

  $('#siswa-document-field').change(function(e) {
    let file = e.target.files[0];
    upload_document(file)
  });

  $("body").delegate(".siswa-document-toggle", "click", function(e) {
    SISWA_ID  = $(this).data('id')
    FILE_NAME = null 
    FILE_TYPE = null
    FILE_SIZE = null
  })

  $("body").delegate(".siswa-inactive-toggle", "click", function(e) {
    SISWA_ID = $(this).data('id')
  })
  
  $("body").delegate(".siswa-active-toggle", "click", function(e) {
    SISWA_ID = $(this).data('id')
  })

  $("body").delegate(".siswa-card-toggle", "click", function(e) {
    SISWA_ID = $(this).data('id')
    const cardNo = $(this).data('card')
    $('#siswa-card-new').val('')

    if(cardNo){
      $('#siswa-card-field').text(cardNo)
    }else{
      $('#siswa-card-field').text("Belum Ada")
    }
  })

  $("body").delegate(".siswa-kelas-toggle", "click", function(e) {
    SISWA_ID = $(this).data('id')
    const kelas = $(this).data('kelas')
    KELASIS_ID = $(this).data('idkelasis')

    if(kelas){
      $('#siswa-kelas-field').text(kelas)
    }else{
      $('#siswa-kelas-field').text("Belum Ada")
    }
  })

  $("body").delegate(".siswa-kamar-toggle", "click", function(e) {
    SISWA_ID = $(this).data('id')
    const kamar = $(this).data('kamar')
    KAMSIS_ID = $(this).data('idkamsis')

    if(kamar){
      $('#siswa-kamar-field').text(kamar)
    }else{
      $('#siswa-kamar-field').text("Belum Ada")
    }
  })

  $('#siswa-inactive-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-inactive-button')

    let $form = $(this),
        request = {
          id_siswa: SISWA_ID,
          tgl: convertToDBDateFormatV2($form.find("input[name='tgl']").val()),
          keterangan: $form.find("textarea[name='keterangan']").val(),
        }
    
    $.ajax({
        async: true,
        url: `${SISWA_MUTASI_API_URL}/leave`,
        type: 'POST',
        data: JSON.stringify(request),
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-inactive-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Nonaktif siswa berhasil!`)
          endLoadingButton('#siswa-inactive-button', 'Simpan')
          $('#siswa-inactive-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })
  
  $('#siswa-active-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-active-button')

    let $form = $(this),
        request = {
          id_siswa: SISWA_ID,
          tgl: convertToDBDateFormatV2($form.find("input[name='tgl']").val()),
          keterangan: $form.find("textarea[name='keterangan']").val(),
        }
    
    $.ajax({
        async: true,
        url: `${SISWA_MUTASI_API_URL}/return`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-active-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Aktifasi siswa berhasil!`)
          endLoadingButton('#siswa-active-button', 'Simpan')
          $('#siswa-active-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })

  $('#siswa-card-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-card-button')

    let $form = $(this),
        data = {
          id_siswa: SISWA_ID,
          card_no: $form.find("input[name='card_no']").val()
        }
    
    $.ajax({
        async: true,
        url: `${SISWA_API_URL}/card`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-card-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Mengubah ID Card siswa berhasil!`)
          endLoadingButton('#siswa-card-button', 'Ya')
          $('#siswa-card-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })

  $('#siswa-kelas-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-kelas-button')

    let data = {
      id_siswa: SISWA_ID,
      id_ajaran: ID_AJARAN_GLOBAL,
      id_kelas: $("#siswa-kelas-option").find(":selected").val()
    }
    let method = 'POST'
    if(KELASIS_ID) {
      data.id_kelasis = KELASIS_ID
      method = 'PUT'
    }
    
    $.ajax({
        async: true,
        url: `${KELASIS_API_URL}`,
        type: method,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-kelas-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Ubah Kelas siswa berhasil!`)
          endLoadingButton('#siswa-kelas-button', 'Ya')
          $('#siswa-kelas-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })

  $('#siswa-kamar-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-kamar-button')

    let data = {
      id_siswa: SISWA_ID,
      id_ajaran: ID_AJARAN_GLOBAL,
      id_kamar: $("#siswa-kamar-option").find(":selected").val()
    }
    let method = 'POST'
    if(KAMSIS_ID) {
      data.id_kamsis = KAMSIS_ID
      method = 'PUT'
    }
    
    $.ajax({
        async: true,
        url: `${KAMSIS_API_URL}`,
        type: method,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-kamar-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Ubah Kamar siswa berhasil!`)
          endLoadingButton('#siswa-kamar-button', 'Simpan')
          $('#siswa-kamar-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })

  $('#siswa-document-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#siswa-document-button')

    let data = {
      id_siswa: SISWA_ID,
      id_file: $("#siswa-document-option").find(":selected").val(),
      file_name: FILE_NAME,
      file_size: FILE_SIZE,
      file_type: FILE_TYPE
    }
    
    $.ajax({
        async: true,
        url: `${SISWA_FILE_API_URL}`,
        type: "POST",
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#siswa-document-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Simpan dokumen siswa berhasil!`)
          endLoadingButton('#siswa-document-button', 'Simpan')
          $('#siswa-document-modal').modal('toggle')
          siswa_table.ajax.reload()
        }
    });
  })
})

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas     = res.data
      render_kelas_option(kelas) 
    }
  })
}

function render_kelas_option(data){
  let options_html = `<option value="">Semua</option>`
  let options2_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
    options2_html += option_html
  }

  $('#filter-kelas-option').html(options_html)
  $('#siswa-kelas-option').html(options2_html)
}

function get_kamar(){
  $.ajax({
    async: true,
    url: `${KAMAR_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas     = res.data
      render_kamar_option(kelas) 
    }
  })
}

function render_kamar_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kamar}">${item.kamar}</option>`
    options_html += option_html
  }

  $('#siswa-kamar-option').html(options_html)
}

function get_file_kat(){
  $.ajax({
    async: true,
    url: `${FILE_KAT_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const file_kat = res.data
      render_file_kat_option(file_kat) 
    }
  })
}

function render_file_kat_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id}">${item.file}</option>`
    options_html += option_html
  }

  $('#siswa-document-option').html(options_html)
}

function upload_document(file){
  $(`#siswa-document-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('document', file);
  
  $.ajax({
      async: true,
      url: `${SISWA_FILE_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload dokumen", "Gagal melakukan upload dokumen!")
          $(`#siswa-document-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response  = res.data
        FILE_NAME       = response.file_name
        FILE_TYPE       = response.file_type
        FILE_SIZE       = response.file_size
        $(`#siswa-document-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}