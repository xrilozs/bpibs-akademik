let selectedRowsData = []
let dataTable = $('#siswa-datatable').DataTable({
  paging: false,      // Disable pagination
  info: false,        // Disable information display
  searching: false,   // Disable search box
  ordering: false,    // Disable sorting
  bInfo: false,       // Disable bottom information (used in older versions)
  bPaginate: false,   // Disable pagination (used in older versions)
  bLengthChange: false,  // Disable the ability to change page length (used in older versions)
  bFilter: false,     // Disable the search/filtering feature (used in older versions)
  bSort: false,       // Disable sorting (used in older versions)
  bAutoWidth: false,
  columns: [
    { 
      data: null, 
      orderable: false, 
      searchable: false, 
      render: function(data, type, row) {
        let checked = 'checked'
        if(row.id_absensi){
          checked = !parseInt(row.hadir) ? '' : 'checked'
        }
        return `<input type="checkbox" class="row-checkbox" ${checked}>`
      }
    },
    { 
      data: null, 
      orderable: false, 
      searchable: false, 
      render: function(data, type, row) {
        let keterangan_absen = ''
        let disabled = 'disabled'
        if(row.id_absensi){
          keterangan_absen  = row.keterangan_absen
          disabled          = parseInt(row.hadir) ? "disabled" : ''
        }
        return `<select class="form-control row-select" ${disabled} required>
          <option value="" disbaled ${keterangan_absen == null || keterangan_absen == "" ? "selected" : ""}>-Pilih Keterangan-</option>
          <option value="S" ${keterangan_absen == "S" ? "selected" : ""}>Sakit</option>
          <option value="I" ${keterangan_absen == "I" ? "selected" : ""}>Ijin</option>
          <option value="TK" ${keterangan_absen == "TK" ? "selected" : ""}>Tanpa Keterangan</option>
        </select>`;
      }
    },
    { 
      data: null, 
      orderable: false, 
      searchable: false, 
      render: function(data, type, row) {
        let deskripsi = ''
        if(row.id_absensi){
          deskripsi = row.deskripsi
        }
        return `<textarea class="form-control row-textarea" rows="2" cols="30">${deskripsi}</textarea>`;
      }
    },
    { 
      data: "nis",
      orderable: false
    },
    // { 
    //   data: "card_no",
    //   orderable: false
    // },
    { 
      data: "nama",
      orderable: false
    },
    // { 
    //   data: "kelamin",
    //   render: function (data, type, row, meta) {
    //     return data ? data.toUpperCase() == 'L' ? 'Laki-laki' : 'Perempuan' : "-"
    //   },
    //   orderable: false
    // },
    { 
      data: "kelas",
      orderable: false
    },
    { 
      data: "kamar",
      orderable: false
    },
    { 
      data: "id_absensi",
      render: function (data, type, row, meta) {
        let color = parseInt(data) ? 'success' : 'secondary'
        let text = parseInt(data) ? 'SUDAH ABSENSI' : 'BELUM ABSENSI'
        let badge = `<span class="badge badge-${color}">${text}</span>`

        return badge
      },
    },
  ]
});

$(document).ready(function(){
  // get_semester()
  get_kelas()
  get_mapel()

  $('#select-all-checkbox').on('click', function() {
    $('.row-checkbox').prop('checked', $(this).prop('checked'));
  });

  $('#siswa-datatable tbody').on('change', '.row-checkbox', function() {
    handleCheckboxChange($(this));
  });

  $('#absensi-button').click(function(){
    startLoadingButton('#absensi-button')

    let list_absensi = getRowsData()

    let invalid = false
    list_absensi.forEach(absensi => {
      if(!absensi.hadir && !absensi.keterangan_absen){
        invalid = true
      }
    });

    if(invalid){
      showError(`Siswa yang tidak hadir harus memilih keterangan absen!`)
      endLoadingButton('#absensi-button', `<i class="fa fa-pencil" aria-hidden="true"></i> Absensi`)
      return
    }
    
    let request = {
      list_absensi
    }

    $.ajax({
      async: true,
      url: `${ABSENSI_API_URL}/siswa`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(request),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          endLoadingButton('#absensi-button', "Absensi")
          showError(`Gagal melakukan absensi siswa`)
        }
      },
      success: function(res) {
        console.log(res.data)
        endLoadingButton('#absensi-button', "Absensi")
        showSuccess(`Berhasil melakukan absensi siswa`)
      }
    });
  })

  $('#absensi-search-form').submit(function(e){
    e.preventDefault()

    startLoadingButton('#absensi-search-button')

    let $form   = $(this),
        request = {
          id_ajaran: ID_AJARAN_GLOBAL,
          id_semester: ID_SEMESTER_GLOBAL,
          id_kelas: $('#filter-kelas-option').find(":selected").val(),
          id_mapel: $('#filter-mapel-option').find(":selected").val(),
          tanggal: convertToDBDateFormatV2($form.find( "input[name='tanggal']" ).val()),
        }
        
    $.ajax({
        async: true,
        url: `${ABSENSI_API_URL}/search`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            endLoadingButton('#absensi-search-button', "Cari")
            $('#absensi-siswa-alert').show()
            $('#absensi-siswa-section').hide()
          }
        },
        success: function(res) {
          console.log(res.data)
          endLoadingButton('#absensi-search-button', "Cari")
          $('#absensi-siswa-alert').hide()
          $('#absensi-siswa-section').show()
          dataTable.clear().rows.add(res.data).draw();
        }
    });
  })
})

function get_semester(){
  $.ajax({
    async: true,
    url: `${SEMESTER_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const semester = res.data
      render_semester_option(semester) 
    }
  })
}

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function get_mapel(){
  $.ajax({
    async: true,
    url: `${MAPEL_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel = res.data
      render_mapel_option(mapel) 
    }
  })
}

function render_semester_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_semester}">${item.semester}</option>`
    options_html += option_html
  }

  $('#filter-semester-option').html(options_html)
}

function render_kelas_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('#filter-kelas-option').html(options_html)
}

function render_mapel_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_mapel}">${item.mapel}</option>`
    options_html += option_html
  }

  $('#filter-mapel-option').html(options_html)
}

function getRowsData() {
  let allRowsData = [];

  dataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
      var rowData               = this.data();
      rowData.deskripsi         = $(this.node()).find('.row-textarea').val();
      rowData.keterangan_absen  = $(this.node()).find('.row-select').val();
      rowData.hadir             = $(this.node()).find('.row-checkbox').prop('checked');
      allRowsData.push(rowData);
  });

  // Display selected rows data in console
  console.log(allRowsData);
  return allRowsData
}

function handleCheckboxChange(checkbox) {
  var select = checkbox.closest('tr').find('.row-select');

  if (!checkbox.prop('checked')) {
    select.prop('disabled', false)
  } else {
    select.val("").change()
    select.prop('disabled', true)
  }
}
