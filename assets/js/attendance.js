$(document).ready(function(){
  console.log("LOAD SCRIPT")

  $('#attendance-form').submit(e => {
    e.preventDefault()
    startLoadingButton('#attendance-button')
    let $form = $(this),
        scanType = $form.find( "input[name='scan-option']:checked" ).val(),
        scanNumber = $form.find( "input[name='scan-value']" ).val()

    $.ajax({
        async: false,
        url: `${SISWA_API_URL}/card?scan_type=${scanType}&scan_number=${scanNumber}`,
        type: 'GET',
        error: function(res) {
            showError("Data siswa tidak ditemukan")
            endLoadingButton('#attendance-button', "Absensi Masuk")
            $('#scan-result').hide()
            $('#no-scan').show()          
        },
        success: function(res) {
          const response = res.data
          showSuccess("Data siswa ditemukan")
          endLoadingButton('#attendance-button', "Absensi Masuk")
          $('#no-scan').hide()
          $('#scan-result').show()
          renderDataSiswa(response)
        }
    });

  })
})

function renderDataSiswa(data){
    let $form = $('#siswa-form')
    $form.find( "input[name='siswa-nis']" ).val(data.nis)
    $form.find( "input[name='siswa-nama']" ).val(data.nama)
    $( "#siswa-kelamin-option" ).val(data.kelamin).change()
}