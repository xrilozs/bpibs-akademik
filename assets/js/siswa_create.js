let IMAGE_URL

$(document).ready(function(){
    //EVENT
    // $('#siswa-create-foto').change(function(e) {
    //   let file = e.target.files[0];
    //   upload_image(file)
    // });

    $('#form-wizard').steps({
      headerTag: 'h2',
      bodyTag: 'section',
      transitionEffect: 'slideLeft',
      onStepChanging: function (event, currentIndex, newIndex) {
        // Validate the form before moving to the next step
        let forms = ['#siswa-form', '#ortu-form', '#riwayat-form', '#info-form']
        var form = $(`${forms[currentIndex]}`);
        console.log(currentIndex)

        // Check if the current step is valid
        var isValid = form.valid();

        // If the current step is valid, allow the user to proceed
        if (isValid) {
          return true;
        }

        // If the current step is not valid, do not allow the user to proceed
        form.validate().focusInvalid();
        return false;
      },
      onFinished: function(_event){
        if(!IMAGE_URL){
          showError("Foto belum diupload!")
          return
        }else{
          createSiswa()
        }
      }
    });

  $(`#siswa-create-foto`).dropify()

  $('#siswa-create-foto').change(function(e) {
    let file = e.target.files[0];
    upload_image(file)
  });
})

function createSiswa(){
    let $siswaform      = $("#siswa-form"),
        $ortuform       = $("#ortu-form"),
        $riwayatform    = $("#riwayat-form"),
        $infoform       = $("#info-form"),
        request = {
          siswa: {
            foto: IMAGE_URL,
            nis: $siswaform.find( "input[name='siswa-nis']" ).val(),
            nisn: $siswaform.find( "input[name='siswa-nisn']" ).val(),
            nama: $siswaform.find( "input[name='siswa-nama']" ).val(),
            panggilan: $siswaform.find( "input[name='siswa-panggilan']" ).val(),
            kelamin: $('#siswa-create-kelamin').find(":selected").val(),
            tmplhr: $siswaform.find( "input[name='siswa-tmplhr']" ).val(),
            tgllhr: convertToDBDateFormat($siswaform.find( "input[name='siswa-tgllhr']" ).val()),
            agama: $siswaform.find( "input[name='siswa-agama']" ).val(),
            statdk: $('#siswa-create-statdk').find(":selected").val(),
            anakke: $siswaform.find( "input[name='siswa-anakke']" ).val(),
            jmlsdr: $siswaform.find( "input[name='siswa-jmlsdr']" ).val(),
            alamat: $siswaform.find( "textarea[name='siswa-alamat']" ).val(),
            kel: $siswaform.find( "input[name='siswa-kel']" ).val(),
            kec: $siswaform.find( "input[name='siswa-kec']" ).val(),
            kokab: $siswaform.find( "input[name='siswa-kokab']" ).val(),
            prov: $siswaform.find( "input[name='siswa-prov']" ).val(),
            nva: $siswaform.find( "input[name='siswa-nva']" ).val(),
            hp: $siswaform.find( "input[name='siswa-hp']" ).val(),
            email: $siswaform.find( "input[name='siswa-email']" ).val(),
            ket: $siswaform.find( "textarea[name='siswa-ket']" ).val(),
          },
          ortu: {
            ayah_nama: $ortuform.find( "input[name='ortu-ayah_nama']" ).val(),
            ayah_pekerjaan: $ortuform.find( "input[name='ortu-ayah_pekerjaan']" ).val(),
            ayah_hp: $ortuform.find( "input[name='ortu-ayah_hp']" ).val(),
            ibu_nama: $ortuform.find( "input[name='ortu-ibu_nama']" ).val(),
            ibu_pekerjaan: $ortuform.find( "input[name='ortu-ibu_pekerjaan']" ).val(),
            ibu_hp: $ortuform.find( "input[name='ortu-ibu_hp']" ).val(),
            penghasilan: $ortuform.find( "input[name='ortu-penghasilan']" ).val(),
            email: $ortuform.find( "input[name='ortu-email']" ).val(),
            alamat: $ortuform.find( "textarea[name='ortu-alamat']" ).val(),
            wali_nama: $ortuform.find( "input[name='ortu-wali_nama']" ).val(),
            wali_pekerjaan: $ortuform.find( "input[name='ortu-wali_pekerjaan']" ).val(),
            wali_hp: $ortuform.find( "input[name='ortu-wali_hp']" ).val(),
            wali_alamat: $ortuform.find( "input[name='ortu-wali_alamat']" ).val(),
            wali_penghasilan: $ortuform.find( "input[name='ortu-wali_penghasilan']" ).val(),
            wali_email: $ortuform.find( "input[name='ortu-wali_email']" ).val(),
          },
          riwayat: {
            tgl_tes: $riwayatform.find( "input[name='riwayat-tgl_tes']" ).val() ? convertToDBDateFormat($riwayatform.find( "input[name='riwayat-tgl_tes']" ).val()) : null,
            asal_sekolah: $riwayatform.find( "input[name='riwayat-asal_sekolah']" ).val(),
            tahun_lulus: $riwayatform.find( "input[name='riwayat-tahun_lulus']" ).val(),
            internal_bp: $riwayatform.find( "input[name='riwayat-internal_bp']" ).val(),
            cabang_bp: $riwayatform.find( "input[name='riwayat-cabang_bp']" ).val(),
            komp_ak: $riwayatform.find( "input[name='riwayat-komp_ak']" ).val(),
            komp_nonak: $riwayatform.find( "input[name='riwayat-komp_nonak']" ).val(),
            komp_mengaji: $riwayatform.find( "input[name='riwayat-komp_mengaji']" ).val(),
            komp_tularab: $riwayatform.find( "input[name='riwayat-komp_tularab']" ).val(),
            hafalan: $riwayatform.find( "input[name='riwayat-hafalan']" ).val(),
          },
          info: {
            tgl_masuk: $infoform.find( "input[name='info-tgl_masuk']" ).val() ? convertToDBDateFormat($infoform.find( "input[name='info-tgl_masuk']" ).val()) : null,
            ke_kelas: $infoform.find( "input[name='info-ke_kelas']" ).val(),
            info_bpibs: $infoform.find( "input[name='info-info_bpibs']" ).val(),
            alasan_ortu: $infoform.find( "textarea[name='info-alasan_ortu']" ).val(),
            alasan_siswa: $infoform.find( "textarea[name='info-alasan_siswa']" ).val(),
            keterangan: $infoform.find( "textarea[name='info-keterangan']" ).val(),
          }
        }
        
    $.ajax({
        async: true,
        url: SISWA_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else showError("Gagal menyimpan data siswa!")
        },
        success: function(res) {
          showSuccess(`Membuat data siswa baru berhasil!`)
          setTimeout(function() {
            window.history.go(-1);
          }, 2000);
        }
    });
}

function upload_image(file){
  // $(`#siswa-create-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${SISWA_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          // $(`#siswa-create-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMAGE_URL = response.file_url
        // $(`#siswa-create-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}