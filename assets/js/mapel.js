let MAPEL_ID,
    AJARAN = [],
    ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  console.log("LOAD SCRIPT")
  if ($('.mapel-kategori-option').length > 0) {
    get_kategori()
  }

  if(!isNaN(ID_ALIAS)){
    get_mapel_detail()
  }

  //DATATABLE
  let mapel_table = $('#mapel-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${MAPEL_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        newObj.id_ajaran = ID_AJARAN_GLOBAL
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "urutan",
          orderable: false
        },
        { 
          data: "kategori",
          orderable: false
        },
        { 
          data: "mapel",
          orderable: false
        },
        { 
          data: "mapel_det",
          orderable: false
        },
        {
          data: "createdt",
          orderable: false
        },
        {
          data: "id_mapel",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let actionList = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}mapel/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>
            <a class="dropdown-item bg-danger text-white mapel-delete-toggle" href="#" title="hapus" data-id="${data}" data-toggle="modal" data-target="#mapel-delete-modal">
              <i class="fa fa-trash"></i> Hapus
            </a>`

            let button = `<div class="dropdown dropleft">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                Action
              </button>
              <div class="dropdown-menu py-0">
                ${actionList}
              </div>
            </div>`
            
            return button
          },
          orderable: false
        }
    ]
  });


  //ACTION
  $('#refresh-table').click(function(){
    mapel_table.ajax.reload()
  })

  $("body").delegate(".mapel-delete-toggle", "click", function(e) {
    MAPEL_ID = $(this).data('id')
  })

  $('#mapel-delete-button').click(function (){
    startLoadingButton('#mapel-delete-button')
    
    $.ajax({
        async: true,
        url: `${MAPEL_API_URL}/delete/${MAPEL_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#mapel-delete-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Hapus mata pelajaran berhasil!`)
          endLoadingButton('#mapel-delete-button', 'Ya')
          $('#mapel-delete-modal').modal('toggle')
          mapel_table.ajax.reload()
        }
    });
  })

  //FORM
  $('#mapel-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#mapel-create-button')
    
    let $form = $(this),
        request = {
          urutan: $form.find( "input[name='urutan']" ).val(),
          mapel: $form.find( "input[name='mapel']" ).val(),
          mapel_det: $form.find( "input[name='mapel_det']" ).val(),
          id_kat: $('#mapel-create-kategori').find(":selected").val(),
          id_ajaran: ID_AJARAN_GLOBAL
        }
        
    $.ajax({
        async: true,
        url: MAPEL_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#mapel-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Membuat data mata pelajaran baru berhasil!`)
          endLoadingButton('#mapel-create-button', 'Simpan')
          $('#mapel-create-modal').modal('hide')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);
        }
    });
  })
  
  $('#mapel-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#mapel-update-button')

    let $form = $(this),
        request = {
          id_mapel: MAPEL_ID,
          urutan: $form.find( "input[name='urutan']" ).val(),
          mapel: $form.find( "input[name='mapel']" ).val(),
          mapel_det: $form.find( "input[name='mapel_det']" ).val(),
          id_kat: $('#mapel-update-kategori').find(":selected").val()
        }

    $.ajax({
        async: true,
        url: MAPEL_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#mapel-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Mengubah data mapel berhasil!`)
          endLoadingButton('#mapel-update-button', 'Simpan')
          $('#mapel-update-modal').modal('toggle')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);        
        }
    });
  })
})

function get_kategori(){
  $.ajax({
    async: true,
    url: `${KATEGORI_MAPEL_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_kategori_option(res.data) 
    }
  })
}

function get_mapel_detail(){
  $.ajax({
    async: true,
    url: `${MAPEL_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel   = res.data
      MAPEL_ID      = mapel.id_mapel
      render_mapel_detail(mapel) 
    }
  })
}

function render_mapel_detail(data){
  let $form = $(`#mapel-update-form`)
  $form.find( "input[name='urutan']" ).val(data.urutan)
  $form.find( "input[name='mapel']" ).val(data.mapel)
  $form.find( "input[name='mapel_det']" ).val(data.mapel_det)
  $(`#mapel-update-kategori`).val(data.id_kat).change()
}

function render_kategori_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kat}">${item.kategori}</option>`
    options_html += option_html
  }

  $('.mapel-kategori-option').html(options_html)
}