let USER_LOGIN_ID,
    IMAGE_URL,
    USER_ROLE = [],
    ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  console.log("LOAD SCRIPT")
  if ($('.user-login-role-option').length > 0) {
    get_role()
  }

  if(!isNaN(ID_ALIAS)){
    get_user_login_detail()
  }

  //DATATABLE
  let user_login_table = $('#user-login-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${USER_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "name",
          orderable: false
        },
        { 
          data: "email",
          orderable: false
        },
        { 
          data: "username",
          orderable: false
        },
        { 
          data: "role",
          orderable: false
        },
        {
          data: "is_active",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let color = parseInt(data) ? 'primary' : 'danger'
            let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
            let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

            return badge
          }
        },
        {
          data: "date_created",
          orderable: false
        },
        {
          data: "id",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let actionList = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}user-login/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>`
            if(parseInt(row.is_active)){
              actionList += ` <a href="#" class="dropdown-item bg-warning text-white user-login-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#user-login-inactive-modal" title="nonaktif">
                <i class="fa fa-times"></i> Nonaktif
              </a>`
            }else{
              actionList += ` <a href="#" class="dropdown-item bg-info text-white user-login-active-toggle" data-id="${data}" data-toggle="modal" data-target="#user-login-active-modal" title="aktif">
                <i class="fa fa-check"></i> Aktif
              </a>`
            }

            let button = `<div class="dropdown dropleft">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                Action
              </button>
              <div class="dropdown-menu py-0">
                ${actionList}
              </div>
            </div>`
            
            return button
          },
          orderable: false
        }
    ]
  });


  //ACTION
  $('#refresh-table').click(function(){
    user_login_table.ajax.reload()
  })

  $("body").delegate(".user-login-inactive-toggle", "click", function(e) {
    USER_LOGIN_ID = $(this).data('id')
  })
  
  $("body").delegate(".user-login-active-toggle", "click", function(e) {
    USER_LOGIN_ID = $(this).data('id')
  })

  $('#user-login-inactive-button').click(function (){
    startLoadingButton('#user-login-inactive-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}/inactive/${USER_LOGIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-login-inactive-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Nonaktif user berhasil!`)
          endLoadingButton('#user-login-inactive-button', 'Ya')
          $('#user-login-inactive-modal').modal('toggle')
          user_login_table.ajax.reload()
        }
    });
  })
  
  $('#user-login-active-button').click(function (){
    startLoadingButton('#user-login-active-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}/active/${USER_LOGIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-login-active-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Aktif user berhasil!`)
          endLoadingButton('#user-login-active-button', 'Ya')
          $('#user-login-active-modal').modal('toggle')
          user_login_table.ajax.reload()
        }
    });
  })

  //FORM
  $('#user-login-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#user-login-create-button')
    
    let $form = $(this),
        request = {
          name: $form.find( "input[name='name']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          image: IMAGE_URL,
          role_id: $('#user-login-create-role').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: USER_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-login-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Membuat user baru berhasil!`)
          endLoadingButton('#user-login-create-button', 'Simpan')
          $('#user-login-create-modal').modal('hide')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);
        }
    });
  })
  
  $('#user-login-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#user-login-update-button')

    let $form = $(this),
        request = {
          id: USER_LOGIN_ID,
          name: $form.find( "input[name='name']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          image: IMAGE_URL,
          role_id: $('#user-login-update-role').find(":selected").val(),
        }

    $.ajax({
        async: true,
        url: USER_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-login-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Mengubah user berhasil!`)
          endLoadingButton('#user-login-update-button', 'Simpan')
          $('#user-login-update-modal').modal('toggle')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);        
        }
    });
  })

  //EVENT
  $('#user-login-create-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });

  $('#user-login-update-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });
})

function get_role(){
  $.ajax({
    async: true,
    url: `${USER_ROLE_API_URL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_role_option(res.data) 
    }
  })
}

function get_user_login_detail(){
  $.ajax({
    async: true,
    url: `${USER_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const user    = res.data
      USER_LOGIN_ID = user.id
      IMAGE_URL     = user.image
      render_user_login_detail(user) 
    }
  })
}

function upload_image(file, type){
    $(`#user-login-${type}-button`).attr("disabled", true)

    let formData = new FormData();
    formData.append('image', file);
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}/upload`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: formData,
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Upload gambar", "Gagal melakukan upload gambar!")
            $(`#user-login-${type}-button`).attr("disabled", false)
          }
        },
        success: function(res) {
          const response = res.data
          IMAGE_URL = response.file_url
          $(`#user-login-${type}-button`).attr("disabled", false)
          showSuccess(`Mengunggah gambar berhasil!`)
        }
    });
}

function render_user_login_detail(data){
  let $form = $(`#user-login-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='username']" ).val(data.username)
  $form.find( "input[name='email']" ).val(data.email)
  $(`#user-login-update-role`).val(data.role_id).change()
  $(`#user-login-update-image`).attr("data-default-file", data.full_image)
  $(`#user-login-update-image`).dropify({
    defaultFile: data.full_image
  })

  let color = data.is_active ? 'primary' : 'danger'
  let text = data.is_active ? 'ACTIVE' : 'INACTIVE'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#user-login-update-status`).html(badge)
}

function render_role_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id}">${item.role}</option>`
    options_html += option_html
  }

  $('.user-login-role-option').html(options_html)
}