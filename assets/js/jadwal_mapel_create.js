$(document).ready(function(){
  get_semester()
  get_kelas()
  get_mapel()
  get_hari()

  $('#jadwal-mapel-create-form').submit(function(e){
    e.preventDefault()

    startLoadingButton('#jadwal-mapel-create-button')

    let $form   = $(this),
        request = {
          id_ajaran: ID_AJARAN_GLOBAL,
          id_semester: $('#jadwal-mapel-create-semester').find(":selected").val(),
          id_kelas: $('#jadwal-mapel-create-kelas').find(":selected").val(),
          id_mapel: $('#jadwal-mapel-create-mapel').find(":selected").val(),
          id_hari: $('#jadwal-mapel-create-hari').find(":selected").val(),
          awal: $form.find( "input[name='awal']" ).val(),
          akhir:  $form.find( "input[name='akhir']" ).val()
        }
        
    $.ajax({
        async: true,
        url: JADWAL_MAPEL_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Buat Jadwal Mata Pelajaran gagal!")
            endLoadingButton('#jadwal-mapel-create-button', "Simpan")
          }
        },
        success: function(res) {
          showSuccess("Buat Jadwal Mata Pelajaran sukses!")
          endLoadingButton('#jadwal-mapel-create-button', "Simpan")
          setTimeout(function() {
            window.history.go(-1);
          }, 2000);
        }
    });
  })
})

function get_semester(){
  $.ajax({
    async: true,
    url: `${SEMESTER_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const semester = res.data
      render_semester_option(semester) 
    }
  })
}

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function get_mapel(){
  $.ajax({
    async: true,
    url: `${MAPEL_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel = res.data
      render_mapel_option(mapel) 
    }
  })
}

function get_hari(){
  $.ajax({
    async: true,
    url: `${HARI_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const hari = res.data
      render_hari_option(hari) 
    }
  })
}

function render_semester_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_semester}">${item.semester}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-semester-option').html(options_html)
}

function render_kelas_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-kelas-option').html(options_html)
}

function render_mapel_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_mapel}">${item.mapel}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-mapel-option').html(options_html)
}

function render_hari_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_hari}">${item.hari}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-hari-option').html(options_html)
}
