let TOAST = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION     = localStorage.getItem("user-token");

$(document).ready(function(){
  if(SESSION){
    getProfile()
  }

  $("#login-form").submit(function(e) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form     = $( this ),
        email     = $form.find( "input[name='email']" ).val(),
        password  = $form.find( "input[name='password']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}/user/login`,
        type: 'POST',
        data: JSON.stringify({
          email: email,
          password: password
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Login')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          window.location.href = 'dashboard'
        }
    });
  })
})

function getProfile(){
  $.ajax({
    async: true,
    url: `${API_URL}/user/profile`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      response = res.responseJSON
      removeSession()
    },
    success: function(res) {
      // response = res.data;
      // setSession(response)
      window.location.href = 'dashboard'
    }
}); 
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-profile", JSON.stringify(data.user))
  localStorage.setItem("user-menu", JSON.stringify(data.menu))
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-profile");
  localStorage.removeItem("user-menu");
  window.location.href = `login`
}