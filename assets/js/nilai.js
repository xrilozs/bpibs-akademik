let selectedRowsData = []
let dataTable = $('#siswa-datatable').DataTable({
  paging: false,      // Disable pagination
  info: false,        // Disable information display
  searching: false,   // Disable search box
  ordering: false,    // Disable sorting
  bInfo: false,       // Disable bottom information (used in older versions)
  bPaginate: false,   // Disable pagination (used in older versions)
  bLengthChange: false,  // Disable the ability to change page length (used in older versions)
  bFilter: false,     // Disable the search/filtering feature (used in older versions)
  bSort: false,       // Disable sorting (used in older versions)
  bAutoWidth: false,
  columns: [
    { 
      data: "nis",
      orderable: false
    },
    // { 
    //   data: "card_no",
    //   render: function (data, type, row, meta) {
    //     return data ? data : "-"
    //   },
    //   orderable: false
    // },
    { 
      data: "nama",
      orderable: false
    },
    // { 
    //   data: "kelamin",
    //   render: function (data, type, row, meta) {
    //     return data ? data.toUpperCase() == 'L' ? 'Laki-laki' : 'Perempuan' : "-"
    //   },
    //   orderable: false
    // },
    { 
      data: "kelas",
      orderable: false
    },
    { 
      data: "kamar",
      render: function (data, type, row, meta) {
        return data ? data : "-"
      },
      orderable: false
    },
    { 
      data: "id_nilai",
      render: function (data, type, row, meta) {
        let color = data ? 'success' : 'secondary'
        let text = data ? 'SUDAH DINILAI' : 'BELUM DINILAI'
        let badge = `<span class="badge badge-${color}">${text}</span>`

        return badge
      },
    },
    { 
      data: null, 
      orderable: false, 
      render: function(data, type, row) {
        let nilai = ''
        if(row.id_nilai){
          nilai = row.nilai
        }
        return `<input type="text" class="custom-input" value="${nilai}" style="width:50px;">`;
      }
    },
    { 
      data: null, 
      orderable: false, 
      render: function(data, type, row) {
        let deskripsi = ''
        if(row.id_nilai){
          deskripsi = row.deskripsi
        }
        return `<textarea class="form-control custom-textarea" rows="2" cols="30">${deskripsi}</textarea>`;
      }
    },
  ]
});

$(document).ready(function(){
  get_semester()
  get_kelas()
  get_mapel()
  get_kat_nilai()

  $('#nilai-button').click(function(){
    startLoadingButton('#nilai-button')

    let list_nilai = getRowsData()

    list_nilai.forEach(nilai => {
      nilai.id_ajaran = ID_AJARAN_GLOBAL
      nilai.id_semester = $('#filter-semester-option').find(":selected").val()
      nilai.id_kelas = $('#filter-kelas-option').find(":selected").val()
      nilai.id_mapel = $('#filter-mapel-option').find(":selected").val()
      nilai.id_kat_nilai = $('#filter-kategori-option').find(":selected").val()
    });
    
    let request = {
      list_nilai: list_nilai
    }

    $.ajax({
      async: true,
      url: `${NILAI_API_URL}/siswa`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(request),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          endLoadingButton('#nilai-button', "Simpan Nilai")
          showError(`Gagal melakukan nilai siswa`)
        }
      },
      success: function(res) {
        console.log(res.data)
        endLoadingButton('#nilai-button', "Simpan Nilai")
        showSuccess(`Berhasil melakukan nilai siswa`)
        $('#nilai-search-form').submit()
      }
    });
  })

  $('#nilai-search-form').submit(function(e){
    e.preventDefault()

    startLoadingButton('#nilai-search-button')

    let request = {
          id_ajaran: ID_AJARAN_GLOBAL,
          id_semester: $('#filter-semester-option').find(":selected").val(),
          id_kelas: $('#filter-kelas-option').find(":selected").val(),
          id_mapel: $('#filter-mapel-option').find(":selected").val(),
          id_kat_nilai: $('#filter-kategori-option').find(":selected").val(),
        }
        
    $.ajax({
        async: true,
        url: `${NILAI_API_URL}/search`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            endLoadingButton('#nilai-search-button', "Cari")
            $('#nilai-siswa-alert').show()
            $('#nilai-siswa-section').hide()
          }
        },
        success: function(res) {
          console.log(res.data)
          endLoadingButton('#nilai-search-button', "Cari")
          $('#nilai-siswa-alert').hide()
          $('#nilai-siswa-section').show()
          dataTable.clear().rows.add(res.data).draw();
        }
    });
  })
})

function get_semester(){
  $.ajax({
    async: true,
    url: `${SEMESTER_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const semester = res.data
      render_semester_option(semester) 
    }
  })
}

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function get_mapel(){
  $.ajax({
    async: true,
    url: `${MAPEL_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel = res.data
      render_mapel_option(mapel) 
    }
  })
}

function get_kat_nilai(){
  $.ajax({
    async: true,
    url: `${KATEGORI_NILAI_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel = res.data
      render_kategori_option(mapel) 
    }
  })
}

function render_semester_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_semester}">${item.semester}</option>`
    options_html += option_html
  }

  $('#filter-semester-option').html(options_html)
}

function render_kelas_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('#filter-kelas-option').html(options_html)
}

function render_mapel_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_mapel}">${item.mapel}</option>`
    options_html += option_html
  }

  $('#filter-mapel-option').html(options_html)
}

function render_kategori_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id}">${item.kategori}</option>`
    options_html += option_html
  }

  $('#filter-kategori-option').html(options_html)
}

function getRowsData() {
  let allRowsData = [];

  dataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
      var rowData = this.data();
      rowData.deskripsi = $(this.node()).find('.custom-textarea').val();
      rowData.nilai = $(this.node()).find('.custom-input').val();
      if(rowData.nilai) allRowsData.push(rowData);
  });

  // Display selected rows data in console
  console.log(allRowsData);
  return allRowsData
}