const ID_ALIAS = window.location.pathname.split("/").pop()
let ID_SISWA, ID_ORTU, ID_RIWAYAT, ID_INFO
let IMAGE_URL

$(document).ready(function(){
    console.log("LOAD SCRIPT")

    $('#form-wizard').steps({
      headerTag: 'h2',
      bodyTag: 'section',
      transitionEffect: 'slideLeft',
      onStepChanging: function (event, currentIndex, newIndex) {
        // Validate the form before moving to the next step
        let forms = ['#siswa-form', '#ortu-form', '#riwayat-form', '#info-form']
        var form = $(`${forms[currentIndex]}`);
        console.log(currentIndex)

        // Check if the current step is valid
        var isValid = form.valid();

        // If the current step is valid, allow the user to proceed
        if (isValid) {
          return true;
        }

        // If the current step is not valid, do not allow the user to proceed
        form.validate().focusInvalid();
        return false;
      },
      onFinished: function(_event){
        if(!IMAGE_URL){
          showError("Foto belum diupload!")
          return
        }else{
          updateSiswa()
        }
      }
    });

    getSiswaDetail()
    
    //EVENT
    $('#siswa-update-foto').change(function(e) {
      let file = e.target.files[0];
      upload_image(file)
    });
})

function getSiswaDetail(){
    $.ajax({
      async: true,
      url: `${SISWA_API_URL}/by-id/${ID_ALIAS}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else showError("Gagal mendapatkan data siswa!")
      },
      success: function(res) {
        const siswa = res.data
        renderSiswaDetail(siswa) 
      }
    })
}

function updateSiswa(){
  let $siswaform      = $("#siswa-form"),
      $ortuform       = $("#ortu-form"),
      $riwayatform    = $("#riwayat-form"),
      $infoform       = $("#info-form"),
      request = {
        siswa: {
          id_siswa: ID_SISWA,
          foto: IMAGE_URL,
          nis: $siswaform.find( "input[name='siswa-nis']" ).val(),
          nisn: $siswaform.find( "input[name='siswa-nisn']" ).val(),
          nama: $siswaform.find( "input[name='siswa-nama']" ).val(),
          panggilan: $siswaform.find( "input[name='siswa-panggilan']" ).val(),
          kelamin: $('#siswa-update-kelamin').find(":selected").val(),
          tmplhr: $siswaform.find( "input[name='siswa-tmplhr']" ).val(),
          tgllhr: convertToDBDateFormat($siswaform.find( "input[name='siswa-tgllhr']" ).val()),
          agama: $siswaform.find( "input[name='siswa-agama']" ).val(),
          statdk: $('#siswa-update-statdk').find(":selected").val(),
          anakke: $siswaform.find( "input[name='siswa-anakke']" ).val(),
          jmlsdr: $siswaform.find( "input[name='siswa-jmlsdr']" ).val(),
          alamat: $siswaform.find( "textarea[name='siswa-alamat']" ).val(),
          kel: $siswaform.find( "input[name='siswa-kel']" ).val(),
          kec: $siswaform.find( "input[name='siswa-kec']" ).val(),
          kokab: $siswaform.find( "input[name='siswa-kokab']" ).val(),
          prov: $siswaform.find( "input[name='siswa-prov']" ).val(),
          nva: $siswaform.find( "input[name='siswa-nva']" ).val(),
          hp: $siswaform.find( "input[name='siswa-hp']" ).val(),
          email: $siswaform.find( "input[name='siswa-email']" ).val(),
          ket: $siswaform.find( "textarea[name='siswa-ket']" ).val(),
        },
        ortu: {
          id_ortu: ID_ORTU ? ID_ORTU : null,
          id_siswa: ID_SISWA,
          ayah_nama: $ortuform.find( "input[name='ortu-ayah_nama']" ).val(),
          ayah_pekerjaan: $ortuform.find( "input[name='ortu-ayah_pekerjaan']" ).val(),
          ayah_hp: $ortuform.find( "input[name='ortu-ayah_hp']" ).val(),
          ibu_nama: $ortuform.find( "input[name='ortu-ibu_nama']" ).val(),
          ibu_pekerjaan: $ortuform.find( "input[name='ortu-ibu_pekerjaan']" ).val(),
          ibu_hp: $ortuform.find( "input[name='ortu-ibu_hp']" ).val(),
          penghasilan: $ortuform.find( "input[name='ortu-penghasilan']" ).val(),
          email: $ortuform.find( "input[name='ortu-email']" ).val(),
          alamat: $ortuform.find( "textarea[name='ortu-alamat']" ).val(),
          wali_nama: $ortuform.find( "input[name='ortu-wali_nama']" ).val(),
          wali_pekerjaan: $ortuform.find( "input[name='ortu-wali_pekerjaan']" ).val(),
          wali_hp: $ortuform.find( "input[name='ortu-wali_hp']" ).val(),
          wali_alamat: $ortuform.find( "input[name='ortu-wali_alamat']" ).val(),
          wali_penghasilan: $ortuform.find( "input[name='ortu-wali_penghasilan']" ).val(),
          wali_email: $ortuform.find( "input[name='ortu-wali_email']" ).val(),
        },
        riwayat: {
          id_rs: ID_RIWAYAT ? ID_RIWAYAT : null,
          id_siswa: ID_SISWA,
          tgl_tes: $riwayatform.find( "input[name='riwayat-tgl_tes']" ).val() ? convertToDBDateFormat($riwayatform.find( "input[name='riwayat-tgl_tes']" ).val()) : null,
          asal_sekolah: $riwayatform.find( "input[name='riwayat-asal_sekolah']" ).val(),
          tahun_lulus: $riwayatform.find( "input[name='riwayat-tahun_lulus']" ).val(),
          internal_bp: $riwayatform.find( "input[name='riwayat-internal_bp']" ).val(),
          cabang_bp: $riwayatform.find( "input[name='riwayat-cabang_bp']" ).val(),
          komp_ak: $riwayatform.find( "input[name='riwayat-komp_ak']" ).val(),
          komp_nonak: $riwayatform.find( "input[name='riwayat-komp_nonak']" ).val(),
          komp_mengaji: $riwayatform.find( "input[name='riwayat-komp_mengaji']" ).val(),
          komp_tularab: $riwayatform.find( "input[name='riwayat-komp_tularab']" ).val(),
          hafalan: $riwayatform.find( "input[name='riwayat-hafalan']" ).val(),
        },
        info: {
          id_is: ID_INFO ? ID_INFO : null,
          id_siswa: ID_SISWA,
          tgl_masuk: $infoform.find( "input[name='info-tgl_masuk']" ).val() ? convertToDBDateFormat($infoform.find( "input[name='info-tgl_masuk']" ).val()) : null,
          ke_kelas: $infoform.find( "input[name='info-ke_kelas']" ).val(),
          info_bpibs: $infoform.find( "input[name='info-info_bpibs']" ).val(),
          alasan_ortu: $infoform.find( "textarea[name='info-alasan_ortu']" ).val(),
          alasan_siswa: $infoform.find( "textarea[name='info-alasan_siswa']" ).val(),
          keterangan: $infoform.find( "textarea[name='info-keterangan']" ).val(),
        }
      }
      
  $.ajax({
      async: true,
      url: SISWA_API_URL,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(request),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else showError("Gagal menyimpan perubahan!")
      },
      success: function(res) {
        showSuccess(`Mengubah data siswa berhasil!`)
        setTimeout(function() {
          window.history.go(-1);
        }, 2000);
      }
  });
}
  
function renderSiswaDetail(data){
    let siswa   = data.siswa,
        ortu    = data.ortu,
        riwayat = data.riwayat,
        info    = data.info

    ID_SISWA = siswa.id_siswa
    if(ortu) ID_ORTU = ortu.id_ortu
    if(riwayat) ID_RIWAYAT = riwayat.id_rs
    if(info) ID_INFO = info.id_is
    IMAGE_URL = siswa.foto

    let $siswaform      = $("#siswa-form"),
        $ortuform       = $("#ortu-form"),
        $riwayatform    = $("#riwayat-form"),
        $infoform       = $("#info-form")

        //siswa form
        $(`#siswa-update-foto`).dropify({
          defaultFile: siswa.full_foto
        })
        $siswaform.find( "input[name='siswa-nis']" ).val(siswa.nis)
        $siswaform.find( "input[name='siswa-nisn']" ).val(siswa.nisn)
        $siswaform.find( "input[name='siswa-nama']" ).val(siswa.nama)
        $siswaform.find( "input[name='siswa-panggilan']" ).val(siswa.panggilan)
        $('#siswa-update-kelamin').val(siswa.kelamin).change()
        $siswaform.find( "input[name='siswa-tmplhr']" ).val(siswa.tmplhr)
        $siswaform.find( "input[name='siswa-tgllhr']" ).val(convertToInputDateFormat(siswa.tgllhr))
        $siswaform.find( "input[name='siswa-agama']" ).val(siswa.agama)
        $('#siswa-update-statdk').val(siswa.statdk).change()
        $siswaform.find( "input[name='siswa-anakke']" ).val(siswa.anakke)
        $siswaform.find( "input[name='siswa-jmlsdr']" ).val(siswa.jmlsdr)
        $siswaform.find( "textarea[name='siswa-alamat']" ).val(siswa.alamat)
        $siswaform.find( "input[name='siswa-kel']" ).val(siswa.kel)
        $siswaform.find( "input[name='siswa-kec']" ).val(siswa.kec)
        $siswaform.find( "input[name='siswa-kokab']" ).val(siswa.kokab)
        $siswaform.find( "input[name='siswa-prov']" ).val(siswa.prov)
        $siswaform.find( "input[name='siswa-nva']" ).val(siswa.nva)
        $siswaform.find( "input[name='siswa-hp']" ).val(siswa.hp)
        $siswaform.find( "input[name='siswa-email']" ).val(siswa.email)
        $siswaform.find( "textarea[name='siswa-ket']" ).val(siswa.ket)
        if(ortu){
          //ortu form
          $ortuform.find( "input[name='ortu-ayah_nama']" ).val(ortu.ayah_nama)
          $ortuform.find( "input[name='ortu-ayah_pekerjaan']" ).val(ortu.ayah_pekerjaan)
          $ortuform.find( "input[name='ortu-ayah_hp']" ).val(ortu.ayah_hp)
          $ortuform.find( "input[name='ortu-ibu_nama']" ).val(ortu.ibu_nama)
          $ortuform.find( "input[name='ortu-ibu_pekerjaan']" ).val(ortu.ibu_pekerjaan)
          $ortuform.find( "input[name='ortu-ibu_hp']" ).val(ortu.ibu_hp)
          $ortuform.find( "input[name='ortu-penghasilan']" ).val(ortu.penghasilan)
          $ortuform.find( "input[name='ortu-email']" ).val(ortu.email)
          $ortuform.find( "textarea[name='ortu-alamat']" ).val(ortu.alamat)
          $ortuform.find( "input[name='ortu-wali_nama']" ).val(ortu.wali_nama)
          $ortuform.find( "input[name='ortu-wali_pekerjaan']" ).val(ortu.wali_pekerjaan)
          $ortuform.find( "input[name='ortu-wali_hp']" ).val(ortu.wali_hp)
          $ortuform.find( "input[name='ortu-wali_alamat']" ).val(ortu.wali_alamat)
          $ortuform.find( "input[name='ortu-wali_penghasilan']" ).val(ortu.wali_penghasilan)
          $ortuform.find( "input[name='ortu-wali_email']" ).val(ortu.wali_email)
        }
        if(riwayat){
          //riwayat form
          $riwayatform.find( "input[name='riwayat-tgl_tes']" ).val(riwayat.tgl_tes ? convertToInputDateFormat(riwayat.tgl_tes) : '')
          $riwayatform.find( "input[name='riwayat-asal_sekolah']" ).val(riwayat.asal_sekolah)
          $riwayatform.find( "input[name='riwayat-tahun_lulus']" ).val(riwayat.tahun_lulus)
          $riwayatform.find( "input[name='riwayat-internal_bp']" ).val(riwayat.internal_bp)
          $riwayatform.find( "input[name='riwayat-cabang_bp']" ).val(riwayat.cabang_bp)
          $riwayatform.find( "input[name='riwayat-komp_ak']" ).val(riwayat.komp_ak)
          $riwayatform.find( "input[name='riwayat-komp_nonak']" ).val(riwayat.komp_nonak)
          $riwayatform.find( "input[name='riwayat-komp_mengaji']" ).val(riwayat.komp_mengaji)
          $riwayatform.find( "input[name='riwayat-komp_tularab']" ).val(riwayat.komp_tularab)
          $riwayatform.find( "input[name='riwayat-hafalan']" ).val(riwayat.hafalan)
        }
        if(info){
          //info form
          $infoform.find( "input[name='info-tgl_masuk']" ).val(info.tgl_masuk ? convertToInputDateFormat(info.tgl_masuk) : '')
          $infoform.find( "input[name='info-ke_kelas']" ).val(info.ke_kelas)
          $infoform.find( "input[name='info-info_bpibs']" ).val(info.info_bpibs)
          $infoform.find( "textarea[name='info-alasan_ortu']" ).val(info.alasan_ortu)
          $infoform.find( "textarea[name='info-alasan_siswa']" ).val(info.alasan_siswa)
          $infoform.find( "textarea[name='info-keterangan']" ).val(info.keterangan)
        }
}

function upload_image(file){
  // $(`#siswa-create-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${SISWA_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response  = JSON.parse(res.responseText)
        let isRetry     = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          // $(`#siswa-create-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response  = res.data
        IMAGE_URL       = response.file_url
        // $(`#siswa-create-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}
