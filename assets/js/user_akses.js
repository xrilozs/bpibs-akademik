let ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  console.log("LOAD SCRIPT")

  if(!isNaN(ID_ALIAS)){
    get_menu_all()
    get_user_akses_detail()
  }

  //DATATABLE
  let user_akses_table = $('#user-akses-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${USER_AKSES_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "name",
          width: '16%',
          orderable: false
        },
        { 
          data: "email",
          width: '16%',
          orderable: false
        },
        { 
          data: "username",
          width: '16%',
          orderable: false
        },
        { 
          data: "role",
          width: '16%',
          orderable: false
        },
        {
          data: "menu",
          className: "dt-body-right",
          width: '20%',
          render: function (data, type, row, meta) {
            let badges = ''
            data.forEach((item, id) => {
              let badge = `<span class="badge badge-pill badge-dark">${item.title}</span>`
              if(id % 2){
                badge += "<br>"
              }
              badges += badge
            });
            
            return badges
          },
          orderable: false
        },
        {
          data: "id",
          className: "dt-body-right",
          width: '16%',
          render: function (data, type, row, meta) {
            let button = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}user-akses/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>`
            
            return button
          },
          orderable: false
        }
    ]
  });


  //ACTION
  $('#refresh-table').click(function(){
    user_akses_table.ajax.reload()
  })

  //FORM  
  $('#user-akses-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#user-akses-update-button')

    let $form = $(this),
        request = {
          id: $form.find( "input[name='id']" ).val(),
          user_id: $form.find( "input[name='user_id']" ).val(),
          app_id: $form.find( "input[name='app_id']" ).val(),
          role_id: $form.find( "input[name='role_id']" ).val(),
          menu_id: $("#menu-option").val().join(","),
        }

    $.ajax({
        async: true,
        url: USER_AKSES_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-akses-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Mengubah user berhasil!`)
          endLoadingButton('#user-akses-update-button', 'Simpan')
          $('#user-akses-update-modal').modal('toggle')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);        
        }
    });
  })
})

function get_user_akses_detail(){
  $.ajax({
    async: true,
    url: `${USER_AKSES_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const user    = res.data
      USER_AKSES_ID = user.id
      render_user_akses_detail(user) 
    }
  })
}

function get_menu_all(){
  $.ajax({
    async: false,
    url: `${MENU_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const menu = res.data
      render_menu(menu) 
    }
  })
}

function render_user_akses_detail(data){
  let $form = $(`#user-akses-update-form`)
  $form.find( "input[name='id']" ).val(data.user_akses_id)
  $form.find( "input[name='user_id']" ).val(data.id)
  $form.find( "input[name='app_id']" ).val(data.app_id)
  $form.find( "input[name='role_id']" ).val(data.role_id)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='username']" ).val(data.username)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "input[name='role']" ).val(data.role)
  if(data.menu_ids){
    $('#menu-option').val(data.menu_ids.split(",")).trigger('change');
  }
}

function render_menu(menu){
  let menu_html = ``
  for(const item of menu){
    let sub_menu = parseInt(item.parent) ? "(ada sub menu)" : "" 
    const item_html = `<option value="${item.id}">${item.title} ${sub_menu}</option>`
    menu_html += item_html
  }
  $('#menu-option').html(menu_html)
}