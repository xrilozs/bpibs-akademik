let USER_API_URL            = `${API_URL}/user`
let USER_ROLE_API_URL       = `${API_URL}/user-role`
let USER_AKSES_API_URL      = `${API_URL}/user-akses`
let SISWA_API_URL           = `${API_URL}/siswa`
let AJARAN_API_URL          = `${API_URL}/ajaran`
let KELAS_API_URL           = `${API_URL}/kelas`
let KELASIS_API_URL         = `${API_URL}/kelasis`
let KAMAR_API_URL           = `${API_URL}/kamar`
let KAMSIS_API_URL          = `${API_URL}/kamsis`
let GURU_API_URL            = `${API_URL}/guru`
let GURU_EDU_API_URL        = `${API_URL}/guru-edu`
let GURU_FAM_API_URL        = `${API_URL}/guru-fam`
let GURU_EDU_LVL_API_URL    = `${API_URL}/guru-edu-lvl`
let GURU_FAM_TITLE_API_URL  = `${API_URL}/guru-fam-title`
let MAPEL_API_URL           = `${API_URL}/mapel`
let KATEGORI_MAPEL_API_URL  = `${API_URL}/kategori-mapel`
let JADWAL_MAPEL_API_URL    = `${API_URL}/jadwal-mapel`
let SEMESTER_API_URL        = `${API_URL}/semester`
let HARI_API_URL            = `${API_URL}/hari`
let ABSENSI_API_URL         = `${API_URL}/absensi`
let NILAI_API_URL           = `${API_URL}/nilai`
let KATEGORI_NILAI_API_URL  = `${API_URL}/kategori-nilai`
let REPORT_SISWA_API_URL    = `${API_URL}/report-siswa`
let MENU_API_URL            = `${API_URL}/menu`
let SISWA_FILE_API_URL      = `${API_URL}/siswa-file`
let FILE_KAT_API_URL        = `${API_URL}/file-kat`
let SISWA_MUTASI_API_URL    = `${API_URL}/siswa-mutasi`
let TOAST                   = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});
let SESSION                 = localStorage.getItem("user-token")
let REFRESH_SESSION         = localStorage.getItem("user-refresh-token")
let PROFILE                 = localStorage.getItem("user-profile") ? JSON.parse(localStorage.getItem("user-profile")) : null
let MENU                    = localStorage.getItem("user-menu") ? JSON.parse(localStorage.getItem("user-menu")) : null
let RETRY_COUNT             = 0
let CURRENT_PAGE            = localStorage.getItem("current-page")
let ID_AJARAN_GLOBAL        = localStorage.getItem("tahun-ajaran")
let ID_SEMESTER_GLOBAL      = localStorage.getItem("semester")

// $('.rich-text-create').summernote({
//   placeholder: 'Type here..',
//   height: 300
// });
// $('.rich-text-update').summernote({
//   placeholder: 'Type here..',
//   height: 300
// });

$('.select2').select2()

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}login`
  }else{
    checkAuthorityPage()
  }
  
  getTahunAjaran()
  
  renderHeader()
  renderProfile()
  renderSidebar()

  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      zeroRecords: 'Tidak ada data',
      emptyTable: 'Data tidak ditemukan',
      search: "Cari",
      info: 'Total data _TOTAL_',
      processing: 'Memproses..',
      paginate: {
        first: 'Pertama',
        previous: 'Sebelumnya',
        next: 'Selanjutnya',
        last: 'Terakhir'
      },
      // Add other language options as needed
    }
  });

  $('.time24').inputmask('hh:mm', { placeholder: '__:__ _m', alias: 'time24', hourFormat: '24' });

  $('#header-ajaran-option').change(function(){
    const selectedOption  = $(this).val()
    const ajaranSemester  = selectedOption.split("|")
    ID_AJARAN_GLOBAL      = ajaranSemester[0]
    ID_SEMESTER_GLOBAL    = ajaranSemester[1]
    localStorage.setItem("tahun-ajaran", ID_AJARAN_GLOBAL)
    localStorage.setItem("semester", ID_SEMESTER_GLOBAL)
    console.log(ID_AJARAN_GLOBAL)
    console.log(ID_SEMESTER_GLOBAL)
    window.history.go(0);
  })
});

function renderHeader(){
  $('#header-username').html(PROFILE.name)
  if(PROFILE.id_guru){
    $('#header-profile').show()
  }
}

function renderProfile(){
  let $form = $('#profile-form')
  $form.find("input[name='user-name']").val(PROFILE.name)
  $form.find("input[name='user-email']").val(PROFILE.email)
  $form.find("input[name='user-role']").val(PROFILE.role)
}

function renderSidebar(){
  let sidebar_menu = ``
  MENU.forEach(item => {
    let menu_html = `<li class="${PAGE_TITLE == item.title ? 'active' : ''}">
      <a href="${WEB_URL+item.link}"><i class="${item.icon}"></i><span>${item.title}</span></a>
    </li>`
    if(parseInt(item.parent)){
      let menu_sub_html = ``
      let menu_sub_titles = "("
      item.sub.forEach(sub => {
        let title = sub.title.toLowerCase() == 'data master' ? item.title : sub.title
        if(menu_sub_titles != "("){
          menu_sub_titles += ` || '${PAGE_TITLE}' == '${title}'`
        }else{
          menu_sub_titles += `'${PAGE_TITLE}' == '${title}'`
        }
        let sub_html = `<li><a href="${WEB_URL+sub.link}">${sub.title}</a></li>`
        menu_sub_html += sub_html
      });

      menu_sub_titles += ")"

      menu_html = `<li class="${eval(menu_sub_titles) ? 'active' : ''}">
        <a href="#" class="has-arrow" aria-expanded="false"><i class="${item.icon}"></i><span>${item.title}</span></a>
        <ul aria-expanded="false">
          ${menu_sub_html}
        </ul>
      </li>`
    }

    sidebar_menu += menu_html
  });
  $('#sidebar-menu').html(sidebar_menu)
  $('#sidebar-menu').metisMenu()
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-profile", JSON.stringify(data.user));
  localStorage.setItem("user-menu", JSON.stringify(data.menu));
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-profile");
  localStorage.removeItem("user-menu");
  window.location.href = `${WEB_URL}login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}/user/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  console.log("RETRY: ", RETRY_COUNT)
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function checkAuthorityPage(){
  if(PAGE_TITLE == 'Profilku') return

  let subMenu = []
  MENU.forEach(item => {
    if(item.sub){
      subMenu = subMenu.concat(item.sub)
    }
  });
  let menuIndex = MENU.findIndex(x => x.title == PAGE_TITLE)
  if(menuIndex < 0){
    let subMenuIndex = subMenu.findIndex(x => x.title == PAGE_TITLE)
    if(subMenuIndex < 0){
      window.location.href = `${WEB_URL}${MENU[0].link}`      
    }
  }
}

function formatRupiah(angka, prefix){
  var angkaStr = angka.replace(/[^,\d]/g, '').toString(),
      split = angkaStr.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDate(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr = datetimeArr[0]
  let arr = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function convertToDBDateFormat(inputDate) {
  // Split the input date into day, month, and year
  var dateParts = inputDate.split('/');
  
  // Create a new Date object using the parsed values
  var originalDate = new Date(dateParts[2], dateParts[0] - 1, dateParts[1]);
  
  // Extract the year, month, and day from the Date object
  var year = originalDate.getFullYear();
  var month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
  var day = originalDate.getDate().toString().padStart(2, '0');
  
  // Assemble the new date string in "yyyy-mm-dd" format
  var convertedDate = year + '-' + month + '-' + day;
  
  return convertedDate;
}

function convertToDBDateFormatV2(inputDate) {
  // Split the input date into day, month, and year
  var dateParts = inputDate.split('/');
  
  // Create a new Date object using the parsed values
  var originalDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
  
  // Extract the year, month, and day from the Date object
  var year = originalDate.getFullYear();
  var month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
  var day = originalDate.getDate().toString().padStart(2, '0');
  
  // Assemble the new date string in "yyyy-mm-dd" format
  var convertedDate = year + '-' + month + '-' + day;
  
  return convertedDate;
}

function convertToInputDateFormat(dbdate) {
  // Split the input date into year, month, and day
  var dateParts = dbdate.split('-');
  
  // Create a new Date object using the parsed values
  var originalDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
  
  // Extract the day, month, and year from the Date object
  var day = originalDate.getDate().toString().padStart(2, '0');
  var month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
  var year = originalDate.getFullYear();
  
  // Assemble the new date string in "dd/mm/yyyy" format
  var convertedDate = month + '/' + day + '/' + year;
  
  return convertedDate;
}

function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function sortText(text){
  return text.length > 20 ? text.slice(0, 20) + ".." : text
}

$("#logout-button").click(function(){    
  removeSession()
})

$("#header-password-button").click(function(e){
  let $form = $("#password-form" )
  $form.find( "input[name='password']" ).val('')
  $form.find( "input[name='confirm']" ).val('')
})

$("#password-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#password-button")

  let $form     = $( this ),
      password  = $form.find( "input[name='password']" ).val(),
      confirm   = $form.find( "input[name='confirm']" ).val()

  if(password != confirm){
    showError("Kata sandi tidak sama")
    endLoadingButton("#password-button", "Ganti")
    return
  }
  
  $.ajax({
      async: true,
      url: `${API_URL}/user/change-password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        password: password
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#password-button', 'Ganti')
        let is_retry = retryRequest(response)
        if(is_retry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#password-button', 'Ganti')
        $('#password-modal').modal('hide')
      }
  });
})

$(document).on('keydown', '.input-decimal', function(e){
  console.log("input decimal")
  let input = $(this);
  let oldVal = input.val();
  let regex
  let attr = input.attr('pattern')
  if (typeof attr !== 'undefined' && attr !== false) {
    regex = new RegExp(input.attr('pattern'), 'g');
  }else{
    regex = new RegExp(/^\s*-?(\d+(\.\d{1,2})?|\.\d{1,2})\s*$/, 'g')
  }
  
  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).on('change', '.input-currency', function(e){
  var oldVal = $(this).val();
  $(this).val(formatRupiah(oldVal))
});

function getTahunAjaran(){
  $.ajax({
    async: true,
    url: `${AJARAN_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      endLoadingButton('#password-button', 'Ganti')
      let is_retry = retryRequest(response)
      if(is_retry) $.ajax(this)
    },
    success: function(res) {
      renderAjaranHeaderOption(res.data)
    }
  });
}

function renderAjaranHeaderOption(data){
  let options_html = ``
  if(!ID_AJARAN_GLOBAL || !ID_SEMESTER_GLOBAL){
    let selectedTahunAjaran = data.find(item => parseInt(item.aktif))
    if(selectedTahunAjaran){
      ID_AJARAN_GLOBAL    = selectedTahunAjaran.id_ajaran
      ID_SEMESTER_GLOBAL  = 1
      localStorage.setItem("tahun-ajaran", ID_AJARAN_GLOBAL)
      localStorage.setItem("semester", ID_SEMESTER_GLOBAL)
    }
  }
  let selected = `${ID_AJARAN_GLOBAL}|${ID_SEMESTER_GLOBAL}`

  const semester          = [{name:"Ganjil", value:1}, {name:"Genap", value:2}]
  let ajaranSemesterList  = []
  data.forEach(item => {
    semester.forEach(x => {
      let newItem                = Object.assign({}, item);
      newItem.ajaran_semester    = `${newItem.thn_ajaran} [${x.name}]`
      newItem.id_ajaran_semester = `${newItem.id_ajaran}|${x.value}`
      ajaranSemesterList.push(newItem)
    })
  })

  for(let item of ajaranSemesterList){
    let isSelected    = item.id_ajaran_semester == selected ? 'selected' : ''
    const option_html = `<option value="${item.id_ajaran_semester}" ${isSelected}>${item.ajaran_semester}</option>`
    options_html      += option_html
  }

  $('#header-ajaran-option').html(options_html)
}