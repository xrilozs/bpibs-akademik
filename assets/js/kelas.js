let KELAS_ID,
    AJARAN = [],
    ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  console.log("LOAD SCRIPT")
  if ($('.kelas-ajaran-option').length > 0) {
    get_ajaran()
  }

  if(!isNaN(ID_ALIAS)){
    get_kelas_detail()
  }

  //DATATABLE
  let kelas_table = $('#kelas-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${KELAS_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        newObj.id_ajaran = ID_AJARAN_GLOBAL
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "thn_ajaran",
          orderable: false
        },
        { 
          data: "kdkelas",
          orderable: false
        },
        { 
          data: "kelas",
          orderable: false
        },
        { 
          data: "tipe",
          orderable: false
        },
        {
          data: "createdt",
          orderable: false
        },
        {
          data: "id_kelas",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let button = `<a class="btn btn-sm btn-primary" href="${WEB_URL}kelas/update/${data}" title="ubah">
              <i class="fa fa-edit"></i>
            </a>`
            
            return button
          },
          orderable: false
        }
    ]
  });


  //ACTION
  $('#refresh-table').click(function(){
    kelas_table.ajax.reload()
  })

  //FORM
  $('#kelas-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#kelas-create-button')
    
    let $form = $(this),
        request = {
          kdkelas: $form.find( "input[name='kdkelas']" ).val(),
          kelas: $form.find( "input[name='kelas']" ).val(),
          tipe: $('#kelas-create-tipe').find(":selected").val(),
          id_ajaran: $('#kelas-create-ajaran').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: KELAS_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kelas-create-button', 'Create')
        },
        success: function(res) {
          showSuccess(`Membuat data kelas baru berhasil!`)
          endLoadingButton('#kelas-create-button', 'Create')
          $('#kelas-create-modal').modal('hide')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);
        }
    });
  })
  
  $('#kelas-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#kelas-update-button')

    let $form = $(this),
        request = {
          id_kelas: KELAS_ID,
          kdkelas: $form.find( "input[name='kdkelas']" ).val(),
          kelas: $form.find( "input[name='kelas']" ).val(),
          tipe: $('#kelas-update-tipe').find(":selected").val(),
          id_ajaran: $('#kelas-update-ajaran').find(":selected").val()
        }

    $.ajax({
        async: true,
        url: KELAS_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kelas-update-button', 'Update')
        },
        success: function(res) {
          showSuccess(`Mengubah data kelas berhasil!`)
          endLoadingButton('#kelas-update-button', 'Update')
          $('#kelas-update-modal').modal('toggle')
          setTimeout(function() {
            // Redirect to the desired page
            window.history.go(-1);
          }, 2000);        
        }
    });
  })
})

function get_ajaran(){
  $.ajax({
    async: true,
    url: `${AJARAN_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_ajaran_option(res.data) 
    }
  })
}

function get_kelas_detail(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas     = res.data
      KELAS_ID        = kelas.id_kelas
      render_kelas_detail(kelas) 
    }
  })
}

function render_kelas_detail(data){
  let $form = $(`#kelas-update-form`)
  $form.find( "input[name='kdkelas']" ).val(data.kdkelas)
  $form.find( "input[name='kelas']" ).val(data.kelas)
  $(`#kelas-update-tipe`).val(data.tipe).change()
  $(`#kelas-update-ajaran`).val(data.id_ajaran).change()
}

function render_ajaran_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_ajaran}">${item.thn_ajaran}</option>`
    options_html += option_html
  }

  $('.kelas-ajaran-option').html(options_html)
}