let ID_ALIAS  = window.location.pathname.split("/").pop()

$(document).ready(function(){
  get_semester()
  get_kelas()
  get_mapel()
  get_hari()
  getJadwalMapelDetail()

  $('#jadwal-mapel-update-form').submit(function(e){
    e.preventDefault()

    startLoadingButton('#jadwal-mapel-update-button')

    let $form   = $(this),
        request = {
          id_jadwal: ID_ALIAS,
          id_ajaran: ID_AJARAN_GLOBAL,
          id_semester: $('#jadwal-mapel-update-semester').find(":selected").val(),
          id_kelas: $('#jadwal-mapel-update-kelas').find(":selected").val(),
          id_mapel: $('#jadwal-mapel-update-mapel').find(":selected").val(),
          id_hari: $('#jadwal-mapel-update-hari').find(":selected").val(),
          awal: $form.find( "input[name='awal']" ).val(),
          akhir:  $form.find( "input[name='akhir']" ).val()
        }
        
    $.ajax({
        async: true,
        url: JADWAL_MAPEL_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Ubah data jadwal mata pelajaran gagal!")
            endLoadingButton('#jadwal-mapel-update-button', "Simpan")
          }
        },
        success: function(res) {
          showSuccess("Ubah data jadwal mata pelajaran sukses!")
          endLoadingButton('#jadwal-mapel-update-button', "Simpan")
          setTimeout(function() {
            window.history.go(-1);
          }, 2000);
        }
    });
  })
})

function getJadwalMapelDetail(){
  $.ajax({
    async: true,
    url: `${JADWAL_MAPEL_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal medapatkan detail jadwal mata pelajaran!")
    },
    success: function(res) {
      const jadwalMapel = res.data
      renderJadwalMapelDetail(jadwalMapel) 
    }
  })
}

function renderJadwalMapelDetail(data){
  $form = $('#jadwal-mapel-update-form')
  
  $('#jadwal-mapel-update-semester').val(data.id_semester).change()
  $('#jadwal-mapel-update-kelas').val(data.id_kelas).change()
  $('#jadwal-mapel-update-mapel').val(data.id_mapel).change()
  $('#jadwal-mapel-update-hari').val(data.id_hari).change()
  $form.find( "input[name='awal']" ).val(data.awal)
  $form.find( "input[name='akhir']" ).val(data.akhir)
}

function get_semester(){
  $.ajax({
    async: true,
    url: `${SEMESTER_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const semester = res.data
      render_semester_option(semester) 
    }
  })
}

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function get_mapel(){
  $.ajax({
    async: true,
    url: `${MAPEL_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const mapel = res.data
      render_mapel_option(mapel) 
    }
  })
}

function get_hari(){
  $.ajax({
    async: true,
    url: `${HARI_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const hari = res.data
      render_hari_option(hari) 
    }
  })
}

function render_semester_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_semester}">${item.semester}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-semester-option').html(options_html)
}

function render_kelas_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-kelas-option').html(options_html)
}

function render_mapel_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_mapel}">${item.mapel}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-mapel-option').html(options_html)
}

function render_hari_option(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id_hari}">${item.hari}</option>`
    options_html += option_html
  }

  $('.jadwal-mapel-hari-option').html(options_html)
}