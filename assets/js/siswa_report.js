const ID_ALIAS = window.location.pathname.split("/").pop()

$(document).ready(function(){
    getSiswaReport()
})

function getSiswaReport(){
    $.ajax({
        async: true,
        url: `${REPORT_SISWA_API_URL}/nilai`,
        type: 'GET',
        data: {
            id_siswa: ID_ALIAS,
            id_semester: ID_SEMESTER_GLOBAL,
            id_ajaran: ID_AJARAN_GLOBAL
        },
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          renderReportNilai(res.data)
        }
    });
}

function renderReportNilai(report){
  const {task, mapel, nilai} = report

  let tableHeaderHtml = `<td>No.</td>
  <td>Mata Pelajaran\\Tugas</td>`
  task.forEach(item => {
    tableHeaderHtml += `<td>${item.kategori}</td>`
  });

  let tableBodyHtml = ``
  mapel.forEach((item, i) => {
    const nilaiMapel = nilai[item.mapel]
    if(!nilaiMapel) return

    let columnHtml = `<td>${i+1}</td>
    <td>${item.mapel}</td>`
    task.forEach(item => {
      let nilaiTugas = nilaiMapel.find(x => x.tugas_nama == item.kategori)
      columnHtml += `<td>${nilaiTugas ? nilaiTugas.nilai ? nilaiTugas.nilai : "-" : "-"}</td>`
    });
    
    let rowHtml = `<tr>
      ${columnHtml}
    </tr>`

    tableBodyHtml += rowHtml
  })

  const reportTableHtml = `<table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        ${tableHeaderHtml}
      </tr>
    </thead>
    <tbody>
      ${tableBodyHtml}
    </tbody>
  </table>`

  const reportSectionHtml = `<div class="row">
    <div class="col-12">
      ${reportTableHtml}
    </div>
  </div>`

  $('#report-section').html(reportSectionHtml)
}