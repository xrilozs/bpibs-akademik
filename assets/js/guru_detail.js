const ID_ALIAS  = window.location.pathname.split("/").pop()
let SELECTED_ID

$(document).ready(function(){
    console.log("LOAD SCRIPT")
    get_guru_edu_lvl()
    get_guru_fam_title()
    getGuruDetail()

    //DATATABLE
    let guru_edu_table = $('#guru-edu-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,   
      ajax: {
        async: true,
        url: `${GURU_EDU_API_URL}/by-guru/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "level",
            orderable: false
          },
          { 
            data: "sekolah",
            orderable: false
          },
          { 
            data: "jurusan",
            orderable: false
          },
          { 
            data: "thnlulus",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-primary guru-edu-edit-toggle" title="update"  data-id="${data}" data-toggle="modal" data-target="#guru-edu-edit-modal">
                <i class="fa fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger guru-edu-delete-toggle" title="delete" data-id="${data}" data-toggle="modal" data-target="#guru-edu-delete-modal">
                <i class="fa fa-times"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
    });

    let guru_fam_table = $('#guru-fam-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,
      ajax: {
        async: true,
        url: `${GURU_FAM_API_URL}/by-guru/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "hubungan",
            orderable: false
          },
          { 
            data: "f_name",
            orderable: false
          },
          { 
            data: "f_dob",
            orderable: false
          },
          { 
            data: "f_edu",
            orderable: false
          },
          { 
            data: "f_ethnic",
            orderable: false
          },
          { 
            data: "f_job",
            orderable: false
          },
          { 
            data: "f_phone",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-primary guru-fam-edit-toggle" title="update"  data-id="${data}" data-toggle="modal" data-target="#guru-fam-edit-modal">
                <i class="fa fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger guru-fam-delete-toggle" title="delete" data-id="${data}" data-toggle="modal" data-target="#guru-fam-delete-modal">
                <i class="fa fa-times"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
    });

    $("body").delegate(".guru-fam-delete-toggle", "click", function() {
      SELECTED_ID = $(this).data('id')
    })
    $("body").delegate(".guru-edu-delete-toggle", "click", function() {
      SELECTED_ID = $(this).data('id')
    })
    
    $("body").delegate(".guru-fam-edit-toggle", "click", function() {
      clearFamForm()
      SELECTED_ID = $(this).data('id')
      getFamDetail()
    })
    
    $("body").delegate(".guru-edu-edit-toggle", "click", function() {
      clearEduForm()
      SELECTED_ID = $(this).data('id')
      getEduDetail()
    })

    $('#guru-fam-delete-button').click(function (){
      startLoadingButton('#guru-fam-delete-button')
      
      $.ajax({
          async: true,
          url: `${GURU_FAM_API_URL}/delete/${SELECTED_ID}`,
          type: 'DELETE',
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#guru-fam-delete-button', 'Iya')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#guru-fam-delete-button', 'Iya')
            $('#guru-fam-delete-modal').modal('toggle')
            guru_fam_table.ajax.reload()
          }
      });
    })

    $('#guru-edu-delete-button').click(function (){
      startLoadingButton('#guru-edu-delete-button')
      
      $.ajax({
          async: true,
          url: `${GURU_EDU_API_URL}/delete/${SELECTED_ID}`,
          type: 'DELETE',
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#guru-edu-delete-button', 'Iya')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#guru-edu-delete-button', 'Iya')
            $('#guru-edu-delete-modal').modal('toggle')
            guru_edu_table.ajax.reload()
          }
      });
    })
    
    $('#guru-edu-edit-form').submit(function (e){
      e.preventDefault()
      startLoadingButton('#guru-edu-edit-button')
  
      const $form = $(this),
            request = {
              id: SELECTED_ID,
              idguru: ID_ALIAS,
              idlvl: $('#guru-edu-lvl-option').find(":selected").val(),
              sekolah: $form.find( "input[name='sekolah']" ).val(),
              jurusan: $form.find( "input[name='jurusan']" ).val(),
              thnlulus: $form.find( "input[name='thnlulus']" ).val(),
            }
      
      $.ajax({
          async: true,
          url: `${GURU_EDU_API_URL}`,
          type: 'PUT',
          data: JSON.stringify(request),
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#guru-edu-edit-button', 'Simpan')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#guru-edu-edit-button', 'Simpan')
            $('#guru-edu-edit-modal').modal('toggle')
            guru_edu_table.ajax.reload()
          }
      });
    })
  
    $('#guru-fam-edit-form').submit(function (e){
      e.preventDefault()
      startLoadingButton('#guru-fam-edit-button')
  
      const $form = $(this),
            request = {
              id: SELECTED_ID,
              idguru: ID_ALIAS,
              idfam: $('#guru-fam-title-option').find(":selected").val(),
              f_name: $form.find( "input[name='f_name']" ).val(),
              f_dob: convertToDBDateFormat($form.find( "input[name='f_dob']" ).val()),
              f_edu: $form.find( "input[name='f_edu']" ).val(),
              f_ethnic: $form.find( "input[name='f_ethnic']" ).val(),
              f_job: $form.find( "input[name='f_job']" ).val(),
              f_phone: $form.find( "input[name='f_phone']" ).val(),
            }
      
      $.ajax({
          async: true,
          url: `${GURU_FAM_API_URL}`,
          type: 'PUT',
          data: JSON.stringify(request),
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#guru-fam-edit-button', 'Simpan')
          },
          success: function(res) {
            showSuccess(res.message)
            endLoadingButton('#guru-fam-edit-button', 'Simpan')
            $('#guru-fam-edit-modal').modal('toggle')
            guru_fam_table.ajax.reload()
          }
      });
    })
})

function getGuruDetail(){
    $.ajax({
      async: true,
      url: `${GURU_API_URL}/by-id/${ID_ALIAS}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else showError("Get Detail Guru Failed!")
      },
      success: function(res) {
        const data = res.data
        renderGuruDetail(data.guru) 
        // renderGuruEdu(data.edu) 
        // renderGuruFam(data.fam) 
      }
    })
}

function getEduDetail(){
  $.ajax({
    async: true,
    url: `${GURU_EDU_API_URL}/by-id/${SELECTED_ID}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal mendapatkan detail edukasi guru !")
    },
    success: function(res) {
      renderEduDetail(res.data)
    }
  })
}

function getFamDetail(){
  $.ajax({
    async: true,
    url: `${GURU_FAM_API_URL}/by-id/${SELECTED_ID}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal mendapatkan detail keluarga guru !")
    },
    success: function(res) {
      renderFamDetail(res.data)
    }
  })
}
  
function renderGuruDetail(data){
  let $form = $("#guru-detail-form")
    $form.find( "input[name='nid']" ).val(data.nid)
    $form.find( "input[name='npp']" ).val(data.npp)
    $form.find( "input[name='nama']" ).val(data.npp)
    $form.find( "input[name='tlhr']" ).val(data.tlhr)
    $form.find( "input[name='tgllhr']" ).val(convertToInputDateFormat(data.tgllhr))
    // $('#guru-update-kelamin').val(data.kelamin).change()
    $form.find( "input[name='kelamin']" ).val(data.kelamin ? data.kelamin == 'L' ? 'Laki-laki' : 'Perempuan' : '-')
    $form.find( "input[name='email']" ).val(data.email)
    $form.find( "input[name='hp']" ).val(data.hp)
    $form.find( "input[name='nikah']" ).val(parseInt(data.nikah) ? "Sudah Menikah" : 'Belum Menikah')
    $form.find( "input[name='tb']" ).val(`${data.tb} CM`)
    $form.find( "input[name='bb']" ).val(`${data.bb} KG`)
    $form.find( "input[name='goldar']" ).val(data.goldar)
    $form.find( "input[name='bj']" ).val(data.bj)
    $form.find( "input[name='tgljoin']" ).val(convertToInputDateFormat(data.tgljoin))
    $('#guru-detail-foto').html(`<img src="${data.full_foto}" class="img" style="height:200px; width:100%; object-fit:cover" />`)
}

function renderEduDetail(edu){
  const $form = $('#guru-edu-edit-form')
  $('#guru-edu-lvl-option').val(edu.idlvl).change()
  $form.find( "input[name='sekolah']" ).val(edu.sekolah)
  $form.find( "input[name='jurusan']" ).val(edu.jurusan)
  $form.find( "input[name='thnlulus']" ).val(edu.thnlulus)
}
function renderFamDetail(fam){
  const $form = $('#guru-fam-edit-form')
  $('#guru-fam-title-option').val(fam.idfam)
  $form.find( "input[name='f_name']" ).val(fam.f_name)
  $form.find( "input[name='f_dob']" ).val(convertToInputDateFormat(fam.f_dob))
  $form.find( "input[name='f_edu']" ).val(fam.f_edu)
  $form.find( "input[name='f_ethnic']" ).val(fam.f_ethnic)
  $form.find( "input[name='f_job']" ).val(fam.f_job)
  $form.find( "input[name='f_phone']" ).val(fam.f_phone)
}

function get_guru_edu_lvl(){
  $.ajax({
    async: true,
    url: `${GURU_EDU_LVL_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_guru_edu_lvl_option(res.data) 
    }
  })
}

function get_guru_fam_title(){
  $.ajax({
    async: true,
    url: `${GURU_FAM_TITLE_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_guru_fam_title_option(res.data) 
    }
  })
}

function render_guru_edu_lvl_option(options){
  let options_html = ``
  options.forEach(option => {
      let option_html = `<option value="${option.id}">${option.level}</option>`
      options_html += option_html
  });

  $('#guru-edu-lvl-option').html(options_html)
}

function render_guru_fam_title_option(options){
  let options_html = ``
  options.forEach(option => {
      let option_html = `<option value="${option.id}">${option.hubungan}</option>`
      options_html += option_html
  });

  $('#guru-fam-title-option').html(options_html)
}

function clearFamForm(){
  $("#guru-fam-edit-form")[0].reset()
}

function clearEduForm(){
  $("#guru-edu-edit-form")[0].reset()
}