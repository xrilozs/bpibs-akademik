let ID_ALIAS  = window.location.pathname.split("/").pop(),
    IMAGE_URL

$(document).ready(function(){
  getGuruDetail()

  //EVENT
  $('#guru-update-foto').change(function(e) {
    let file = e.target.files[0];
    upload_image(file)
  });

  $('#guru-update-form').submit(function(e){
    e.preventDefault()

    if(!IMAGE_URL){
      showError("Foto belum diupload!")
      return
    }

    startLoadingButton('#guru-update-button')

    let $form   = $(this),
        request = {
          id: ID_ALIAS,
          nid: $form.find( "input[name='nid']" ).val(),
          npp: $form.find( "input[name='npp']" ).val(),
          nama: $form.find( "input[name='nama']" ).val(),
          tlhr: $form.find( "input[name='tlhr']" ).val(),
          tgllhr: convertToDBDateFormat($form.find( "input[name='tgllhr']" ).val()),
          kelamin:  $('#guru-update-kelamin').find(":selected").val(),
          email: $form.find( "input[name='email']" ).val(),
          foto: IMAGE_URL,
          hp: $form.find( "input[name='hp']" ).val(),
          nikah: $('#guru-update-nikah').find(":selected").val(),
          tb: $form.find( "input[name='tb']" ).val(),
          bb: $form.find( "input[name='bb']" ).val(),
          goldar: $form.find( "input[name='goldar']" ).val(),
          bj: $form.find( "input[name='bj']" ).val(),
          tgljoin: convertToDBDateFormat($form.find( "input[name='tgljoin']" ).val()),
        }
        
    $.ajax({
        async: true,
        url: GURU_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Gagal ubah data guru")
            endLoadingButton('#guru-update-button', "Simpan")
          }
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#guru-update-button', "Simpan")
          setTimeout(function() {
            window.history.go(-1);
          }, 2000);
        }
    });
  })
})

function getGuruDetail(){
  $.ajax({
    async: true,
    url: `${GURU_API_URL}/by-id/${ID_ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal medapatkan detail guru!")
    },
    success: function(res) {
      const guru = res.data.guru
      renderGuruDetail(guru) 
    }
  })
}

function upload_image(file){
  $(`#guru-update-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${GURU_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#guru-update-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMAGE_URL = response.file_url
        $(`#guru-update-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderGuruDetail(guru){
  IMAGE_URL = guru.foto
  $form = $('#guru-update-form')

  $form.find( "input[name='nid']" ).val(guru.nid)
  $form.find( "input[name='npp']" ).val(guru.npp)
  $form.find( "input[name='nama']" ).val(guru.nama)
  $form.find( "input[name='tlhr']" ).val(guru.tlhr)
  $form.find( "input[name='tgllhr']" ).val(convertToInputDateFormat(guru.tgllhr))
  $('#guru-update-kelamin').val(guru.kelamin).change()
  $form.find( "input[name='email']" ).val(guru.email)
  $(`#guru-update-foto`).attr("data-default-file", guru.full_foto)
  $(`#guru-update-foto`).dropify({
    defaultFile: guru.full_foto
  })
  $form.find( "input[name='hp']" ).val(guru.hp)
  $('#guru-update-nikah').val(guru.nikah).change()
  $form.find( "input[name='tb']" ).val(guru.tb)
  $form.find( "input[name='bb']" ).val(guru.bb)
  $form.find( "input[name='goldar']" ).val(guru.goldar)
  $form.find( "input[name='bj']" ).val(guru.bj)
  $form.find( "input[name='tgljoin']" ).val(convertToInputDateFormat(guru.tgljoin))
}