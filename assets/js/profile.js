const ID_ALIAS  = PROFILE.id_guru

$(document).ready(function(){
    console.log("LOAD SCRIPT")
    getGuruDetail()

    //DATATABLE
    $('#guru-edu-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,   
      ajax: {
        async: true,
        url: `${GURU_EDU_API_URL}/by-guru/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "level",
            orderable: false
          },
          { 
            data: "sekolah",
            orderable: false
          },
          { 
            data: "jurusan",
            orderable: false
          },
          { 
            data: "thnlulus",
            orderable: false
          }
      ]
    });

    $('#guru-fam-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: false,
      paging: false,
      info: false,
      ajax: {
        async: true,
        url: `${GURU_FAM_API_URL}/by-guru/${ID_ALIAS}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "hubungan",
            orderable: false
          },
          { 
            data: "f_name",
            orderable: false
          },
          { 
            data: "f_dob",
            orderable: false
          },
          { 
            data: "f_edu",
            orderable: false
          },
          { 
            data: "f_ethnic",
            orderable: false
          },
          { 
            data: "f_job",
            orderable: false
          },
          { 
            data: "f_phone",
            orderable: false
          },
      ]
    });
})

function getGuruDetail(){
    $.ajax({
      async: true,
      url: `${GURU_API_URL}/by-id/${ID_ALIAS}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        console.log("ERROR ", res)
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else showError("Get Detail Guru Failed!")
      },
      success: function(res) {
        const data = res.data
        renderGuruDetail(data.guru) 
      }
    })
}
  
function renderGuruDetail(data){
  let $form = $("#guru-detail-form")
    $form.find( "input[name='nid']" ).val(data.nid)
    $form.find( "input[name='npp']" ).val(data.npp)
    $form.find( "input[name='nama']" ).val(data.npp)
    $form.find( "input[name='tlhr']" ).val(data.tlhr)
    $form.find( "input[name='tgllhr']" ).val(convertToInputDateFormat(data.tgllhr))
    // $('#guru-update-kelamin').val(data.kelamin).change()
    $form.find( "input[name='kelamin']" ).val(data.kelamin ? data.kelamin == 'L' ? 'Laki-laki' : 'Perempuan' : '-')
    $form.find( "input[name='email']" ).val(data.email)
    $form.find( "input[name='hp']" ).val(data.hp)
    $form.find( "input[name='nikah']" ).val(parseInt(data.nikah) ? "Sudah Menikah" : 'Belum Menikah')
    $form.find( "input[name='tb']" ).val(`${data.tb} CM`)
    $form.find( "input[name='bb']" ).val(`${data.bb} KG`)
    $form.find( "input[name='goldar']" ).val(data.goldar)
    $form.find( "input[name='bj']" ).val(data.bj)
    $form.find( "input[name='tgljoin']" ).val(convertToInputDateFormat(data.tgljoin))
    $('#guru-detail-foto').html(`<img src="${data.full_foto}" class="img" style="height:200px; width:100%; object-fit:cover" />`)
}