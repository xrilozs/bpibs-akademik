let IMAGE_URL

$(document).ready(function(){
  //EVENT
  $('#guru-create-foto').change(function(e) {
    let file = e.target.files[0];
    upload_image(file)
  });

  $('#guru-create-form').submit(function(e){
    e.preventDefault()

    if(!IMAGE_URL){
      showError("Foto belum diupload!")
      return
    }

    startLoadingButton('#guru-create-button')

    let $form   = $(this),
        request = {
          nid: $form.find( "input[name='nid']" ).val(),
          npp: $form.find( "input[name='npp']" ).val(),
          nama: $form.find( "input[name='nama']" ).val(),
          tlhr: $form.find( "input[name='tlhr']" ).val(),
          tgllhr: convertToDBDateFormat($form.find( "input[name='tgllhr']" ).val()),
          kelamin:  $('#guru-create-kelamin').find(":selected").val(),
          email: $form.find( "input[name='email']" ).val(),
          foto: IMAGE_URL,
          hp: $form.find( "input[name='hp']" ).val(),
          nikah: $('#guru-create-nikah').find(":selected").val(),
          tb: $form.find( "input[name='tb']" ).val(),
          bb: $form.find( "input[name='bb']" ).val(),
          goldar: $form.find( "input[name='goldar']" ).val(),
          bj: $form.find( "input[name='bj']" ).val(),
          tgljoin: convertToDBDateFormat($form.find( "input[name='tgljoin']" ).val()),
        }
        
    $.ajax({
        async: true,
        url: GURU_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Create guru failed")
            endLoadingButton('#guru-create-button', "Simpan")
          }
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#guru-create-button', "Simpan")
          setTimeout(function() {
            window.history.go(-1);
          }, 2000);
        }
    });
  })
})

function upload_image(file){
  $(`#guru-create-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${GURU_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#guru-create-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMAGE_URL = response.file_url
        $(`#guru-create-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}
