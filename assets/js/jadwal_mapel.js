let KELAS_ID,
    JADWAL_MAPEL_ID

$(document).ready(function(){
  console.log("LOAD SCRIPT")

  get_kelas()

  //DATATABLE
  let jadwal_mapel_table = $('#jadwal-mapel-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    ajax: {
      async: true,
      url: `${JADWAL_MAPEL_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        newObj.page_number  = d.start > 0 ? (start/size) : 0;
        newObj.page_size    = size
        newObj.search       = d.search.value
        newObj.draw         = d.draw
        newObj.id_kelas     = KELAS_ID 
        newObj.id_ajaran    = ID_AJARAN_GLOBAL
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    columns: [
        { 
          data: "semester",
          orderable: false
        },
        { 
          data: "kelas",
          orderable: false
        },
        { 
          data: "mapel",
          orderable: false
        },
        { 
          data: "hari",
          orderable: false
        },
        { 
          data: "awal",
          orderable: false
        },
        { 
          data: "akhir",
          orderable: false
        },
        {
          data: "createdt",
          orderable: false
        },
        {
          data: "id_jadwal",
          className: "dt-body-right",
          render: function (data, type, row, meta) {
            let actionList = `<a class="dropdown-item bg-primary text-white" href="${WEB_URL}jadwal-mapel/update/${data}" title="ubah">
              <i class="fa fa-edit"></i> Ubah
            </a>
            <a class="dropdown-item bg-secondary text-white" href="${WEB_URL}jadwal-mapel/detail/${data}" title="detail">
              <i class="fa fa-search"></i> Detail
            </a>
            <a class="dropdown-item bg-danger text-white" href="${WEB_URL}jadwal-mapel/delete/${data}" title="hapus">
              <i class="fa fa-times"></i> Hapus
            </a>`

            let button = `<div class="dropdown dropleft">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                Action
              </button>
              <div class="dropdown-menu py-0">
                ${actionList}
              </div>
            </div>`
            
            return button
          },
          orderable: false
        }
    ]
  });

  //ACTION
  $('#refresh-table').click(function(){
    jadwal_mapel_table.ajax.reload()
  })

  $('#filter-kelas-option').change(function(){
    KELAS_ID = $(this).val()
    jadwal_mapel_table.ajax.reload()
  })

  $("body").delegate(".jadwal-mapel-delete-toggle", "click", function(e) {
    JADWAL_MAPEL_ID = $(this).data('id')
  })

  $('#jadwal-mapel-delete-button').click(function(){
    startLoadingButton('#jadwal-mapel-delete-button')
    
    $.ajax({
        async: true,
        url: `${JADWAL_MAPEL_API_URL}/delete/${JADWAL_MAPEL_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#jadwal-mapel-delete-button', 'Ya')
        },
        success: function(res) {
          showSuccess(`Hapus jadwal mata pelajaran berhasil!`)
          endLoadingButton('#jadwal-mapel-delete-button', 'Ya')
          $('#jadwal-mapel-delete-modal').modal('toggle')
          jadwal_mapel_table.ajax.reload()
        }
    });
  })
})

function get_kelas(){
  $.ajax({
    async: true,
    url: `${KELAS_API_URL}/all?id_ajaran=${ID_AJARAN_GLOBAL}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("ERROR ", res)
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const kelas = res.data
      render_kelas_option(kelas) 
    }
  })
}

function render_kelas_option(data){
  let options_html = `<option value="">Semua</option>`
  for(let item of data){
    const option_html = `<option value="${item.id_kelas}">${item.kelas}</option>`
    options_html += option_html
  }

  $('#filter-kelas-option').html(options_html)
}
