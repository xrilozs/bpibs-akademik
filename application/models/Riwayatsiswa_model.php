<?php
  class Riwayatsiswa_model extends CI_Model{
    function create_riwayatsiswa($data){
      $this->db->insert('t_riwayatsiswa', $data);
      return $this->db->affected_rows();
    }

    function update_riwayatsiswa($data){
      $this->db->update('t_riwayatsiswa', $data, array("id_rs" => $data['id_rs']));
      return $this->db->affected_rows();
    }

    function get_riwayatsiswa_by_id_siswa($id_siswa){
      $this->db->where("id_siswa", $id_siswa);
      $query = $this->db->get("t_riwayatsiswa");
      return $query->num_rows() >= 1 ? $query->row() : null;
    }
  }
?>
