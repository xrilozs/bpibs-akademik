<?php
  class Guru_fam_title_model extends CI_Model{
    function get_guru_fam_title_all(){
      $query = $this->db->get('t_guru_fam_title');
      return $query->result();
    }

    function get_guru_fam_title_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('t_guru_fam_title');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>