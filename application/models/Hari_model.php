<?php
  class Hari_model extends CI_Model{
    function get_hari_all(){
      $query = $this->db->get('t_hari');
      return $query->result();
    }

    function get_hari_by_id($id){
      $this->db->where("id_hari", $id);
      $query = $this->db->get("t_hari");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_hari_by_hari($hari){
      $this->db->where("hari", $hari);
      $query = $this->db->get("t_hari");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>