<?php
  class Kategori_mapel_model extends CI_Model{
    function get_kategori_mapel_all(){
      $query = $this->db->get('t_kategori_mapel');
      return $query->result();
    }

    function get_kategori_mapel_by_id($id_kat){
      $this->db->where("id_kat", $id_kat);
      $query = $this->db->get('t_kategori_mapel');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>