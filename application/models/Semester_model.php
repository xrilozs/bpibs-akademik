<?php
  class Semester_model extends CI_Model{
    function get_semester_all(){
      $query = $this->db->get('t_semester');
      return $query->result();
    }

    function get_semester_by_id($id){
      $this->db->where("id_semester", $id);
      $query = $this->db->get("t_semester");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>