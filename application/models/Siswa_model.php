<?php
    class Siswa_model extends CI_Model{
    
    function get_siswa_by_nis($nis){
      $this->db->select("s.*, c.card_no");
      $this->db->join("t_card c", "c.id_siswa = s.id_siswa AND c.stat=1", "LEFT");
      $this->db->where("s.nis", $nis);
      $query = $this->db->get('t_siswa s');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_siswa_by_nisn($nisn){
      $this->db->select("s.*, c.card_no");
      $this->db->join("t_card c", "c.id_siswa = s.id_siswa AND c.stat=1", "LEFT");
      $this->db->where("s.nisn", $nisn);
      $query = $this->db->get('t_siswa s');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_siswa_by_card_number($card_no){
      $this->db->select("s.*, c.card_no");
      $this->db->join("t_card c", "c.id_siswa=s.id_siswa AND c.stat=1", "LEFT");
      $this->db->where("c.card_no", $card_no);
      $query = $this->db->get('t_siswa s');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_siswa($search=null, $id_kelas=null, $id_kamar=null, $id_ajaran=null, $order=null, $limit=null){
      $this->db->select('s.*, c.card_no, k.kelas, ks.id_kelasis, k2.kamar, ks2.id_kamsis');
      if($search){
        $where_search = "CONCAT_WS(',', s.nis, s.nisn, s.nama, s.kelamin, s.tmplhr, s.tgllhr, s.agama, s.anakke, s.statdk, s.jmlsdr, s.alamat, s.kec, s.kokab, s.prov, s.nva, s.hp, s.email, s.ket, c.card_no, k.kelas, k2.kamar) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_kelas){
        $this->db->where("ks.id_kelas", $id_kelas);
      }
      if($id_kamar){
        $this->db->where("ks2.id_kamar", $id_kamar);
      }
      if($id_ajaran){
        $this->db->where("ks.id_ajaran", $id_ajaran);
      }
      $this->db->from('t_siswa s');
      $this->db->join("t_card c", "c.id_siswa = s.id_siswa AND c.stat=1", "LEFT");
      $this->db->join("t_kelasis ks", "s.id_siswa = ks.id_siswa", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = ks.id_kelas", "LEFT");
      $this->db->join("t_kamsis ks2", "s.id_siswa = ks2.id_siswa", "LEFT");
      $this->db->join("t_kamar k2", "k2.id_kamar = ks2.id_kamar", "LEFT");
      if($order){
        $this->db->order_by("s.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_siswa($search=null, $id_kelas=null, $id_kamar=null, $id_ajaran=null){
      $this->db->from('t_siswa s');
      $this->db->join("t_card c", "c.id_siswa = s.id_siswa AND c.stat = 1", "left");
      $this->db->join("t_kelasis ks", "s.id_siswa = ks.id_siswa", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = ks.id_kelas", "LEFT");
      $this->db->join("t_kamsis ks2", "s.id_siswa = ks2.id_siswa", "LEFT");
      $this->db->join("t_kamar k2", "k2.id_kamar = ks2.id_kamar", "LEFT");
      if($search){
        $where_search = "CONCAT_WS(',', s.nis, s.nisn, s.nama, s.kelamin, s.tmplhr, s.tgllhr, s.agama, s.anakke, s.statdk, s.jmlsdr, s.alamat, s.kec, s.kokab, s.prov, s.nva, s.hp, s.email, s.ket, c.card_no, k.kelas, k2.kamar) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_kelas){
        $this->db->where("ks.id_kelas", $id_kelas);
      }
      if($id_kamar){
        $this->db->where("ks2.id_kamar", $id_kamar);
      }
      if($id_ajaran){
        $this->db->where("ks.id_ajaran", $id_ajaran);
      }
      return $this->db->count_all_results();
    }

    function count_siswa_by_gender($id_ajaran, $kelamin){
      $this->db->from('t_siswa s');
      $this->db->join("t_kelasis ks", "s.id_siswa = ks.id_siswa", "LEFT");
      $this->db->where("s.kelamin", $kelamin);
      $this->db->where("ks.id_ajaran", $id_ajaran);
      return $this->db->count_all_results();
    }

    function count_siswa_by_kelas($id_ajaran, $id_kelas){
      $this->db->from('t_siswa s');
      $this->db->join("t_kelasis ks", "s.id_siswa = ks.id_siswa", "LEFT");
      $this->db->where("ks.id_kelas", $id_kelas);
      $this->db->where("ks.id_ajaran", $id_ajaran);
      return $this->db->count_all_results();
    }

    function get_siswa_by_id($id){
      $this->db->select('s.*, c.card_no');
      $this->db->join("t_card c", "c.id_siswa = s.id_siswa AND c.stat = 1", "left");
      $this->db->where("s.id_siswa", $id);
      $this->db->from('t_siswa s');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_siswa($data){
      $this->db->insert('t_siswa', $data);
      return $this->db->insert_id();
    }

    function update_siswa($data){
      $this->db->update('t_siswa', $data, array("id_siswa" => $data['id_siswa']));
      return $this->db->affected_rows();
    }

    function inactive($id){
      $data = array(
        'is_active' => 0,
      );
      $this->db->update('t_siswa', $data, array('id_siswa' => $id));
      return $this->db->affected_rows();
    }

    function active($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update('t_siswa', $data, array('id_siswa' => $id));
      return $this->db->affected_rows();
    }
  }
?>
