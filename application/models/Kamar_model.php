<?php
  class Kamar_model extends CI_Model{
    function get_kamar($search=null, $id_ajaran=null, $tipe=null, $order=null, $limit=null){
      $this->db->select('k.*, a.thn_ajaran as thn_ajaran');
      if($search){
        $where_search = "CONCAT_WS(',', k.wali, k.kamar, k.tipe, a.thn_ajaran) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where('k.id_ajaran', $id_ajaran);
      }
      if($tipe){
        $this->db->where('k.tipe', $tipe);
      }
      $this->db->from('t_kamar k');
      $this->db->join('t_ajaran a', 'a.id_ajaran = k.id_ajaran');
      if($order){
        $this->db->order_by("k.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_kamar($search=null, $id_ajaran=null, $tipe=null){
      $this->db->from('t_kamar k');
      $this->db->join('t_ajaran a', 'a.id_ajaran = k.id_ajaran');
      if($search){
        $where_search = "CONCAT_WS(',', k.wali, k.kamar, k.tipe, a.thn_ajaran) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where('k.id_ajaran', $id_ajaran);
      }
      if($tipe){
        $this->db->where('k.tipe', $tipe);
      }
      return $this->db->count_all_results();
    }

    function get_kamar_by_id($id_kamar){
      $this->db->where("id_kamar", $id_kamar);
      $this->db->from('t_kamar');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_kamar($data){
      $this->db->insert('t_kamar', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_kamar($data){
      $this->db->update('t_kamar', $data, array("id_kamar" => $data['id_kamar']));
      return $this->db->affected_rows();
    }
  }
?>
