<?php
  class File_kat_model extends CI_Model{
    function get_file_kat_all(){
      $query = $this->db->get('t_file_kat');
      return $query->result();
    }

    function get_file_kat_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('t_file_kat');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>