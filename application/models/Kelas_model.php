<?php
  class Kelas_model extends CI_Model{
    function get_kelas($search=null, $id_ajaran=null, $tipe=null, $order=null, $limit=null){
      $this->db->select('k.*, a.thn_ajaran as thn_ajaran');
      if($search){
        $where_search = "CONCAT_WS(',', k.kdkelas, k.kelas, k.tipe, a.thn_ajaran) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where('k.id_ajaran', $id_ajaran);
      }
      if($tipe){
        $this->db->where('k.tipe', $tipe);
      }
      $this->db->from('t_kelas k');
      $this->db->join('t_ajaran a', 'a.id_ajaran = k.id_ajaran');
      if($order){
        $this->db->order_by("k.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_kelas($search=null, $id_ajaran=null, $tipe=null){
      $this->db->from('t_kelas k');
      $this->db->join('t_ajaran a', 'a.id_ajaran = k.id_ajaran');
      if($search){
        $where_search = "CONCAT_WS(',', k.kdkelas, k.kelas, k.tipe, a.thn_ajaran) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where('k.id_ajaran', $id_ajaran);
      }
      if($tipe){
        $this->db->where('k.tipe', $tipe);
      }
      return $this->db->count_all_results();
    }

    function get_kelas_by_id($id_kelas){
      $this->db->where("id_kelas", $id_kelas);
      $this->db->from('t_kelas');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_kelas_like_kelas($like_kelas){
      $this->db->like("kelas", $like_kelas);
      $this->db->from('t_kelas');
      $query = $this->db->get();
      return $query->result();
    }

    function create_kelas($data){
      $this->db->insert('t_kelas', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_kelas($data){
      $this->db->update('t_kelas', $data, array("id_kelas" => $data['id_kelas']));
      return $this->db->affected_rows();
    }
  }
?>
