<?php
class Kamsis_model extends CI_Model{
    function get_siswa_by_id_kamar($id_kamar){
        $this->db->select("s.*");
        $this->db->join("t_siswa s", "s.id_siswa = ks.id_siswa");
        $this->db->where("ks.id_kamar", $id_kamar);
        $query = $this->db->get('t_kamsis ks');
        return $query->result();
    }

    function get_kamsis_by_id($id_kamsis){
        $this->db->where("id_kamsis", $id_kamsis);
        $query = $this->db->get('t_kamsis');
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_kamsis_by_siswa($id_siswa){
        $this->db->select("ks.*, a.thn_ajaran as tahun_ajaran, k.kamar as kamar");
        $this->db->join("t_kamar k", "k.id_kamar = ks.id_kamar", "LEFT");
        $this->db->join("t_ajaran a", "a.id_ajaran = ks.id_ajaran", "LEFT");
        $this->db->where("ks.id_siswa", $id_siswa);
        $query = $this->db->get('t_kamsis ks');
        return $query->result();
    }

    function get_kamsis_by_siswa_kamar_ajaran($id_siswa, $id_kamar, $id_ajaran){
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("id_kamar", $id_kamar);
        $this->db->where("id_ajaran", $id_ajaran);
        $query = $this->db->get('t_kamsis');
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_kamsis($data){
        $this->db->insert('t_kamsis', $data);
        return $this->db->insert_id();
    }

    function update_kamsis($data){
        $this->db->update('t_kamsis', $data, array("id_kamsis" => $data['id_kamsis']));
        return $this->db->affected_rows();
    }
}
?>