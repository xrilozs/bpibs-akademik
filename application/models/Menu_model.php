<?php
  class Menu_model extends CI_Model{
    
    function get_menu_by_ids($ids){
      $this->db->where_in("id", $ids);
      $this->db->where("is_active", 1);
      $query = $this->db->get('t_menu');
      return $query->result();
    }

    function get_menu_all($is_active=1){
      $this->db->where("is_active", $is_active);
      $query = $this->db->get('t_menu');
      return $query->result();
    }
  }
?>
