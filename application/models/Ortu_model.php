<?php
  class Ortu_model extends CI_Model{
    function create_ortu($data){
      $this->db->insert('t_ortu', $data);
      return $this->db->affected_rows();
    }

    function update_ortu($data){
      $this->db->update('t_ortu', $data, array("id_ortu" => $data['id_ortu']));
      return $this->db->affected_rows();
    }

    function get_ortu_by_id_siswa($id_siswa){
      $this->db->where("id_siswa", $id_siswa);
      $query = $this->db->get("t_ortu");
      return $query->num_rows() >= 1 ? $query->row() : null;
    }
  }
?>
