<?php
  class Infosiswa_model extends CI_Model{
    function create_infosiswa($data){
      $this->db->insert('t_infosiswa', $data);
      return $this->db->affected_rows();
    }

    function update_infosiswa($data){
      $this->db->update('t_infosiswa', $data, array("id_is" => $data['id_is']));
      return $this->db->affected_rows();
    }
    
    function get_infosiswa_by_id_siswa($id_siswa){
      $this->db->where("id_siswa", $id_siswa);
      $query = $this->db->get("t_infosiswa");
      return $query->num_rows() >= 1 ? $query->row() : null;
    }
  }
?>
