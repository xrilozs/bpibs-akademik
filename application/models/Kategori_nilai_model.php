<?php
  class Kategori_nilai_model extends CI_Model{
    function get_kategori_nilai_all(){
      $query = $this->db->get('t_kategori_nilai');
      return $query->result();
    }

    function get_kategori_nilai_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get("t_kategori_nilai");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>