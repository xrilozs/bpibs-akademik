<?php
  class Card_model extends CI_Model{
    function get_card($search=null, $id_siswa=null, $stat=null, $order=null, $limit=null){
      $this->db->select('c.*, s.nama as nama_siswa');
      if($search){
        $where_search = "CONCAT_WS(',', c.card_no, s.nama) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_siswa){
        $this->db->where('c.id_siswa', $id_siswa);
      }
      if($stat){
        $this->db->where('c.stat', $stat);
      }
      $this->db->from('t_card c');
      $this->db->join('t_siswa s', 's.id_siswa = c.id_siswa');
      if($order){
        $this->db->order_by("c.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_card($search=null, $id_siswa=null, $stat=null){
      $this->db->from('t_card k');
      $this->db->join('t_siswa s', 's.id_siswa = c.id_siswa');
      if($search){
        $where_search = "CONCAT_WS(',', c.card_no, s.nama) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_siswa){
        $this->db->where('c.id_siswa', $id_siswa);
      }
      if($stat){
        $this->db->where('c.stat', $stat);
      }
      return $this->db->count_all_results();
    }

    function get_card_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('t_card');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_card_by_id_siswa($id_siswa){
      $this->db->where("id_siswa", $id_siswa);
      $this->db->where("stat", 1);
      $this->db->from('t_card');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_card($data){
      $this->db->insert('t_card', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_card($data){
      $this->db->update('t_card', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function inactive_card($id){
      $data = array(
        'stat' => 0,
      );
      $this->db->update('t_card', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_card($id){
      $data = array(
        'stat' => 1
      );
      $this->db->update('t_card', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
