<?php
  class Menu_sub_model extends CI_Model{
    
    function get_menu_sub_by_menu($id_menu){
      $this->db->where("id_menu", $id_menu);
      $this->db->where("is_active", 1);
      $this->db->order_by("urutan", "ASC");
      $query = $this->db->get('t_menu_sub');
      return $query->result();
    }
  }
?>
