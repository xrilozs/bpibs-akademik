<?php
  class User_model extends CI_Model{
    
    function get_user_by_email($email){
      $this->db->select('u.*, ur.role as role');
      $this->db->join('user_role ur', 'ur.id = u.role_id');
      $this->db->where("u.email", $email);
      $query = $this->db->get('user u');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_user_by_username($username){
      $this->db->select('u.*, ur.role as role');
      $this->db->join('user_role ur', 'ur.id = u.role_id');
      $this->db->where("u.username", $username);
      $query = $this->db->get('user u');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_users($search=null, $role=null, $order=null, $limit=null){
      $this->db->select('u.*, ur.role as role');
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.div, u.email, u.username, u.jk, ur.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where('u.role_id', $role);
      }
      $this->db->from('user u');
      $this->db->join('user_role ur', 'ur.id = u.role_id');
      if($order){
        $this->db->order_by("u.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_user($search=null, $role=null){
      $this->db->from('user u');
      $this->db->join('user_role ur', 'ur.id = u.role_id');
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.div, u.email, u.username, u.jk, ur.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where('u.role_id', $role);
      }
      return $this->db->count_all_results();
    }

    function get_user_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('user');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_user($data){
      $this->db->insert('user', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_user($data){
      $this->db->update('user', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function inactive($id){
      $data = array(
        'is_active' => 0,
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
    
    function change_password($id, $password){
      $req = array(
        'password' => $password
      );
      $this->db->update('user', $req, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
