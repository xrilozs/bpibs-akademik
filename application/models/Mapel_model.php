<?php
    class Mapel_model extends CI_Model{
    function get_mapel($search=null, $id_kat=null, $id_ajaran=null, $order=null, $limit=null){
      $this->db->select('m.*, k.kategori');
      if($search){
        $where_search = "CONCAT_WS(',', m.mapel, m.mapel_det, k.kategori) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_kat){
        $this->db->where("id_kat", $id_kat);
      }
      if($id_ajaran){
        $this->db->where("id_ajaran", $id_ajaran);
      }
      $this->db->from('t_mapel m');
      $this->db->join("t_kategori_mapel k", "k.id_kat = m.id_kat", "LEFT");
      if($order){
        $this->db->order_by("m.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_mapel($search=null, $id_kat=null, $id_ajaran=null){
      $this->db->from('t_mapel m');
      $this->db->join("t_kategori_mapel k", "k.id_kat = m.id_kat", "left");
      if($search){
        $where_search = "CONCAT_WS(',', m.mapel, m.mapel_det, k.kategori) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_kat){
        $this->db->where("id_kat", $id_kat);
      }
      if($id_ajaran){
        $this->db->where("id_ajaran", $id_ajaran);
      }
      return $this->db->count_all_results();
    }

    function get_mapel_by_id($id){
      $this->db->select('m.*, k.kategori');
      $this->db->join("t_kategori_mapel k", "k.id_kat = m.id_kat", "left");
      $this->db->where("m.id_mapel", $id);
      $this->db->from('t_mapel m');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_mapel_by_id_kat($id_kat){
      $this->db->select("m.*, k.kategori");
      $this->db->join("t_kategori_mapel k", "k.id_kat=m.id_kat", "LEFT");
      $this->db->where("m.id_kat", $id_kat);
      $query = $this->db->get('t_mapel m');
      return $query->result();
    }

    function create_mapel($data){
      $this->db->insert('t_mapel', $data);
      return $this->db->insert_id();
    }

    function update_mapel($data){
      $this->db->update('t_mapel', $data, array("id_mapel" => $data['id_mapel']));
      return $this->db->affected_rows();
    }

    function delete_mapel($id){
      $this->db->delete('t_mapel', array('id_mapel' => $id));
      return $this->db->affected_rows();
    }
  }
?>
