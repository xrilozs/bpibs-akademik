<?php
  class Guru_edu_lvl_model extends CI_Model{
    function get_guru_edu_lvl_all(){
      $query = $this->db->get('t_guru_edu_lvl');
      return $query->result();
    }

    function get_guru_edu_lvl_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('t_guru_edu_lvl');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>