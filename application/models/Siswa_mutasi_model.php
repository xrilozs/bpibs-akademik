<?php
    class Siswa_mutasi_model extends CI_Model{
        function get_siswa_mutasi_by_siswa($id_siswa){
            $this->db->where("id_siswa", $id_siswa);
            $query = $this->db->get("t_siswa_mutasi");
            return $query->result();
        }

        function create_siswa_mutasi($data){
            $this->db->insert("t_siswa_mutasi", $data);
            return $this->db->insert_id();
        }
    }
?>