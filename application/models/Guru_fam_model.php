<?php
  class Guru_fam_model extends CI_Model{
    function create_guru_fam($data){
      $this->db->insert('t_guru_fam', $data);
      return $this->db->affected_rows();
    }

    function update_guru_fam($data){
      $this->db->update('t_guru_fam', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }
    
    function get_guru_fam_by_idguru($idguru){
      $this->db->select("f.*, t.hubungan");
      $this->db->join("t_guru_fam_title t", "t.id = f.idfam");
      $this->db->where("f.idguru", $idguru);
      $this->db->order_by("f.idfam", "ASC");
      $query = $this->db->get("t_guru_fam f");
      return $query->result();
    }

    function get_guru_fam_by_id($id){
      $this->db->select("f.*, t.hubungan");
      $this->db->join("t_guru_fam_title t", "t.id = f.idfam");
      $this->db->where("f.id", $id);
      $query = $this->db->get("t_guru_fam f");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function delete_guru_fam($id){
      $this->db->delete('t_guru_fam', array('id' => $id));
      return $this->db->affected_rows();
    }
}
?>
