<?php
  class Siswa_file_model extends CI_Model{
    function create_siswa_file($data){
      $this->db->insert('t_file', $data);
      return $this->db->affected_rows();
    }

    function update_siswa_file($data){
      $this->db->update('t_file', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }
    
    function get_siswa_file_by_id_siswa($id_siswa){
      $this->db->select("f.*, k.file as file_kat");
      $this->db->join("t_file_kat k", "k.id = f.id_file", "LEFT");
      $this->db->where("f.id_siswa", $id_siswa);
      $this->db->order_by("f.file_name", "ASC");
      $query = $this->db->get("t_file f");
      return $query->result();
    }

    function get_siswa_file_by_id($id){
      $this->db->select("f.*, k.file as file_kat");
      $this->db->join("t_file_kat k", "k.id = f.id_file", "LEFT");
      $this->db->where("f.id", $id);
      $query = $this->db->get("t_file f");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function delete_siswa_file($id){
      $this->db->delete('t_file', array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
