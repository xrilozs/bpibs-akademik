<?php
  class Guru_model extends CI_Model{
    function get_guru($search=null, $order=null, $limit=null){
      $this->db->where("is_deleted", 0);
      if($search){
        $where_search = "CONCAT_WS(',', nid, npp, nama, tlhr, tgllhr, kelamin, email, hp, nikah, tb, bb, goldar, bj, tgljoin) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->from('t_guru');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_guru($search=null){
      $this->db->where("is_deleted", 0);
      $this->db->from('t_guru');
      if($search){
        $where_search = "CONCAT_WS(',', nid, npp, nama, tlhr, tgllhr, kelamin, email, hp, nikah, tb, bb, goldar, bj, tgljoin) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_guru_by_id($id){
      $this->db->where("is_deleted", 0);
      $this->db->where("id", $id);
      $this->db->from('t_guru');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_guru_by_email($id){
      $this->db->where("is_deleted", 0);
      $this->db->where("id", $id);
      $this->db->from('t_guru');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_guru($data){
      $this->db->insert('t_guru', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_guru($data){
      $this->db->update('t_guru', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_guru($id){
      $data = array(
        "is_deleted" => 1
      );
      $this->db->update('t_guru', $data, array("id" => $id));
      return $this->db->affected_rows();
    }
  }
?>
