<?php
  class Guru_edu_model extends CI_Model{
    function create_guru_edu($data){
      $this->db->insert('t_guru_edu', $data);
      return $this->db->affected_rows();
    }

    function update_guru_edu($data){
      $this->db->update('t_guru_edu', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }
    
    function get_guru_edu_by_idguru($idguru){
      $this->db->select("e.*, l.level");
      $this->db->join("t_guru_edu_lvl l", "l.id = e.idlvl");
      $this->db->where("e.idguru", $idguru);
      $this->db->order_by("e.idlvl", "ASC");
      $query = $this->db->get("t_guru_edu e");
      return $query->result();
    }

    function get_guru_edu_by_id($id){
      $this->db->select("e.*, l.level");
      $this->db->join("t_guru_edu_lvl l", "l.id = e.idlvl");
      $this->db->where("e.id", $id);
      $query = $this->db->get("t_guru_edu e");
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function delete_guru_edu($id){
      $this->db->delete('t_guru_edu', array('id' => $id));
      return $this->db->affected_rows();
    }
}
?>
