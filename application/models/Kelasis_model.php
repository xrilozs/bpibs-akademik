<?php
class Kelasis_model extends CI_Model{
    function get_siswa_by_id_kelas($id_kelas){
        $this->db->select("s.*");
        $this->db->join("t_siswa s", "s.id_siswa = ks.id_siswa");
        $this->db->where("ks.id_kelas", $id_kelas);
        $query = $this->db->get('t_kelasis ks');
        return $query->result();
    }

    function get_kelasis_by_id($id_kelasis){
        $this->db->where("id_kelasis", $id_kelasis);
        $query = $this->db->get('t_kelasis');
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_kelasis_by_siswa($id_siswa){
        $this->db->select("ks.*, a.thn_ajaran as tahun_ajaran, k.kelas as kelas");
        $this->db->join("t_kelas k", "k.id_kelas = ks.id_kelas", "LEFT");
        $this->db->join("t_ajaran a", "a.id_ajaran = ks.id_ajaran", "LEFT");
        $this->db->where("ks.id_siswa", $id_siswa);
        $query = $this->db->get('t_kelasis ks');
        return $query->result();
    }

    function get_kelasis_by_siswa_kelas_ajaran($id_siswa, $id_kelas, $id_ajaran){
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("id_kelas", $id_kelas);
        $this->db->where("id_ajaran", $id_ajaran);
        $query = $this->db->get('t_kelasis');
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_kelasis_by_kelas_ajaran($id_kelas, $id_ajaran){
        $this->db->where("id_kelas", $id_kelas);
        $this->db->where("id_ajaran", $id_ajaran);
        $query = $this->db->get('t_kelasis');
        return $query->result();
    }

    function create_kelasis($data){
        $this->db->insert('t_kelasis', $data);
        return $this->db->insert_id();
    }

    function update_kelasis($data){
        $this->db->update('t_kelasis', $data, array("id_kelasis" => $data['id_kelasis']));
        return $this->db->affected_rows();
    }
}
?>