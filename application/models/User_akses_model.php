<?php
  class User_akses_model extends CI_Model{
    
    function get_user_akses_by_user_id_and_appV2($user_id, $app = "simak"){
      $this->db->select("u.*, ur.role as role, ur.id as role_id, ua.app_id as app_id, ua.id as user_akses_id, ua.menu_id as menu_ids");
      $this->db->join("user_akses ua", "ua.user_id = u.id", "LEFT");
      $this->db->join('user_app ap', 'ua.app_id = ap.id', 'LEFT');
      $this->db->join("user_role ur", "ur.id = ua.role_id", "LEFT");
      $this->db->where("u.id", $user_id);
      $this->db->where("ap.app", $app);
      $query = $this->db->get('user u');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_user_akses_by_user_id_and_app($user_id, $app = "simak"){
      $this->db->from('user_akses ak');
      $this->db->join('user_app ap', 'ak.app_id = ap.id', 'LEFT');
      $this->db->where("ak.user_id", $user_id);
      $this->db->where("ap.app", $app);
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_user_aksesV2($search=null, $order=null, $limit=null, $app = "simak"){
      $this->db->select("u.*, ur.role as role, ua.menu_id as menu_ids");
      $this->db->join("user_akses ua", "ua.user_id = u.id", "LEFT");
      $this->db->join('user_app ap', 'ua.app_id = ap.id', 'LEFT');
      $this->db->join("user_role ur", "ur.id = ua.role_id", "LEFT");
      $this->db->where("ap.app", $app);
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.email, u.username, ur.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by("u.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('user u');
      return $query->result();
    }

    function get_user_akses($search=null, $order=null, $limit=null){
      $this->db->select('ua.*, u.name as name, u.email as email, ur.role as role, up.app as app');
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.email, ur.role, up.app) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->from('user_akses ua');
      $this->db->join('user u', 'u.id = ua.user_id');
      $this->db->join('user_app up', 'up.id = ua.app_id');
      $this->db->join('user_role ur', 'ur.id = ua.role_id');
      if($order){
        $this->db->order_by("ua.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_user_aksesV2($search=null, $app = "simak"){
      $this->db->join("user_akses ua", "ua.user_id = u.id", "LEFT");
      $this->db->join('user_app ap', 'ua.app_id = ap.id', 'LEFT');
      $this->db->join("user_role ur", "ur.id = ua.role_id", "LEFT");
      $this->db->where("ap.app", $app);
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.email, u.username, ur.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->get('user u');
      return $this->db->count_all_results();
    }

    function count_user_akses($search=null, $role=null){
      $this->db->from('user_akses ua');
      $this->db->join('user u', 'u.id = ua.user_id');
      $this->db->join('user_app up', 'up.id = ua.app_id');
      $this->db->join('user_role ur', 'ur.id = ua.role_id');
      if($search){
        $where_search = "CONCAT_WS(',', u.name, u.email, ur.role, up.app) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_user_akses_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('user_akses');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_user_akses($data){
      $this->db->insert('user_akses', $data);
      return $this->db->affected_rows() > 0;
    }

    function update_user_akses($data){
      $this->db->update('user_akses', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_user_akses($id){
      $this->db->delete('user_akses', array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
