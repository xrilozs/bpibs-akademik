<?php
  class User_role_model extends CI_Model{
    
    function get_user_role_all($search = null){
      if($search){
        $this->db->like("role", $search);
      }
      $query = $this->db->get('user_role');
      return $query->result();
    }

    function count_user_role_all($search = null){
      if($search){
        $this->db->like("role", $search);
      }
      $this->db->from('user_role');
      return $this->db->count_all_results();
    }
  }
?>
