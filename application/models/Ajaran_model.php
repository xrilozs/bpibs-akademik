<?php
  class Ajaran_model extends CI_Model{
    function get_ajaran_all($is_aktif=null){
      if(!is_null($is_aktif)){
        $this->db->where("aktif", $is_aktif);
      }
      $query = $this->db->get('t_ajaran');
      return $query->result();
    }

    function get_ajaran_by_id($id){
      $this->db->where("id_ajaran", $id);
      $query = $this->db->get("t_ajaran");
      return $query->num_rows() > 0 ? $query->row() : null;
    }
  }
?>