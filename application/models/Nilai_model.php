<?php
    class Nilai_model extends CI_Model{
    function get_nilai($search=null, $id_ajaran=null, $id_semester=null, $id_mapel=null, $id_kat_nilai=null, $id_kelasis=null, $id_siswa=null, $order=null, $limit=null){
      $this->db->select('n.*, m.mapel, k.kategori, kl.kelas, s.nama');
      if($search){
        $where_search = "CONCAT_WS(',', n.nilai, n.deskripsi, m.mapel, k.kategori, kl.kelas) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where("n.id_ajaran", $id_ajaran);
      }
      if($id_semester){
        $this->db->where("n.id_semester", $id_semester);
      }
      if($id_mapel){
        $this->db->where("n.id_mapel", $id_mapel);
      }
      if($id_kat_nilai){
        $this->db->where("n.id_kat_nilai", $id_kat_nilai);
      }
      if($id_kelasis){
        $this->db->where_in("n.id_kelasis", $id_kelasis);
      }
      if($id_siswa){
        $this->db->where("n.id_siswa", $id_siswa);
      }
      $this->db->from('t_nilai n');
      $this->db->join("t_kategori_nilai k", "n.id_kat_nilai = k.id", "LEFT");
      $this->db->join("t_siswa s", "s.id_siswa = n.id_siswa", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = n.id_mapel", "LEFT");
      $this->db->join("t_kelasis ks", "ks.id_kelasis = n.id_kelasis", "LEFT");
      $this->db->join("t_kelas kl", "kl.id_kelas = ks.id_kelas", "LEFT");
      if($order){
        $this->db->order_by("n.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_nilai($search=null, $id_ajaran=null, $id_semester=null, $id_mapel=null, $id_kat_nilai=null, $id_kelasis=null, $id_siswa=null){
      if($search){
        $where_search = "CONCAT_WS(',', n.nilai, n.deskripsi, m.mapel, k.kategori, kl.kelas) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_ajaran){
        $this->db->where("n.id_ajaran", $id_ajaran);
      }
      if($id_semester){
        $this->db->where("n.id_semester", $id_semester);
      }
      if($id_mapel){
        $this->db->where("n.id_mapel", $id_mapel);
      }
      if($id_kat_nilai){
        $this->db->where("n.id_kat_nilai", $id_kat_nilai);
      }
      if($id_kelasis){
        $this->db->where("n.id_kelasis", $id_kelasis);
      }
      if($id_siswa){
        $this->db->where("n.id_siswa", $id_siswa);
      }
      $this->db->from('t_nilai n');
      $this->db->join("t_kategori_nilai k", "n.id_kat_nilai = k.id", "LEFT");
      $this->db->join("t_siswa s", "s.id_siswa = n.id_siswa", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = n.id_mapel", "LEFT");
      $this->db->join("t_kelasis ks", "ks.id_kelasis = n.id_kelasis", "LEFT");
      $this->db->join("t_kelas kl", "kl.id_kelas = ks.id_kelas", "LEFT");
      return $this->db->count_all_results();
    }

    function get_nilai_by_id($id){
      $this->db->select('n.*, m.mapel, k.kategori, kl.kelas, s.nama');
      if($id_ajaran){
        $this->db->where("n.id", $id);
      }
      $this->db->from('t_nilai n');
      $this->db->join("t_kategori_nilai k", "n.id_kat_nilai = k.id", "LEFT");
      $this->db->join("t_siswa s", "s.id_siswa = n.id_siswa", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = n.id_mapel", "LEFT");
      $this->db->join("t_kelasis ks", "ks.id_kelasis = n.id_kelasis", "LEFT");
      $this->db->join("t_kelas kl", "kl.id_kelas = ks.id_kelas", "LEFT");
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_nilai($data){
      $this->db->insert('t_nilai', $data);
      return $this->db->insert_id();
    }

    function update_nilai($data){
      $this->db->update('t_nilai', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function get_report_nilai_siswa($id_ajaran, $id_semester, $id_siswa){
      $query = "SELECT s.id AS nilai_id,
                      t.id AS tugas_id,
                      sub.id_mapel AS mapel_id,
                      t.kategori AS tugas_nama,
                      sub.mapel AS mapel_nama,
                      s.nilai AS nilai
                FROM t_kategori_nilai t
                CROSS JOIN t_mapel sub
                LEFT JOIN t_nilai s ON t.id = s.id_kat_nilai AND sub.id_mapel = s.id_mapel AND 
                          s.id_siswa = $id_siswa AND s.id_ajaran = $id_ajaran AND s.id_semester = $id_semester
                ORDER BY sub.urutan, t.id;";
      $query = $this->db->query($query);
      return $query->result();
    }
  }
?>
