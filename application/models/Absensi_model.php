<?php
    class Absensi_model extends CI_Model{
    function get_absensi($search=null, $id_jadwal=null, $id_siswa=null, $tgl=null, $order=null, $limit=null){
      $this->db->select('a.*, m.mapel, s.nama');
      if($search){
        $where_search = "CONCAT_WS(',', a.tgl, a.hadir, m.mapel, m.mapel, h.hari, j.awal, j.akhir) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($id_jadwal){
        $this->db->where("a.id_jadwal", $id_jadwal);
      }
      if($id_siswa){
        $this->db->where("s.id_siswa", $id_siswa);
      }
      if($tgl){
        $this->db->where("a.tgl", $tgl);
      }
      $this->db->from('t_absensi a');
      $this->db->join("t_jadwal_mapel j", "a.id_jadwal = j.id_jadwal", "LEFT");
      $this->db->join("t_siswa s", "s.id_siswa = a.id_siswa", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      if($order){
        $this->db->order_by("a.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_absensi($search=null, $id_jadwal=null, $id_siswa=null, $tgl=null){
      if($search){
        $where_search = "CONCAT_WS(',', a.tgl, a.hadir, m.mapel, m.mapel, h.hari, j.awal, j.akhir) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }                                  
      if($id_jadwal){
        $this->db->where("a.id_jadwal", $id_jadwal);
      }
      if($id_siswa){
        $this->db->where("a.id_siswa", $id_siswa);
      }
      if($tgl){
        $this->db->where("a.tgl", $tgl);
      }
      $this->db->from('t_absensi a');
      $this->db->join("t_jadwal_mapel j", "a.id_jadwal = j.id_jadwal", "LEFT");
      $this->db->join("t_siswa s", "s.id_siswa = a.id_siswa", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      return $this->db->count_all_results();
    }

    function get_absensi_by_id($id){
      $this->db->select('s.*, a.thn_ajaran, s.semester, k.kelas, m.mapel, h.hari');
      $this->db->join("t_card c", "c.id_jadwal = s.id_jadwal AND c.stat = 1", "left");
      $this->db->where("j.id_jadwal", $id);
      $this->db->from('t_absensi j');
      $this->db->join("t_ajaran a", "a.id_ajaran = j.id_ajaran", "LEFT");
      $this->db->join("t_semester s", "s.id_semester = j.id_semester", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = j.id_kelas", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_absensi($data){
      $this->db->insert('t_absensi', $data);
      return $this->db->insert_id();
    }

    function update_absensi($data){
      $this->db->update('t_absensi', $data, array("id" => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_absensi($id){
      $data = array(
        'is_deleted' => 1,
      );
      $this->db->update('t_absensi', $data, array('id_jadwal' => $id));
      return $this->db->affected_rows();
    }
  }
?>
