<?php
    class Jadwal_mapel_model extends CI_Model{
    function get_jadwal_mapel($search=null, $id_ajaran=null, $id_semester=null, $id_kelas=null, $id_mapel=null, $id_hari=null, $order=null, $limit=null){
      $this->db->select('j.*, a.thn_ajaran, s.semester, k.kelas, m.mapel, h.hari');
      if($search){
        $where_search = "CONCAT_WS(',', a.thn_ajaran, s.semester, k.kelas, m.mapel, h.hari, j.awal, j.akhir) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }                                  
      if($id_ajaran){
        $this->db->where("a.id_ajaran", $id_ajaran);
      }
      if($id_semester){
        $this->db->where("s.id_semester", $id_semester);
      }
      if($id_kelas){
        $this->db->where("k.id_kelas", $id_kelas);
      }
      if($id_mapel){
        $this->db->where("m.id_mapel", $id_mapel);
      }
      if($id_hari){
        $this->db->where("h.id_hari", $id_hari);
      }
      $this->db->from('t_jadwal_mapel j');
      $this->db->join("t_ajaran a", "a.id_ajaran = j.id_ajaran", "LEFT");
      $this->db->join("t_semester s", "s.id_semester = j.id_semester", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = j.id_kelas", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      $this->db->join("t_hari h", "h.id_hari = j.id_hari", "LEFT");
      if($order){
        $this->db->order_by("j.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_jadwal_mapel($search=null, $id_ajaran=null, $id_semester=null, $id_kelas=null, $id_mapel=null, $id_hari=null){
      if($search){
        $where_search = "CONCAT_WS(',', a.thn_ajaran, s.semester, k.kelas, m.mapel, h.hari, j.awal, j.akhir) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }                                  
      if($id_ajaran){
        $this->db->where("a.id_ajaran", $id_ajaran);
      }
      if($id_semester){
        $this->db->where("s.id_semester", $id_semester);
      }
      if($id_kelas){
        $this->db->where("k.id_kelas", $id_kelas);
      }
      if($id_mapel){
        $this->db->where("m.id_mapel", $id_mapel);
      }
      if($id_hari){
        $this->db->where("h.id_hari", $id_hari);
      }
      $this->db->from('t_jadwal_mapel j');
      $this->db->join("t_ajaran a", "a.id_ajaran = j.id_ajaran", "LEFT");
      $this->db->join("t_semester s", "s.id_semester = j.id_semester", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = j.id_kelas", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      $this->db->join("t_hari h", "h.id_hari = j.id_hari", "LEFT");
      return $this->db->count_all_results();
    }

    function get_jadwal_mapel_by_id($id){
      $this->db->select('j.*, a.thn_ajaran, s.semester, k.kelas, m.mapel, h.hari');
      $this->db->where("j.id_jadwal", $id);
      $this->db->from('t_jadwal_mapel j');
      $this->db->join("t_ajaran a", "a.id_ajaran = j.id_ajaran", "LEFT");
      $this->db->join("t_semester s", "s.id_semester = j.id_semester", "LEFT");
      $this->db->join("t_kelas k", "k.id_kelas = j.id_kelas", "LEFT");
      $this->db->join("t_mapel m", "m.id_mapel = j.id_mapel", "LEFT");
      $this->db->join("t_hari h", "h.id_hari = j.id_hari", "LEFT");
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_jadwal_mapel($data){
      $this->db->insert('t_jadwal_mapel', $data);
      return $this->db->insert_id();
    }

    function update_jadwal_mapel($data){
      $this->db->update('t_jadwal_mapel', $data, array("id_jadwal" => $data['id_jadwal']));
      return $this->db->affected_rows();
    }

    function delete_jadwal_mapel($id){
      $data = array(
        'is_deleted' => 1,
      );
      $this->db->update('t_jadwal_mapel', $data, array('id_jadwal' => $id));
      return $this->db->affected_rows();
    }
  }
?>
