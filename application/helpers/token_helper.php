<?php
  require 'vendor/autoload.php';
  use Firebase\JWT\JWT;

  function verify_user_token($header){
    $CI       =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
    }else{
      $resp_obj->set_response(401, "failed", "Please use token to access this resource.");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    try {
      $jwt = JWT::decode($token, ACCESS_TOKEN_SALT, ['HS256']);
    } catch (Exception $e) {
      $resp_obj->set_response(401, "failed", "Invalid requested token");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    $email = $jwt->email;
    $user  = $CI->user_model->get_user_by_email($email);
    
    #check User exist
    if(is_null($user)){
      $resp_obj->set_response(401, "failed", "User not found");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    if(!$user->is_active){
      $resp_obj->set_response(401, "failed", "User is Inactive");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check guard role
    $user_menu = guard_role($user);
    if($user_menu){
      $data = array(
        'user' => $user,
        'menu' => $user_menu
      );
      $resp_obj->set_response(200, "success", "Authorized access", $data);
      $resp = $resp_obj->get_response();
      return $resp;
    }else{
      $resp_obj->set_response(401, "failed", "Unauthorized role access", $allowed_role);
      $resp = $resp_obj->get_response();
      return $resp;
    }
  }
?>