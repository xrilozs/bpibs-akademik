<?php
  function check_parameter($params){
    foreach($params as $param){
      if(is_null($param)){
        return false;
      }
    }
    return true;
  }

  function convert_form_to_array($keys){
    $array = array();
    foreach($keys as $key){
      if ($this->input->post($key) === null) {
        return null;
      } else {
        $array['key'] = $this->input->post($key);
      }
    }
    return $array;
  }

  function check_parameter_by_keys($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return false;
      }
    }
    return true;
  }

  function check_parameter_by_keysV2($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return array(
          "success" => false,
          "message" => "$key is missing in request body"
        );
      }
    }
    return array(
      "success" => true,
    );
  }

  function get_unique_code(){
    $randomNum = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 3);
    return strtoupper($randomNum);
  }

  function generate_alias($str){
    $str = preg_replace('/[^a-z0-9\s\-]/i', '', $str);
    // Replace all spaces with hyphens
    $str = preg_replace('/\s/', '-', $str);
    // Replace multiple hyphens with a single hyphen
    $str = preg_replace('/\-\-+/', '-', $str);
    // Remove leading and trailing hyphens, and then lowercase the URL
    $str = strtolower(trim($str, '-'));

    return $str;
  }

  function set_output($resp){
    $CI =& get_instance();
    $CI->output
        ->set_header('Access-Control-Allow-Origin: *')
        ->set_status_header($resp['code'])
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($resp));
  }

  function logging($type, $message, $data=null){
    log_message($type, $message);
    if($data){
      log_message($type, 'Request/Response: '.json_encode($data));
    }
  }

  function convert_day_ID($date){
    $dateTime = new DateTime($date);
    $dayName  = $dateTime->format('l');
    $day      = strtolower($dayName);
    switch ($day) {
      case 'sunday':
        $day = "Ahad";
        break;
      case 'monday':
        $day = "Senin";
        break;
      case 'tuesday':
        $day = "Selasa";
        break;
      case 'wednesday':
        $day = "Rabu";
        break;
      case 'thursday':
        $day = "Kamis";
        break;
      case 'friday':
        $day = "Jumat";
        break;
      case 'saturday':
        $day = "Sabtu";
        break;
      default:
        break;
    }

    return $day;
  }
?>
