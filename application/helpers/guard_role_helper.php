<?php
    function guard_role($user){
        $CI =& get_instance();
        try {
            $user_akses = $CI->user_akses_model->get_user_akses_by_user_id_and_app($user->id);
            if(!is_null($user_akses)){
                $menu_ids = $user_akses->menu_id;
                if(is_null($menu_ids)){
                    return null;
                }
                $menu_arr = explode(',', $menu_ids);
                if(sizeof($menu_arr) == 0){
                    return null;
                }
                $user_menu = $CI->menu_model->get_menu_by_ids($menu_arr);
                foreach ($user_menu as &$menu) {
                    if($menu->parent){
                        $menu->sub = $CI->menu_sub_model->get_menu_sub_by_menu($menu->id);
                    }
                }
                return $user_menu;
            }
            return $user_akses;
        } catch (Exception $e) {
            return null;
        }
    }
?>
