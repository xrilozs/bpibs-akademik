<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response_api {
  private $http_code;
  private $status;
  private $message;
  private $data = null;
  private $draw;
  private $records_total;
  private $records_filtered;

  public function set_response($http_code, $status, $message, $data=null){
    $this->http_code = $http_code;
    $this->status = $status;
    $this->message = $message;
    $this->data = $data;
  }
  
  public function get_response(){
    $response = array(
      "code" => $this->http_code,
      "status" => $this->status,
      "message" => $this->message,
      "data" => $this->data
    );
    return $response;
  }
  
  public function set_response_datatable($http_code, $data, $draw, $records_total, $records_filtered){
    $this->http_code = $http_code;
    $this->data = $data;
    $this->draw = $draw;
    $this->records_total = $records_total;
    $this->records_filtered = $records_filtered;
  }
  
  public function get_response_datatable(){
    $response = array(
      "code" => $this->http_code,
      "data" => $this->data,
      "draw" => $this->draw,
      "recordsTotal" => $this->records_total,
      "recordsFiltered" => $this->records_filtered
    );
    return $response;
  }
}

?>