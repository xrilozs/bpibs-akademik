<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Siswa</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('siswa/list');?>">Siswa</a></li>
                  <li class="breadcrumb-item active">Buat</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>Buat Siswa</h2>
                        </div>
                        <div class="col-lg-6 text-right">
                          <a href="<?=base_url('siswa/list');?>" class="btn btn-secondary">Batal</a>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div id="form-wizard">
                        <h2>Data Siswa</h2>
                        <section class="w-100">
                            <form class="form my-4" id="siswa-form">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Foto:</label>
                                            <input 
                                                type="file" 
                                                id="siswa-create-foto"
                                                data-allowed-file-extensions="jpeg jpg png" 
                                                data-max-file-size="500K"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>NIS</label>
                                            <input type="text" name="siswa-nis" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>NISN</label>
                                            <input type="text" name="siswa-nisn" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>NVA</label>
                                            <input type="text" name="siswa-nva" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" name="siswa-nama" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Panggilan</label>
                                            <input type="text" name="siswa-panggilan" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <select name="kelamin" class="form-control siswa-kelamin-option" id="siswa-create-kelamin" required>
                                                <option value="L">Laki-laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="siswa-email" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>HP</label>
                                            <input type="text" name="siswa-hp" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tempat Lahir</label>
                                            <input type="text" name="siswa-tmplhr" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <div class="input-group mb-3">                                        
                                                <input data-provide="datepicker" name="siswa-tgllhr" data-date-autoclose="true" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Agama</label>
                                            <input type="text" name="siswa-agama" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Status Di Keluarga</label>
                                            <select name="statdk" class="form-control siswa-statdk-option" id="siswa-create-statdk" required>
                                                <option value="Anak Kandung">Anak Kandung</option>
                                                <option value="Anak Tiri">Anak Tiri</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Anak Ke</label>
                                            <input type="number" name="siswa-anakke" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Jumlah Saudara</label>
                                            <input type="number" name="siswa-jmlsdr" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea name="siswa-alamat" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kelurahan</label>
                                            <input type="text" name="siswa-kel" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <input type="text" name="siswa-kec" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kota/Kabupaten</label>
                                            <input type="text" name="siswa-kokab" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <input type="text" name="siswa-prov" class="form-control" required>
                                        </div>
                                    </div>
                                </div>                  
                            </form>
                        </section>
                        <h2>Data Orang Tua</h2>
                        <section class="w-100">
                            <form class="form my-4" id="ortu-form">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nama Ayah</label>
                                            <input type="text" name="ortu-ayah_nama" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Pekerjaan Ayah</label>
                                            <input type="text" name="ortu-ayah_pekerjaan" class="form-control" required>
                                        </div>
                                    </div>
                                    <di class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nomor Handphone Ayah</label>
                                            <input type="text" name="ortu-ayah_hp" class="form-control" required>
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nama Ibu</label>
                                            <input type="text" name="ortu-ibu_nama" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Pekerjaan Ibu</label>
                                            <input type="text" name="ortu-ibu_pekerjaan" class="form-control" required>
                                        </div>
                                    </div>
                                    <di class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nomor Handphone Ibu</label>
                                            <input type="text" name="ortu-ibu_hp" class="form-control" required>
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Penghasilan</label>
                                            <input type="text" name="ortu-penghasilan" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="ortu-email" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea name="ortu-alamat" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nama Wali (optional)</label>
                                            <input type="text" name="ortu-wali_nama" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Pekerjaan Wali (optional)</label>
                                            <input type="text" name="ortu-wali_pekerjaan" class="form-control">
                                        </div>
                                    </div>
                                    <di class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nomor Handphone Wali (optional)</label>
                                            <input type="text" name="ortu-wali_hp" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Penghasilan Wali (optional)</label>
                                            <input type="text" name="ortu-wali_penghasilan" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Email Wali (optional)</label>
                                            <input type="text" name="ortu-wali_email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Alamat Wali (optional)</label>
                                            <textarea name="ortu-wali_alamat" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </section>
                        <h2>Data Riwayat (Optional)</h2>
                        <section class="w-100">
                            <form class="form my-4" id="riwayat-form">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Tanggal Tes</label>
                                            <input type="text" name="riwayat-tgl_tes" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Sekolah Asal</label>
                                            <input type="text" name="riwayat-asal_sekolah" class="form-control">
                                        </div>
                                    </div>
                                    <di class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tahun Lulus</label>
                                            <input type="text" name="riwayat-thn_lulus" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Internal BP</label>
                                            <input type="text" name="riwayat-internal_bp" class="form-control">
                                        </div>
                                    </div>
                                    <di class="col-lg-6">
                                        <div class="form-group">
                                            <label>Cabang BP</label>
                                            <input type="text" name="riwayat-cabang_bp" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kompetensi Akademik</label>
                                            <input type="text" name="riwayat-komp_ak" class="form-control">
                                        </div>
                                    </div>
                                    <di class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kompetensi Non-akademik</label>
                                            <input type="text" name="riawayat-komp_nonak" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kompetensi Mengaji</label>
                                            <input type="text" name="riwayat-komp_mengaji" class="form-control">
                                        </div>
                                    </div>
                                    <di class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kompetensi Tulis Arab</label>
                                            <input type="text" name="riwayat-komp-tularab" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Hafalan</label>
                                            <input type="text" name="riwayat-hafalan" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </section>
                        <h2>Data Info (Optional)</h2>
                        <section class="w-100">
                            <form class="form my-4" id="info-form">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Tanggal Masuk</label>
                                            <div class="input-group mb-3">                                        
                                                <input data-provide="datepicker" name="info-tgl_masuk" data-date-autoclose="true" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Ke Kelas</label>
                                            <input type="text" name="info-ke_kelas" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <di class="col-lg-12">
                                        <div class="form-group">
                                            <label>Info BPIBS</label>
                                            <input type="text" name="info-info_bpibs" class="form-control">
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Alasan Orang Tuan</label>
                                            <textarea name="info-alasan_ortu" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <di class="col-lg-6">
                                        <div class="form-group">
                                            <label>Alasan Siswa</label>
                                            <textarea name="info-alasan_siswa" class="form-control"></textarea>
                                        </div>
                                    </di>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea name="info-keterangan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>