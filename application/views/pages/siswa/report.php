<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Siswa</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('siswa/list');?>">Siswa</a></li>
                  <li class="breadcrumb-item active">Raport Nilai</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>Rapor Nilai Siswa</h2>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a href="<?=base_url('siswa/list');?>" class="btn btn-secondary">Kembali</a>
                        </div>
                    </div>
                </div>
                <div class="body" id="report-section">

                </div>
            </div>
        </div>
    </div>
</div>