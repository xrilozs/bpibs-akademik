<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Siswa</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">Siswa</li>
                  <li class="breadcrumb-item active">Data Siswa</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="accordion" id="filter-accordion">
          <div class="card">
            <div class="card-header" id="headingOne" style="background-color:white;">
              <h2 class="mb-0">
                <button class="btn btn-block text-between" type="button" data-toggle="collapse" data-target="#filter-section" aria-expanded="true" aria-controls="collapseOne">
                  Filter <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
              </h2>
            </div>
            <div id="filter-section" class="collapse show" aria-labelledby="headingOne" data-parent="#filter-accordion">
              <div class="card-body">
                <div class="row mb-4">
                  <div class="col-lg-4">
                    <label>Kelas</label>
                    <select name="filter-kelas-option" class="form-control" id="filter-kelas-option">
                      <option value="">Semua</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
          <div class="header">
            <h2>Data Siswa</h2>
            <ul class="header-dropdown dropdown dropdown-animated scale-left">
              <li>
                <a href="<?=base_url('siswa/create');?>" class="btn btn-sm btn-primary text-white" title="">Buat baru</a>
              </li>
              <li>
                <a id="refresh-table" href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="siswa-datatable">
                <thead>
                  <tr>
                    <th>NIS</th>
                    <th>ID Card</th>
                    <th>Nama</th>
                    <th>Gender</th>
                    <th>Kelas</th>
                    <th>Kamar</th>
                    <th>Status</th>
                    <th>Tanggal dibuat</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--MODAL-->
<div class="modal fade" id="siswa-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktif Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-inactive-form">
          <div class="form-group">
            <label>Tanggal</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                </div>
                <input data-provide="datepicker" name="tgl" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" required>
            </div>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="keterangan" class="form-control" required></textarea>
          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-warning" id="siswa-inactive-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-active-form">
          <div class="form-group">
            <label>Tanggal</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                </div>
                <input data-provide="datepicker" name="tgl" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" required>
            </div>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="keterangan" class="form-control" required></textarea>
          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-info" id="siswa-active-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-card-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nomor Kartu Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-card-form">
        <div class="form-group">
          <label>Nomor ID Card Siswa:</label>
          <span id="siswa-card-field"></span>
        </div>
        <div class="form-group">
          <label>ID Card Siswa Baru:</label>
          <input type="text" class="form-control" id="siswa-card-new" name="card_no" required>
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-info" id="siswa-card-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-kelas-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Kelas Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-kelas-form">
        <div class="form-group">
          <label>Kelas Siswa:</label>
          <span id="siswa-kelas-field"></span>
        </div>
        <div class="form-group">
          <label>Ganti Kelas:</label>
          <select name="id_kelasis" id="siswa-kelas-option" class="form-control"></select>
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-info" id="siswa-kelas-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-kamar-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Kamar Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-kamar-form">
        <div class="form-group">
          <label>Kamar Siswa:</label>
          <span id="siswa-kamar-field"></span>
        </div>
        <div class="form-group">
          <label>Ganti Kamar:</label>
          <select name="id_kamsis" id="siswa-kamar-option" class="form-control"></select>
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-info" id="siswa-kamar-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-document-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Dokumen Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-document-form">
        <div class="form-group">
          <label>Jenis Dokumen:</label>
          <select name="id_file" id="siswa-document-option" class="form-control"></select>
        </div>
        <div class="form-group">
          <label>File Dokumen:</label>
          <input 
            type="file" 
            class="dropify"
            id="siswa-document-field" 
            data-max-file-size="500K"
            required
          >
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-info" id="siswa-document-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>