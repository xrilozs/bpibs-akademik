<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Siswa</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('siswa/list');?>">Siswa</a></li>
                  <li class="breadcrumb-item active">Detail</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>Detail Siswa</h2>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a href="<?=base_url('siswa/list');?>" class="btn btn-secondary">Batal</a>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div id="form-wizard">
                        <div class="container-fluid">
                            <ul class="nav nav-pills nav-fill flex-nowrap overflow-auto" id="siswa-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pill1" data-toggle="pill" href="#data-siswa">Data Siswa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pill2" data-toggle="pill" href="#data-ortu">Data Orang Tua</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pill3" data-toggle="pill" href="#data-riwayat">Data Riwayat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pill4" data-toggle="pill" href="#data-info">Data Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pill5" data-toggle="pill" href="#data-document">Data Dokumen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pill6" data-toggle="pill" href="#kelasis-kamsis">Kelas dan Kamar</a>
                                </li>
                            </ul>

                            <div class="tab-content mt-2">
                                <div class="tab-pane fade show active" id="data-siswa">
                                    <section class="w-100">
                                        <form class="form my-4" id="siswa-form">
                                            <div class="row">
                                                <div class="col-lg-6 text-center">
                                                    <img src="<?=base_url('/assets/img/user.jpg');?>" id="siswa-foto" alt="poto siswa" style="height:300px;">
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">NIS:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="siswa-nis" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">NISN:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="siswa-nisn" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">NVA:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="siswa-nva" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">Panggilan:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="panggilan" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">Nama Lengkap:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="siswa-nama" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-4">Jenis Kelamin:</label>
                                                                <div class="col-8">
                                                                    <input type="text" name="siswa-kelamin" class="form-control-plaintext" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-5 mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Email:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-email" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">HP:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-hp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Tempat Lahir:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-tmplhr" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Tanggal Lahir:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-tgllhr" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Agama:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-agama" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Status Di Keluarga:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-statdk" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Anak Ke:</label>
                                                        <div class="col-8">
                                                            <input type="number" name="siswa-anakke" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Jumlah Saudara:</label>
                                                        <div class="col-8">
                                                            <input type="number" name="siswa-jmlsdr" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Alamat:</label>
                                                        <div class="col-8">
                                                            <textarea name="siswa-alamat" class="form-control-plaintext" style="resize:none;" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kelurahan:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-kel" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kecamatan:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-kec" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kota/Kabupaten:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-kokab" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Provinsi:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="siswa-prov" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                  
                                        </form>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="data-ortu">
                                    <section class="w-100">
                                        <form class="form my-4" id="ortu-form">
                                            <div class="row mt-5 mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nama Ayah:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ayah_nama" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Pekerjaan Ayah:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ayah_pekerjaan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nama Ibu:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ibu_nama" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Pekerjaan Ibu:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ibu_pekerjaan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nomor Handphone Ayah:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ayah_hp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nomor Handphone Ibu:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-ibu_hp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Penghasilan:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-penghasilan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Email:</label>
                                                        <div class="col-7">
                                                            <input type="email" name="ortu-email" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Alamat:</label>
                                                        <div class="col-7">
                                                            <textarea name="ortu-alamat" class="form-control-plaintext" style="resize:none;" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nama Wali:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-wali_nama" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Pekerjaan Wali:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-wali_pekerjaan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div class="row mx-lg-5">
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Nomor Handphone Wali:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-wali_hp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Penghasilan Wali:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-wali_penghasilan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Email Wali:</label>
                                                        <div class="col-7">
                                                            <input type="text" name="ortu-wali_email" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-5">Alamat Wali:</label>
                                                        <div class="col-7">
                                                            <textarea name="ortu-wali_alamat" class="form-control-plaintext" style="resize:none;" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="data-riwayat">
                                    <section class="w-100">
                                        <form class="form my-4" id="riwayat-form">
                                            <div class="row mt-5 mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Tanggal Tes:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-tgl_tes" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Hafalan:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-hafalan" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Sekolah Asal:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-asal_sekolah" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Tahun Lulus:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-thn_lulus" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Internal BP:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-internal_bp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Cabang BP:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-cabang_bp" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kompetensi Akademik:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-komp_ak" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <di class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kompetensi Non-akademik:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riawayat-komp_nonak" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kompetensi Mengaji:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-komp_mengaji" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <di class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Kompetensi Tulis Arab:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="riwayat-komp-tularab" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="data-info">
                                    <section class="w-100">
                                        <form class="form my-4 mt-5" id="info-form">
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Tanggal Masuk:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="info-tgl_masuk" class="form-control-plaintext" reaonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Ke Kelas:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="info-ke_kelas" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <di class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Info BPIBS:</label>
                                                        <div class="col-8">
                                                            <input type="text" name="info-info_bpibs" class="form-control-plaintext" readonly>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Alasan Orang Tuan:</label>
                                                        <div class="col-8">
                                                            <textarea name="info-alasan_ortu" class="form-control-plaintext" style="resize:none;"   readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <di class="col-lg-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Alasan Siswa:</label>
                                                        <div class="col-8">
                                                            <textarea name="info-alasan_ssiwa" class="form-control-plaintext" style="resize:none;" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </di>
                                            </div>
                                            <div class="row mx-lg-5">
                                                <div class="col-lg-12">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Keterangan:</label>
                                                        <div class="col-8">
                                                            <textarea name="info-keterangan" class="form-control-plaintext" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="data-document">
                                    <section class="w-100">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable" id="siswa-file-datatable" style="width:99%">
                                                <thead>
                                                    <tr>
                                                        <th>Kategori</th>
                                                        <th>Nama File</th>
                                                        <th>Tipe File</th>
                                                        <th>Ukuran File</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="kelasis-kamsis">
                                    <section class="w-100">
                                        <div class="row mx-lg-5 my-4">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover dataTable" id="kelasis-datatable" style="width:99%">
                                                        <thead>
                                                            <tr>
                                                                <th>Kelas</th>
                                                                <th>Tahun Ajaran</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mx-lg-5 mb-4">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover dataTable" id="kamsis-datatable" style="width:99%">
                                                        <thead>
                                                            <tr>
                                                                <th>Kamar</th>
                                                                <th>Tahun Ajaran</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="siswa-file-edit-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Dokumen Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="siswa-file-edit-form">
        <div class="form-group">
          <label>Jenis Dokumen:</label>
          <select name="id_file" id="siswa-file-kat-option" class="form-control"></select>
        </div>
        <div class="form-group">
          <label>File Dokumen:</label>
          <input 
            type="file"
            id="siswa-document-field" 
          >
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-info" id="siswa-file-edit-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="siswa-file-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus dokumen Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin untuk menghapus dokumen siswa ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="siswa-file-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>