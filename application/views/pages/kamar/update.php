<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Kamar</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('kamar/list');?>">Kamar</a></li>
                  <li class="breadcrumb-item active">Ubah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Ubah Kamar</h2>
                </div>
                <div class="body">
                <form id="kamar-update-form">
                        <div class="form-group">
                            <label for="food">Ajaran</label>
                            <select name="ajaran" class="form-control kamar-ajaran-option" id="kamar-update-ajaran" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kamar</label>
                            <input type="text" name="kamar" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Tipe</label>
                            <select name="tipe" class="form-control kamar-tipe-option" id="kamar-update-tipe" required>
                                <option value="Ikhwan">Ikhwan</option>
                                <option value="Akhwat">Akhwat</option>
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('kamar/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-primary" id="kamar-update-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>