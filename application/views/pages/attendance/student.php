<!doctype html>
<html lang="en">

<head>
<title>BPIBS - Absensi Masuk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="HexaBit Bootstrap 4x Admin Template">
<meta name="author" content="WrapTheme, www.thememakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/main.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/color_skins.css');?>">
<style>
body {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
    margin: 0;
}
</style>
</head>

<body>
    <!-- WRAPPER -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="card" style="height: 300px;">
                        <div class="header">
                            <div class="row">
                                <div class="col-lg-6 d-flex justify-content-end">
                                    <img src="<?=base_url('assets/img/logo-transparent.png');?>" style="width:100px;" alt="Logo">
                                </div>
                                <div class="col-lg-6 mt-3 font-weight-bold">
                                        <h2>BPIBS</h2>
                                        <h2>Absensi Masuk</h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <form class="form-auth-small" id="attendance-form">
                                <div class="row mb-4">
                                    <div class="col-lg-12 d-flex justify-content-center">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="idcard" name="scan-option" value="idcard" checked>
                                            <label class="form-check-label" for="idcard">ID Card</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="nis" name="scan-option" value="nis">
                                            <label class="form-check-label" for="nis">NIS</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="control-label sr-only">ID Card/NIS</label>
                                            <input type="text" name="scan-value" class="form-control" placeholder="Masukkan ID Card/NIS">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" id="attendance-button" class="btn btn-primary btn-lg btn-block">Absensi Masuk</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6  d-flex align-items-center">
                    <div class="card" style="height: 300px;">
                        <div class="card-body py-0">
                            <div id="scan-result" class="py-4" style="display:none;">
                                <form class="form" id="siswa-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>NIS</label>
                                                <input type="text" name="siswa-nis" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nama Lengkap</label>
                                                <input type="text" name="siswa-nama" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Jenis Kelamin</label>
                                                <select name="kelamin" class="form-control" id=" siswa-kelamin-option" disabled>
                                                    <option value="L">Laki-laki</option>
                                                    <option value="P">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="no-scan" class="card-text">
                                <div class="row" style="height:300px;">
                                    <div class="col-lg-12  d-flex align-items-center justify-content-center">
                                        <b>Silahkan Scan ID Anda</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- END WRAPPER -->
  
<script src="<?=base_url('assets/theme/light/assets/bundles/libscripts.bundle.js');?>"></script>    
<script src="<?=base_url('assets/theme/light/assets/bundles/vendorscripts.bundle.js');?>"></script>
<script src="<?=base_url('assets/theme/light/assets/bundles/mainscripts.bundle.js');?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.all.min.js"></script>

<script>
    const WEB_URL = "<?=base_url()?>"
    const API_URL = "<?=base_url('api')?>"
</script>
<script src="<?=base_url('assets/js/common.js');?>"></script>
<script src="<?=base_url('assets/js/attendance.js');?>"></script>
</body>
</html>
