    <div id="main-content">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                </div>            
                <div class="col-md-6 col-sm-12 text-right">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="card top_report">
                                <div class="row clearfix">
                                    <div class="col-lg-12">
                                        <div class="body">
                                            <div class="clearfix">
                                                <div class="float-left">
                                                    <i class="fa fa-4x fa-users text-col-blue"></i>
                                                </div>
                                                <div class="number float-right text-right">
                                                    <h3>Total Siswa</h3>
                                                    <span style="font-size:32px;" class="font700" id="total-siswa">0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="row clearfix">
                                    <div class="col-lg-12">
                                        <div class="body">
                                            <div class="clearfix">
                                                <div class="float-left">
                                                    <i class="fa fa-2x fa-male text-col-blue"></i>
                                                </div>
                                                <div class="number float-right text-right">
                                                    <h6>Total Siswa Ikhwan</h6>
                                                    <span class="font700" id="total-siswa-male">0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="row clearfix">
                                    <div class="col-lg-12">
                                        <div class="body">
                                            <div class="clearfix">
                                                <div class="float-left">
                                                    <i class="fa fa-2x fa-female text-col-blue"></i>
                                                </div>
                                                <div class="number float-right text-right">
                                                    <h6>Total Siswa Akhwat</h6>
                                                    <span class="font700" id="total-siswa-female">0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="body">
                            <div class="e_chart" id="echart-total-siswa-gender-chart" style="height: 200%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-12">
                    <ul class="nav nav-fill nav-tabs-new">
                        <li class="nav-item"><a class="nav-link bg-warning text-white active show" data-toggle="tab" href="#kelas-7">Kelas 7</a></li>
                        <li class="nav-item"><a class="nav-link bg-warning text-white" data-toggle="tab" href="#kelas-8">Kelas 8</a></li>
                        <li class="nav-item"><a class="nav-link bg-warning text-white" data-toggle="tab" href="#kelas-9">Kelas 9</a></li>
                        <li class="nav-item"><a class="nav-link bg-warning text-white" data-toggle="tab" href="#kelas-10">Kelas 10</a></li>
                        <li class="nav-item"><a class="nav-link bg-warning text-white" data-toggle="tab" href="#kelas-11">Kelas 11</a></li>
                        <li class="nav-item"><a class="nav-link bg-warning text-white" data-toggle="tab" href="#kelas-12">Kelas 12</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane show active" id="kelas-7">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card mb-4 top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 7</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-7">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-7">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-7" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kelas-8">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 8</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-8">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-8">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-8" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kelas-9">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 9</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-9">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-9">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-9" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kelas-10">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 10</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-10">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-10">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-10" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kelas-11">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 11</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-11">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-11">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-11" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kelas-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card top_report">
                                                <div class="body">
                                                    <div class="clearfix">
                                                        <div class="float-left">
                                                            <i class="fa fa-4x fa-users text-col-blue"></i>
                                                        </div>
                                                        <div class="number float-right text-right">
                                                            <h3>Total Siswa Kelas 12</h3>
                                                            <span style="font-size:32px;" class="font700" id="total-siswa-kelas-12">0</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="list-total-siswa-kelas-12">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card">
                                        <div class="body">
                                            <div class="e_chart" id="echart-total-siswa-kelas-12" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
