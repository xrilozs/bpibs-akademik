<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Mata Pelajaran</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('mapel/list');?>">Mata Pelajaran</a></li>
                  <li class="breadcrumb-item active">Ubah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Ubah Mata Pelajaran</h2>
                </div>
                <div class="body">
                    <form id="mapel-update-form">
                        <div class="form-group">
                            <label>Urutan</label>
                            <input type="number" name="urutan" class="form-control" min="1" required>
                        </div>
                        <div class="form-group">
                            <label for="food">Kategori</label>
                            <select name="kategori_mapel" class="form-control mapel-kategori-option" id="mapel-update-kategori" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Mata Pelajaran</label>
                            <input type="text" name="mapel" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Detail</label>
                            <input type="text" name="mapel_det" class="form-control" required>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('mapel/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-lg btn-primary" id="mapel-update-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>