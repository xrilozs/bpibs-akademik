<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Guru</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">Guru</li>
                  <li class="breadcrumb-item active">Data Guru</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="card">
          <div class="header">
            <h2>Data Guru</h2>
            <ul class="header-dropdown dropdown dropdown-animated scale-left">
              <li> 
                <a href="<?=base_url('guru/create');?>" class="btn btn-sm btn-primary text-white" title="">Buat baru</a>
              </li>
              <li> 
                <a id="refresh-table" href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="guru-datatable">
                <thead>
                  <tr>
                    <th>NID</th>
                    <th>NPP</th>
                    <th>Nama</th>
                    <th>Kelamin</th>
                    <th>Email</th>
                    <th>HP</th>
                    <th>Tanggal Bergabung</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--MODAL-->
<div class="modal fade" id="guru-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Guru</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data guru ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="guru-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guru-add-fam-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Keluarga</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="guru-add-fam-form">
          <div class="form-group">
            <label>Keluarga</label>
            <select id="guru-fam-title-option" class="form-control"></select>
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="f_name" class="form-control">
          </div>
          <div class="form-group">
            <label>Tanggal lahir</label>
            <input data-provide="datepicker" name="f_dob" data-date-autoclose="true" class="form-control" required>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Edukasi</label>
                <input type="text" name="f_edu" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label>Pekerjaan</label>
                <input type="text" name="f_job" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Etnis</label>
            <input type="text" name="f_ethnic" class="form-control">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="f_phone" class="form-control">
          </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-primary" id="guru-add-fam-button">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guru-add-edu-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Edukasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="guru-add-edu-form">
          <div class="form-group">
            <label>Level Edukasi</label>
            <select id="guru-edu-lvl-option" class="form-control"></select>
          </div>
          <div class="form-group">
            <label>Sekolah</label>
            <input type="text" name="sekolah" class="form-control">
          </div>
          <div class="form-group">
            <label>Jurusan</label>
            <input type="text" name="jurusan" class="form-control">
          </div>
          <div class="form-group">
            <label>Tahun Lulus</label>
            <input type="number" name="thnlulus" class="form-control">
          </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-primary" id="guru-add-edu-button">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>
