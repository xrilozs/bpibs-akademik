<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Guru</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('guru/list');?>">Guru</a></li>
                  <li class="breadcrumb-item active">Detail</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Detail Guru</h2>
                </div>
                <div class="body">
                    <form class="form" id="guru-detail-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="col-form-label">Foto:</label>
                                <div id="guru-detail-foto" class="d-flex justify-content-center"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">NID:</label>
                                            <div class="col-9">
                                              <input type="text" name="nid" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">NPP:</label>
                                            <div class="col-9">
                                              <input type="text" name="npp" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Nama:</label>
                                            <div class="col-9">
                                              <input type="text" name="nama" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Jenis Kelamin:</label>
                                            <div class="col-9">
                                              <input type="text" name="kelamin" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-6">
                                <div class="form-group row ">
                                    <label class="col-3 col-form-label">Tempat Lahir:</label>
                                    <div class="col-9">
                                      <input type="text" name="tlhr" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Tanggal Lahir:</label>
                                    <div class="col-9">
                                      <input type="text" name="tgllhr" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Email:</label>
                                    <div class="col-9">
                                      <input type="text" name="email" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">HP:</label>
                                    <div class="col-9">
                                      <input type="text" name="hp" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Status Nikah:</label>
                                    <div class="col-9">
                                      <input type="text" name="nikah" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Golongan Darah:</label>
                                    <div class="col-9">
                                      <input type="text" name="goldar" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Tinggi Badan:</label>
                                    <div class="col-9">
                                      <input type="text" name="tb" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Berat Badan:</label>
                                    <div class="col-9">
                                      <input type="text" name="bb" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Ukuran Baju:</label>
                                    <div class="col-9">
                                      <input type="text" name="bj" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Tanggal Join:</label>
                                    <div class="col-9">
                                      <input type="text" name="tgljoin" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <label  class="col-form-label">Edukasi:</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" id="guru-edu-datatable" style="width:99%;">
                                    <thead>
                                        <tr>
                                            <th>Derajat</th>
                                            <th>Sekolah</th>
                                            <th>Jurusan</th>
                                            <th>Tahun Lulus</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label  class="col-form-label">Keluarga:</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" id="guru-fam-datatable" style="width:99%">
                                    <thead>
                                        <tr>
                                            <th>Keluarga</th>
                                            <th>Nama</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Edukasi</th>
                                            <th>Pekerjaan</th>
                                            <th>Etnis</th>
                                            <th>Telepon</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                      <div class="col-lg-12 text-right">
                        <a href="<?=base_url('guru/list');?>" class="btn btn-secondary">Kembali</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<!--MODAL-->
<div class="modal fade" id="guru-edu-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Edukasi Guru</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data edukasi guru ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="guru-edu-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guru-fam-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Hubungan Guru</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data hubungan guru ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="guru-fam-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guru-fam-edit-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Hubungan Guru</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="guru-fam-edit-form">
          <div class="form-group row">
            <label  class="col-3 col-form-label">Keluarga</label>
            <select id="guru-fam-title-option" class="form-control-plaintext"></select>
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Nama</label>
            <input type="text" name="f_name" class="form-control-plaintext">
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Tanggal lahir</label>
            <input data-provide="datepicker" name="f_dob" data-date-autoclose="true" class="form-control-plaintext" required>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group row">
                <label  class="col-3 col-form-label">Edukasi</label>
                <input type="text" name="f_edu" class="form-control-plaintext">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group row">
                <label  class="col-3 col-form-label">Pekerjaan</label>
                <input type="text" name="f_job" class="form-control-plaintext">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Etnis</label>
            <input type="text" name="f_ethnic" class="form-control-plaintext">
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Telepon</label>
            <input type="text" name="f_phone" class="form-control-plaintext">
          </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-primary" id="guru-fam-edit-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guru-edu-edit-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Edukasi Guru</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="guru-edu-edit-form">
          <div class="form-group row">
            <label  class="col-3 col-form-label">Level Edukasi</label>
            <select id="guru-edu-lvl-option" class="form-control-plaintext"></select>
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Sekolah</label>
            <input type="text" name="sekolah" class="form-control-plaintext">
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Jurusan</label>
            <input type="text" name="jurusan" class="form-control-plaintext">
          </div>
          <div class="form-group row">
            <label  class="col-3 col-form-label">Tahun Lulus</label>
            <input type="number" name="thnlulus" class="form-control-plaintext">
          </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-primary" id="guru-edu-edit-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>