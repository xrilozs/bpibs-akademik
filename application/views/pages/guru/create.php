<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Guru</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('guru/list');?>">Guru</a></li>
                  <li class="breadcrumb-item active">Tambah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Tambah Guru</h2>
                </div>
                <div class="body">
                    <form class="form" id="guru-create-form">
                        <div class="form-group">
                            <label>Foto:</label>
                            <input 
                                type="file" 
                                class="dropify"
                                id="guru-create-foto"
                                data-allowed-file-extensions="jpeg jpg png" 
                                data-max-file-size="500K"
                            >
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>NID</label>
                                    <input type="text" name="nid" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>NPP</label>
                                    <input type="text" name="npp" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" name="tlhr" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <div class="input-group mb-3">                                        
                                        <input data-provide="datepicker" name="tgllhr" data-date-autoclose="true" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="kelamin" class="form-control guru-kelamin-option" id="guru-create-kelamin" required>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>HP</label>
                                    <input type="text" name="hp" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Status Nikah</label>
                                    <select name="kelamin" class="form-control guru-nikah-option" id="guru-create-nikah" required>
                                        <option value="1">Sudah menikah</option>
                                        <option value="0">Belum menikah</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tinggi Badan</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="tb" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">CM</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Berat Badan</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="bb" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Golongan Darah</label>
                                    <input type="text" name="goldar" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Ukuran Baju</label>
                                    <input type="text" name="bj" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tanggal Join</label>
                                    <div class="input-group mb-3">                                        
                                        <input data-provide="datepicker" name="tgljoin" data-date-autoclose="true" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <a href="<?=base_url('guru/list');?>" class="btn btn-secondary">Batal</a>
                                <button class="btn btn-primary" id="guru-create-button">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>