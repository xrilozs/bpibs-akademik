<!doctype html>
<html lang="en">

<head>
<title>BPIBS Akademik - Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="HexaBit Bootstrap 4x Admin Template">
<meta name="author" content="WrapTheme, www.thememakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/main.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/color_skins.css');?>">
<style>
body {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
    margin: 0;
}
</style>
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper" class="auth-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12  d-flex align-items-center">
                    <div class="card">
                        <div class="header d-flex justify-content-center">
                            <img src="<?=base_url('assets/img/logo-transparent.png');?>" style="width:200px;" alt="Logo">
                        </div>
                        <div class="body">
                            <h4 class="text-center">BPIBS - Sistem Akademik</h4>
                            <form class="form-auth-small" id="login-form">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email/Username</label>
                                    <input type="text" name="email" class="form-control" id="signin-email" placeholder="Email/Username">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Kata Sandi</label>
                                    <input type="password" name="password" class="form-control" id="signin-password" placeholder="Kata sandi">
                                </div>
                                <button type="submit" id="login-button" class="btn btn-primary btn-lg btn-block">Masuk</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END WRAPPER -->
  
<script src="<?=base_url('assets/theme/light/assets/bundles/libscripts.bundle.js');?>"></script>    
<script src="<?=base_url('assets/theme/light/assets/bundles/vendorscripts.bundle.js');?>"></script>
<script src="<?=base_url('assets/theme/light/assets/bundles/mainscripts.bundle.js');?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.all.min.js"></script>

<script>
    const API_URL = "<?=base_url('api')?>"
</script>
<script src="<?=base_url('assets/js/login.js');?>"></script>
</body>
</html>
