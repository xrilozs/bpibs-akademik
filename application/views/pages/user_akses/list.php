<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>User Akses</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">User</li>
                  <li class="breadcrumb-item active">User Akses</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="card">
          <div class="header">
            <h2>Daftar User Akses</h2>
            <ul class="header-dropdown dropdown dropdown-animated scale-left">
              <li>
                <a id="refresh-table" href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="user-akses-datatable">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Menu</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
