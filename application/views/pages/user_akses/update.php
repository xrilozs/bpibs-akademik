<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>User Akses</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">User</li>
                  <li class="breadcrumb-item"><a href="<?=base_url('user-akses/list');?>">User Akses</a></li>
                  <li class="breadcrumb-item active">Ubah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Ubah User Akses</h2>
                </div>
                <div class="body">
                    <form id="user-akses-update-form">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="hidden" name="id" class="form-control" disabled>
                            <input type="text" name="name" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="hidden" name="user_id" class="form-control" disabled>
                            <input type="email" name="email" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="hidden" name="app_id" class="form-control" disabled>
                            <input type="text" name="username" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <input type="hidden" name="role_id" class="form-control" disabled>
                            <input type="text" name="role" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Menu yang bisa diakses</label>
                            <select class="select2 form-control" name="menu_id" id="menu-option" multiple="multiple" data-placeholder="Pilih menu" style="width: 100%;"></select>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('user-akses/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-primary" id="user-akses-update-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>