<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>User Login</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">User</li>
                  <li class="breadcrumb-item"><a href="<?=base_url('user-login/list');?>">User Login</a></li>
                  <li class="breadcrumb-item active">Ubah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Ubah User Login</h2>
                </div>
                <div class="body">
                    <form id="user-login-update-form">
                        <div class="form-group">
                            <label for="blog-image-update-field">Foto:</label>
                            <input 
                                type="file"
                                id="user-login-update-image"
                                data-allowed-file-extensions="jpeg jpg png" 
                            >
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="food">Role</label>
                            <select name="role" class="form-control user-login-role-option" id="user-login-update-role" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <div id="user-login-update-status"></div>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('user-login/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-primary" id="user-login-update-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>