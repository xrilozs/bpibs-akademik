<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Absensi</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">Siswa</li>
                  <li class="breadcrumb-item active">Absensi</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="accordion" id="filter-accordion">
          <div class="card">
            <div class="card-header" id="headingOne" style="background-color:white;">
              <h2 class="mb-0">
                <button class="btn btn-block text-between" type="button" data-toggle="collapse" data-target="#filter-section" aria-expanded="true" aria-controls="collapseOne">
                  Cari <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
              </h2>
            </div>
            <div id="filter-section" class="collapse show" aria-labelledby="headingOne" data-parent="#filter-accordion">
              <div class="card-body">
              <form class="form" id="absensi-search-form">
                <div class="row mb-4">
                  <!-- <div class="col-lg-6">
                    <label>Semester</label>
                    <select name="filter-semester-option" class="form-control" id="filter-semester-option" required>
                      <option value="">-</option>
                    </select>
                  </div> -->
                  <div class="col-lg-12">
                    <label>Mata Pelajaran</label>
                    <select name="filter-mapel-option" class="form-control" id="filter-mapel-option" required>
                      <option value="">-</option>
                    </select>
                  </div>
                </div>
                <div class="row mb-4">
                  <div class="col-lg-6">
                    <label>Kelas</label>
                    <select name="filter-kelas-option" class="form-control" id="filter-kelas-option" required>
                      <option value="">-</option>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label>Tanggal</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </div>
                        <input data-provide="datepicker" name="tanggal" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" required>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary btn-block" id="absensi-search-button">Cari</button>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card" id="absensi-siswa-section" style="display:none;">
          <div class="header">
            <h2>Data Absensi Siswa</h2>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="siswa-datatable">
                <thead>
                  <tr>
                    <th><input type="checkbox" id="select-all-checkbox"></th>
                    <th>Keterangan Absen</th>
                    <th>Deskripsi</th>
                    <th>NIS</th>
                    <!-- <th>ID Card</th> -->
                    <th>Nama</th>
                    <!-- <th>Gender</th> -->
                    <th>Kelas</th>
                    <th>Kamar</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="row mt-4">
              <div class="col-lg-12 d-flex justify-content-end">
                <button class="btn btn-primary btn-lg" id="absensi-button">
                  <i class="fa fa-pencil" aria-hidden="true"></i> Absensi
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="alert alert-danger" id="absensi-siswa-alert" role="alert" style="display:none;">
          <b>Jadwal Mata Pelajaran Tidak Ditemukan!</b>
        </div>
      </div>
    </div>
  </div>
</div>
