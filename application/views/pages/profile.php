<div id="main-content">
  <!-- <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Guru</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('guru/list');?>">Guru</a></li>
                  <li class="breadcrumb-item active">Detail</li>
              </ul>
          </div>
      </div>
  </div> -->
  <div class="container-fluid mt-5">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h1>Profilku</h1>
                </div>
                <div class="body">
                    <form class="form" id="guru-detail-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="col-form-label">Foto:</label>
                                <div id="guru-detail-foto" class="d-flex justify-content-center"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">NID:</label>
                                            <div class="col-9">
                                              <input type="text" name="nid" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">NPP:</label>
                                            <div class="col-9">
                                              <input type="text" name="npp" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Nama:</label>
                                            <div class="col-9">
                                              <input type="text" name="nama" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Jenis Kelamin:</label>
                                            <div class="col-9">
                                              <input type="text" name="kelamin" class="form-control-plaintext" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-6">
                                <div class="form-group row ">
                                    <label class="col-3 col-form-label">Tempat Lahir:</label>
                                    <div class="col-9">
                                      <input type="text" name="tlhr" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Tanggal Lahir:</label>
                                    <div class="col-9">
                                      <input type="text" name="tgllhr" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Email:</label>
                                    <div class="col-9">
                                      <input type="text" name="email" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">HP:</label>
                                    <div class="col-9">
                                      <input type="text" name="hp" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Status Nikah:</label>
                                    <div class="col-9">
                                      <input type="text" name="nikah" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Golongan Darah:</label>
                                    <div class="col-9">
                                      <input type="text" name="goldar" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Tinggi Badan:</label>
                                    <div class="col-9">
                                      <input type="text" name="tb" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Berat Badan:</label>
                                    <div class="col-9">
                                      <input type="text" name="bb" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Ukuran Baju:</label>
                                    <div class="col-9">
                                      <input type="text" name="bj" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Tanggal Join:</label>
                                    <div class="col-9">
                                      <input type="text" name="tgljoin" class="form-control-plaintext" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <label  class="col-form-label">Edukasi:</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" id="guru-edu-datatable" style="width:99%;">
                                    <thead>
                                        <tr>
                                            <th>Derajat</th>
                                            <th>Sekolah</th>
                                            <th>Jurusan</th>
                                            <th>Tahun Lulus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label  class="col-form-label">Keluarga:</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" id="guru-fam-datatable" style="width:99%">
                                    <thead>
                                        <tr>
                                            <th>Keluarga</th>
                                            <th>Nama</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Edukasi</th>
                                            <th>Pekerjaan</th>
                                            <th>Etnis</th>
                                            <th>Telepon</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row mt-4">
                      <div class="col-lg-12 text-right">
                        <a href="<?=base_url('guru/list');?>" class="btn btn-secondary">Kembali</a>
                      </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
