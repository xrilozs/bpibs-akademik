<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Jadwal Mata Pelajaran</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">Jadwal Mata Pelajaran</li>
                  <li class="breadcrumb-item active">Kalender</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="accordion" id="filter-accordion">
          <div class="card">
            <div class="card-header" id="headingOne" style="background-color:white;">
              <h2 class="mb-0">
                <button class="btn btn-block text-between" type="button" data-toggle="collapse" data-target="#filter-section" aria-expanded="true" aria-controls="collapseOne">
                  Filter <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
              </h2>
            </div>
            <div id="filter-section" class="collapse show" aria-labelledby="headingOne" data-parent="#filter-accordion">
              <div class="card-body">
                <form class="form" id="calendar-search-form">
                <div class="row mb-4">
                  <div class="col-lg-6">
                    <label>Semester</label>
                    <select name="filter-semester-option" class="form-control" id="filter-semester-option">
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label>Kelas</label>
                    <select name="filter-kelas-option" class="form-control" id="filter-kelas-option">
                    </select>
                  </div>
                  <div class="col-lg-12 mt-4">
                    <button type="submit" class="btn btn-primary btn-block" id="calendar-search-button">Cari</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12" id="calendar-section" style="display:none;">
        <div class="card">
          <div class="body px-0 py-0">
            <div class="row">
              <div class="col-lg-2 pr-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card mb-2 text-center font-weight-bold p-2" style="box-shadow: none;">
                      Senin
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="row" id="calendar-monday">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 px-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card mb-2 text-center font-weight-bold p-2" style="box-shadow: none;">
                      Selasa
                    </div>
                    <div class="col-lg-12">
                      <div class="row" id="calendar-tuesday">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 px-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card mb-2 text-center font-weight-bold p-2" style="box-shadow: none;">
                      Rabu
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="row" id="calendar-wednesday">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 px-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card mb-2 text-center font-weight-bold p-2" style="box-shadow: none;">
                      Kamis
                    </div>
                    <div class="col-lg-12">
                      <div class="row" id="calendar-thursday">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 px-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card mb-2 text-center font-weight-bold p-2" style="box-shadow: none;">
                      Jum'at
                    </div>
                    <div class="col-lg-12">
                      <div class="row" id="calendar-friday">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 pl-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card text-center font-weight-bold p-2" style="box-shadow: none;">
                      Sabtu
                    </div>
                    <div class="col-lg-12">
                      <div class="row" id="calendar-saturday">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="alert alert-danger" id="calendar-alert" role="alert" style="display:none;">
          <b>Jadwal Mata Pelajaran Tidak Ditemukan!</b>
        </div>
      </div>
    </div>
  </div>
</div>
