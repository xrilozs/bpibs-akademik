<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Jadwal Mata Pelajaran</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('jadwal-mapel/list');?>">Jadwal Mata Pelajaran</a></li>
                  <li class="breadcrumb-item active">Ubah</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Ubah Jadwal Mata Pelajaran</h2>
                </div>
                <div class="body">
                <form id="jadwal-mapel-update-form">
                        <div class="form-group">
                            <label>Semester</label>
                            <select name="semester" class="form-control jadwal-mapel-semester-option" id="jadwal-mapel-update-semester" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" class="form-control jadwal-mapel-kelas-option" id="jadwal-mapel-update-kelas" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mata Pelajaran</label>
                            <select name="mapel" class="form-control jadwal-mapel-mapel-option" id="jadwal-mapel-update-mapel" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Hari</label>
                            <select name="hari" class="form-control jadwal-mapel-hari-option" id="jadwal-mapel-update-hari" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jam Awal</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-clock"></i></span>
                                </div>
                                <input type="text" name="awal" class="form-control time24" placeholder="Ex: 23:59">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jam Akhir</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-clock"></i></span>
                                </div>
                                <input type="text" name="akhir" class="form-control time24" placeholder="Ex: 23:59">
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('jadwal-mapel/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-primary" id="jadwal-mapel-update-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>