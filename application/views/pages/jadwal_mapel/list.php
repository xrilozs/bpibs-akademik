<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Jadwal Mata Pelajaran</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item">Jadwal Mata Pelajaran</li>
                  <li class="breadcrumb-item active">Data Jadwal Mata Pelajaran</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="accordion" id="filter-accordion">
          <div class="card">
            <div class="card-header" id="headingOne" style="background-color:white;">
              <h2 class="mb-0">
                <button class="btn btn-block text-between" type="button" data-toggle="collapse" data-target="#filter-section" aria-expanded="true" aria-controls="collapseOne">
                  Filter <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
              </h2>
            </div>
            <div id="filter-section" class="collapse show" aria-labelledby="headingOne" data-parent="#filter-accordion">
              <div class="card-body">
                <div class="row mb-4">
                  <div class="col-lg-4">
                    <label>Kelas</label>
                    <select name="filter-kelas-option" class="form-control" id="filter-kelas-option">
                      <option value="">Semua</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
          <div class="header">
            <h2>Data Jadwal Mata Pelajaran</h2>
            <ul class="header-dropdown dropdown dropdown-animated scale-left">
              <li>
                <a href="<?=base_url('jadwal-mapel/create');?>" class="btn btn-sm btn-primary text-white" title="">Buat baru</a>
              </li>
              <li> 
                <a id="refresh-table" href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="jadwal-mapel-datatable">
                <thead>
                  <tr>
                    <th>Semester</th>
                    <th>Kelas</th>
                    <th>Mata Pelajaran</th>
                    <th>Hari</th>
                    <th>Jam Awal</th>
                    <th>Jam Akhir</th>
                    <th>Tanggal dibuat</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--MODAL-->
<div class="modal fade" id="jadwal-mapel-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Jadwal Mata Pelajaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin untuk menghapus mata pelajaran ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="jadwal-mapel-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>
