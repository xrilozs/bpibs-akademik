<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Jadwal Mata Pelajaran</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('jadwal-mapel/list');?>">Jadwal Mata Pelajaran</a></li>
                  <li class="breadcrumb-item active">Detail</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Detail Jadwal Mata Pelajaran</h2>
                </div>
                <div class="body">
                    <form class="form my-4" id="jadwal-mapel-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Semester</label>
                                    <input type="text" name="semester" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Kelas</label>
                                    <input type="text" name="kelas" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Mata Pelajaran</label>
                                    <input type="text" name="mapel" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Hari</label>
                                    <input type="text" name="hari" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jam Awal</label>
                                    <input type="text" name="awal" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jam Akhir</label>
                                    <input type="text" name="akhir" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-4 text-right>
                               <a href="<?=base_url('jadwal-mapel/list');?>" class="btn btn-secondary">Kembali</a>
                            </div>
                        </div>                
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>