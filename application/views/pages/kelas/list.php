<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Kelas</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item active">Kelas</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="card">
          <div class="header">
            <h2>Daftar Kelas</h2>
            <ul class="header-dropdown dropdown dropdown-animated scale-left">
              <li>
                <a href="<?=base_url('kelas/create');?>" class="btn btn-sm btn-primary text-white" title="">Buat baru</a>
              </li>
              <li> 
                <a id="refresh-table" href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dataTable" id="kelas-datatable">
                <thead>
                  <tr>
                    <th>Tahun Ajaran</th>
                    <th>KD Kelas</th>
                    <th>Kelas</th>
                    <th>Tipe</th>
                    <th>Tanggal dibuat</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
