<div id="main-content">
  <div class="block-header">
      <div class="row clearfix">
          <div class="col-md-6 col-sm-12">
              <h2>Kelas</h2>
          </div>            
          <div class="col-md-6 col-sm-12 text-right">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?=base_url('kelas/list');?>">Kelas</a></li>
                  <li class="breadcrumb-item active">Buat</li>
              </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Buat Kelas</h2>
                </div>
                <div class="body">
                    <form id="kelas-create-form">
                        <div class="form-group">
                            <label for="food">Ajaran</label>
                            <select name="ajaran" class="form-control kelas-ajaran-option" id="kelas-create-ajaran" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>KD Kelas</label>
                            <input type="text" name="kdkelas" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Kelas</label>
                            <input type="text" name="kelas" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Tipe</label>
                            <select name="tipe" class="form-control kelas-tipe-option" id="kelas-create-tipe" required>
                                <option value="Ikhwan">Ikhwan</option>
                                <option value="Akhwat">Akhwat</option>
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?=base_url('kelas/list');?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-primary" id="kelas-create-button">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>