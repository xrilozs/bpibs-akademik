<!doctype html>
<html lang="en">
<head>
<title>BPIBS Akademik - <?=$title;?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Sistem akademik sekolah BPIBS">
<meta name="author" content="XRilozs">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/font-awesome/css/font-awesome.min.css');?>">

<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css');?>">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/parsleyjs/css/parsley.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/dropify/css/dropify.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/jquery-steps/jquery.steps.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/vendor/select2/css/select2.min.css');?>">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=base_url('assets/css/theme.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/main.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/theme/light/assets/css/color_skins.css');?>">
</head>
<body class="theme-orange">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="<?=base_url('assets/img/logo-transparent.png');?>" width="48" height="48" alt="HexaBit"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="row" style="width:100%;">
            <div class="col-lg-6 col-6">
                <div class="navbar-btn">
                    <a href="<?=base_url();?>"><img src="<?=base_url('assets/img/logo-transparent.png');?>" alt="HexaBit Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                
                <div class="navbar-menu">
                    <ul class="nav navbar-nav btn-toggle-fullwidth">
                        <li>
                            <a href="javascript:void(0);" class="icon-menu"><i class="fa fa-arrow-left"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="col-lg-6 col-6 d-flex justify-content-end">
                <div class="navbar-menu d-flex align-items-center mr-4">
                    <select id="header-ajaran-option" class="form-control form-control-sm">
                        <option value="">2023 / 2024</option>
                    </select>
                </div>
                <div class="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown dropdown-animated scale-left text-right">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <strong id="header-username">User</strong>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="mb-3 none" id="header-profile">
                                    <a href="<?=base_url('profilku');?>" id="header-profile-button">
                                        <i class="icon-user"></i>&nbsp;
                                        <b>Profilku</b>
                                    </a>
                                </li>
                                <li  class="mb-3">
                                    <a href="#" id="header-password-button" data-toggle="modal" data-target="#password-modal">
                                        <i class="icon-lock"></i>&nbsp;
                                        <b>Kata Sandi</b>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li  class="mb-3" id="logout-button">
                                    <a href="#">
                                        <i class="icon-power"></i>&nbsp;
                                        <b>Keluar</b>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
    </nav>