
    <div id="rightbar" class="rightbar">
        <ul class="nav nav-tabs-new">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting">Settings</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat">Chat</a></li>            
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="setting">
                <div class="slim_scroll">
                    <div class="card">
                        <h6>Choose Theme</h6>
                        <ul class="choose-skin list-unstyled mb-0">
                            <li data-theme="purple"><div class="purple"></div></li>
                            <li data-theme="green"><div class="green"></div></li>
                            <li data-theme="orange" class="active"><div class="orange"></div></li>
                            <li data-theme="blue"><div class="blue"></div></li>
                            <li data-theme="blush"><div class="blush"></div></li>
                            <li data-theme="cyan"><div class="cyan"></div></li>
                        </ul>
                    </div>
                    <div class="card">
                        <h6>General Settings</h6>
                        <ul class="setting-list list-unstyled mb-0">
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox">
                                    <span>Report Panel Usag</span>
                                </label>
                            </li>
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox" checked>
                                    <span>Email Redirect</span>
                                </label>
                            </li>
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox" checked>
                                    <span>Notifications</span>
                                </label>                      
                            </li>
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox">
                                    <span>Auto Updates</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <h6>Account Settings</h6>
                        <ul class="setting-list list-unstyled mb-0">
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox">
                                    <span>Offline</span>
                                </label>
                            </li>
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox" checked>
                                    <span>Location Permission</span>
                                </label>
                            </li>
                            <li>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="checkbox" checked>
                                    <span>Notifications</span>
                                </label>                      
                            </li>
                        </ul>
                    </div>                    
                </div>                
            </div>       
            <div class="tab-pane right_chat" id="chat">
                <div class="slim_scroll">
                    <form>
                        <div class="input-group m-b-20">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <div class="card">
                        <h6>Recent</h6>                        
                        <ul class="right_chat list-unstyled mb-0">
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="<?=base_url('assets/theme/assets/images/xs/avatar4.jpg');?>" alt="">
                                        <div class="media-body">
                                            <span class="name">Ava Alexander <small class="float-right">Just now</small></span>
                                            <span class="message">Lorem ipsum Veniam aliquip culpa laboris minim tempor</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="<?=base_url('assets/theme/assets/images/xs/avatar5.jpg');?>" alt="">
                                        <div class="media-body">
                                            <span class="name">Debra Stewart <small class="float-right">38min ago</small></span>
                                            <span class="message">Many desktop publishing packages and web page editors</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="<?=base_url('assets/theme/assets/images/xs/avatar2.jpg');?>" alt="">
                                        <div class="media-body">
                                            <span class="name">Susie Willis <small class="float-right">2hr ago</small></span>
                                            <span class="message">Contrary to belief, Lorem Ipsum is not simply random text</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="<?=base_url('assets/theme/assets/images/xs/avatar1.jpg');?>" alt="">
                                        <div class="media-body">
                                            <span class="name">Folisise Chosielie <small class="float-right">2hr ago</small></span>
                                            <span class="message">There are many of passages of available, but the majority</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="<?=base_url('assets/theme/assets/images/xs/avatar3.jpg');?>" alt="">
                                        <div class="media-body">
                                            <span class="name">Marshall Nichols <small class="float-right">1day ago</small></span>
                                            <span class="message">It is a long fact that a reader will be distracted</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>                        
                        </ul>
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="<?=base_url();?>">
                <img src="<?=base_url('assets/img/logo-transparent.png');?>"  alt="Logo" class="img-fluid logo">
                <span>BPIBS</span>
            </a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm btn-default float-right"><i class="lnr lnr-menu fa fa-chevron-circle-left"></i></button>
        </div>
        <div class="sidebar-scroll">
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="sidebar-menu" class="metismenu">
                    <!-- <li class="<?=$title == 'Dashboard' ? 'active' : '';?>">
                        <a href="<?=base_url();?>"><i class="fa fa-home"></i><span>Dashboard</span></a>
                    </li>
                    <li class="<?=$title == 'User Login' ? 'active' : '';?>">
                        <a href="#user-login" class="has-arrow"><i class="fa fa-user"></i><span>User</span></a>
                        <ul>
                            <li><a href="<?=base_url('user-login/list');?>">User Login</a></li>
                        </ul>
                    </li>
                    <li class="<?=$title == 'Siswa' ? 'active' : '';?>">
                        <a href="#siswa" class="has-arrow"><i class="fa fa-users"></i><span>Siswa</span></a>
                        <ul>
                            <li><a href="<?=base_url('siswa/list');?>">Data Master</a></li>
                            <li><a href="<?=base_url('absensi');?>">Absensi</a></li>
                        </ul>
                    </li>
                    <li class="<?=$title == 'Guru' ? 'active' : '';?>">
                        <a href="#guru" class="has-arrow"><i class="fa fa-users"></i><span>Guru</span></a>
                        <ul>
                            <li><a href="<?=base_url('guru/list');?>">Data Master</a></li>
                        </ul>
                    </li>
                    <li class="<?=$title == 'Nilai' ? 'active' : '';?>">
                        <a href="<?=base_url('nilai');?>"><i class="fa fa-star"></i><span>Nilai</span></a>
                    </li>
                    <li class="<?=$title == 'Kelas' ? 'active' : '';?>">
                        <a href="<?=base_url('kelas/list');?>"><i class="fa fa-building"></i><span>Kelas</span></a>
                    </li>
                    <li class="<?=$title == 'Kamar' ? 'active' : '';?>">
                        <a href="<?=base_url('kamar/list');?>"><i class="fa fa-home"></i><span>Kamar</span></a>
                    </li>
                    <li class="<?= $title == 'Mata Pelajaran' ? 'active' : '';?>">
                        <a href="<?=base_url('mapel/list');?>"><i class="fa fa-book"></i><span>Mata Pelajaran</span></a>
                    </li>
                    <li class="<?=$title == 'Jadwal Mata Pelajaran' ? 'active' : '';?>">
                        <a href="#guru" class="has-arrow"><i class="fa fa-calendar"></i><span>Jadwal</span></a>
                        <ul>
                            <li><a href="<?=base_url('jadwal-mapel/list');?>">Data Master</a></li>
                        </ul>
                        <ul>
                            <li><a href="<?=base_url('jadwal-mapel/calendar');?>">Kalender</a></li>
                        </ul>
                    </li> -->
                </ul>
            </nav>     
        </div>
    </div>