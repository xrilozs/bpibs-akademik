</div>

<div class="modal fade" id="password-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ganti Kata Sandi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="password-form">
          
          <div class="form-group">
            <label>Kata sandi baru</label>
            <input type="password" name="password" class="form-control">
          </div>
          <div class="form-group">
            <label>Ketik Ulang</label>
            <input type="password" name="confirm" class="form-control">
          </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="submit" class="btn btn-warning" id="password-button">Ganti</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="profile-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Profil</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="profile-form">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="user-name" class="form-control" readonly>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="user-email" class="form-control" readonly>
          </div>
          <div class="form-group">
            <label>Role</label>
            <input type="text" name="user-role" class="form-control" readonly>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- Javascript -->
<script src="<?=base_url('assets/theme/light/assets/bundles/libscripts.bundle.js');?>"></script>    
<script src="<?=base_url('assets/theme/light/assets/bundles/vendorscripts.bundle.js');?>"></script>

<script src="<?=base_url('assets/theme/light/assets/bundles/datatablescripts.bundle.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-datatable/buttons/buttons.html5.min.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-datatable/buttons/buttons.print.min.js');?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.0/dist/sweetalert2.all.min.js"></script>
<script src="<?=base_url('assets/theme/assets/vendor/dropify/js/dropify.min.js');?>"></script>
<script src="<?=base_url('assets/theme/light/assets/js/pages/forms/dropify.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-steps/jquery.steps.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-validation/jquery.validate.js');?>"></script>

<script src="<?=base_url('assets/theme/light/assets/bundles/mainscripts.bundle.js');?>"></script>
<script src="<?=base_url('assets/theme/light/assets/js/pages/tables/jquery-datatable.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js');?>"></script>
<script src="<?=base_url('assets/theme/assets/vendor/echart/echarts.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/select2/js/select2.full.min.js');?>"></script>

<script>
    const WEB_URL     = "<?=base_url()?>"
    const API_URL     = "<?=base_url('api')?>"
    const PAGE_TITLE  = "<?=$title;?>"
</script>
<script src="<?=base_url('assets/js/common.js');?>"></script>
<?php
  if(isset($js_file)){
    foreach ($js_file as $file) {
        echo "<script src='".$file."'></script>";
    }
  }
?>

</body>
</html>
