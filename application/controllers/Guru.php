<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {

	public function guru_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/guru.js?v=').time()
			),
			"title"		=> "Guru"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/guru/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function guru_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/guru_create.js?v=').time()
			),
			"title"		=> "Guru"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/guru/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function guru_update(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/guru_update.js?v=').time()
			),
			"title"		=> "Guru"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/guru/update');
		$this->load->view('layout/main_footer', $data);
	}

	public function guru_detail(){

		$data = array(
			"js_file" => array(
				base_url('assets/js/guru_detail.js?v=').time()
			),
			"title"		=> "Guru"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/guru/detail');
		$this->load->view('layout/main_footer', $data);
	}
}
