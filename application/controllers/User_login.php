<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_login extends CI_Controller {

	public function user_login_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/user_login.js?v=').time()
			),
			"title"		=> "User Login"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_login/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function user_login_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/user_login.js?v=').time()
			),
			"title"		=> "User Login"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_login/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function user_login_update(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/user_login.js?v=').time()
			),
			"title"		=> "User Login"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_login/update');
		$this->load->view('layout/main_footer', $data);
	}
}
