<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public function kelas_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/kelas.js?v=').time()
			),
			"title"		=> "Kelas"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kelas/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function kelas_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/kelas.js?v=').time()
			),
			"title"		=> "Kelas"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kelas/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function kelas_update(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/kelas.js?v=').time()
			),
			"title"		=> "Kelas"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kelas/update');
		$this->load->view('layout/main_footer', $data);
	}
}
