<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {

	public function mapel_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/mapel.js?v=').time()
			),
			"title"		=> "Mata Pelajaran"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/mapel/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function mapel_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/mapel.js?v=').time()
			),
			"title"		=> "Mata Pelajaran"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/mapel/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function mapel_update(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/mapel.js?v=').time()
			),
			"title"		=> "Mata Pelajaran"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/mapel/update');
		$this->load->view('layout/main_footer', $data);
	}
}
