<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function siswa_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/siswa.js?v=').time()
			),
			"title"		=> "Siswa"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/siswa/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function siswa_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/siswa_create.js?v=').time()
			),
			"title"		=> "Siswa"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/siswa/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function siswa_update(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/siswa_update.js?v=').time()
			),
			"title"		=> "Siswa"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/siswa/update');
		$this->load->view('layout/main_footer', $data);
	}

	public function siswa_detail(){

		$data = array(
			"js_file" => array(
				base_url('assets/js/siswa_detail.js?v=').time()
			),
			"title"		=> "Siswa"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/siswa/detail');
		$this->load->view('layout/main_footer', $data);
	}

	public function siswa_report(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/siswa_report.js?v=').time()
			),
			"title"		=> "Siswa"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/siswa/report');
		$this->load->view('layout/main_footer', $data);
	}
}
