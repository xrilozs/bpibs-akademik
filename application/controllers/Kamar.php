<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends CI_Controller {

	public function kamar_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/kamar.js?v=').time()
			),
			"title"		=> "Kamar"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kamar/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function kamar_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/kamar.js?v=').time()
			),
			"title"		=> "Kamar"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kamar/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function kamar_update(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/kamar.js?v=').time()
			),
			"title"		=> "Kamar"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/kamar/update');
		$this->load->view('layout/main_footer', $data);
	}
}
