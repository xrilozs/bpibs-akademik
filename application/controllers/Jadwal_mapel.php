<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_mapel extends CI_Controller {

	public function jadwal_mapel_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/jadwal_mapel.js?v=').time()
			),
			"title"		=> "Jadwal"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/jadwal_mapel/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function jadwal_mapel_create(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/jadwal_mapel_create.js?v=').time()
			),
			"title"		=> "Jadwal"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/jadwal_mapel/create');
		$this->load->view('layout/main_footer', $data);
	}

	public function jadwal_mapel_update(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/jadwal_mapel_update.js?v=').time()
			),
			"title"		=> "Jadwal"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/jadwal_mapel/update');
		$this->load->view('layout/main_footer', $data);
	}

	public function jadwal_mapel_detail(){

		$data = array(
			"js_file" => array(
				base_url('assets/js/jadwal_mapel_detail.js?v=').time()
			),
			"title"		=> "Jadwal"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/jadwal_mapel/detail');
		$this->load->view('layout/main_footer', $data);
	}

	public function jadwal_mapel_calendar(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/calendar.js?v=').time()
			),
			"title"		=> "Kalender"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/jadwal_mapel/calendar');
		$this->load->view('layout/main_footer', $data);
	}
}
