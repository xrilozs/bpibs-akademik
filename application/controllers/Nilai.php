<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/nilai.js?v=').time()
			),
			"title"		=> "Nilai"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/nilai/page');
		$this->load->view('layout/main_footer', $data);
	}
}
