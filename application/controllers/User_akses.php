<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_akses extends CI_Controller {

	public function user_akses_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/user_akses.js?v=').time()
			),
			"title"		=> "User Akses"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_akses/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function user_akses_update(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/user_akses.js?v=').time()
			),
			"title"		=> "User Akses"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_akses/update');
		$this->load->view('layout/main_footer', $data);
	}
}
