<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Kamar extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/kamar [GET]
    function get_kamar(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $id_ajaran    = $this->input->get('id_ajaran');
        $tipe         = $this->input->get('tipe');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamar [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/kamar [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get kamar
        $start = $page_number * $page_size;
        $order = array('field'=>'kamar', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $kamar = $this->kamar_model->get_kamar($search, $id_ajaran, $tipe, $order, $limit);
        $total = $this->kamar_model->count_kamar($search, $id_ajaran, $tipe);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/kamar [GET] - Get kamar is success');
          $resp->set_response(200, "success", "Get kamar is success", $kamar);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/kamar [GET] - Get kamar is success');
          $resp->set_response_datatable(200, $kamar, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/kamar/all [GET]
    function get_kamar_all(){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        $id_ajaran    = $this->input->get('id_ajaran');
        if(!$id_ajaran){
            $ajaran     = $this->ajaran_model->get_ajaran_all(1);
            $id_ajaran  = sizeof($ajaran) > 0 ? $ajaran[0]->id_ajaran : null;
        }
        
        #get kelas
        $kamar = $this->kamar_model->get_kamar(null, $id_ajaran);
        
        #response
        logging('debug', '/api/kelas [GET] - Get kamar is success');
        $resp->set_response(200, "success", "Get kamar is success", $kamar);
        set_output($resp->get_response());
        return;
    }

    #path: /api/kamar/by-id/$id [GET]
    function get_kamar_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamar/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get kamar by id
        $kamar = $this->kamar_model->get_kamar_by_id($id);
        if(is_null($kamar)){
            logging('error', '/api/kamar/by-id/'.$id.' [GET] - kamar not found');
            $resp->set_response(404, "failed", "kamar not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/kamar/by-id/'.$id.' [GET] - Get kamar by id success', $kamar);
        $resp->set_response(200, "success", "Get kamar by id success", $kamar);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/kamar [POST]
    function create_kamar(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamar [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_ajaran', 'kamar', 'tipe');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kamar [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create kamar
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                   = $this->kamar_model->create_kamar($request);
        
        #response
        if(!$flag){
            logging('error', '/api/kamar [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/kamar [POST] - Create kamar success', $request);
        $resp->set_response(200, "success", "Create kamar success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/kamar [PUT]
    function update_kamar(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamar [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_kamar', 'id_ajaran', 'kamar', 'tipe');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kamar [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check kamar
        $kamar = $this->kamar_model->get_kamar_by_id($request['id_kamar']);
        if(is_null($kamar)){
            logging('error', '/api/kamar [PUT] - kamar not found', $request);
            $resp->set_response(404, "failed", "kamar not found");
            set_output($resp->get_response());
            return;
        }

        #update kamar
        $flag = $this->kamar_model->update_kamar($request);
        if(empty($flag)){
            logging('error', '/api/kamar [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/kamar [PUT] - Update kamar success', $request);
        $resp->set_response(200, "success", "Update kamar success", $request);
        set_output($resp->get_response());
        return;
    }
}