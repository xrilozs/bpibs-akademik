<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Mapel extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/mapel/all [GET]
    function get_mapel_all(){
        $resp = new Response_api();

        #init variable
        $id_ajaran    = $this->input->get('id_ajaran');
        $params       = array($id_ajaran);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/mapel/all [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get mapel
        $mapel = $this->mapel_model->get_mapel(null, null, $id_ajaran);
        
        #response
        logging('debug', '/api/mapel [GET] - Get mapel all is success');
        $resp->set_response(200, "success", "Get mapel all is success", $mapel);
        set_output($resp->get_response());
        return;
    }

    #path: /api/mapel [GET]
    function get_mapel(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $id_ajaran    = $this->input->get('id_ajaran');
        $id_kat       = $this->input->get('id_kat');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/mapel [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get mapel
        $start = $page_number * $page_size;
        $order = array('field'=>'urutan', 'order'=>'ASC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $mapel = $this->mapel_model->get_mapel($search, $id_kat, $id_ajaran, $order, $limit);
        $total = $this->mapel_model->count_mapel($search, $id_kat, $id_ajaran);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/mapel [GET] - Get mapel is success');
          $resp->set_response(200, "success", "Get mapel is success", $mapel);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/mapel [GET] - Get mapel is success');
          $resp->set_response_datatable(200, $mapel, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/mapel/by-id/$id [GET]
    function get_mapel_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get mapel by id
        $mapel = $this->mapel_model->get_mapel_by_id($id);
        if(is_null($mapel)){
            logging('error', '/api/mapel/by-id/'.$id.' [GET] - mapel not found');
            $resp->set_response(404, "failed", "mapel not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/mapel/by-id/'.$id.' [GET] - Get mapel by id success', $mapel);
        $resp->set_response(200, "success", "Get mapel by id success", $mapel);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/mapel [POST]
    function create_mapel(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_kat', 'mapel', 'mapel_det', 'id_ajaran', 'urutan');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/mapel [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kategori_mapel = $this->kategori_mapel_model->get_kategori_mapel_by_id($request['id_kat']);
        if(is_null($kategori_mapel)){
            logging('error', '/api/mapel [POST] - Kategori mapel is not found');
            $resp->set_response(400, "failed", "Kategori mapel is not found");
            set_output($resp->get_response());
            return;
        }

        $ajaran = $this->ajaran_model->get_ajaran_by_id($request['id_ajaran']);
        if(is_null($ajaran)){
            logging('error', '/api/mapel [POST] - tahun ajaran is not found');
            $resp->set_response(400, "failed", "tahun ajaran is not found");
            set_output($resp->get_response());
            return;
        }

        #create mapel
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                   = $this->mapel_model->create_mapel($request);
        
        #response
        if(!$flag){
            logging('error', '/api/mapel [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/mapel [POST] - Create mapel success', $request);
        $resp->set_response(200, "success", "Create mapel success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/mapel [PUT]
    function update_mapel(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_mapel', 'id_kat', 'mapel', 'urutan');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/mapel [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kategori_mapel = $this->kategori_mapel_model->get_kategori_mapel_by_id($request['id_kat']);
        if(is_null($kategori_mapel)){
            logging('error', '/api/mapel [POST] - Kategori mapel is not found');
            $resp->set_response(400, "failed", "Kategori mapel is not found");
            set_output($resp->get_response());
            return;
        }

        #check mapel
        $mapel = $this->mapel_model->get_mapel_by_id($request['id_mapel']);
        if(is_null($mapel)){
            logging('error', '/api/mapel [PUT] - mapel not found', $request);
            $resp->set_response(404, "failed", "mapel not found");
            set_output($resp->get_response());
            return;
        }

        #update mapel
        $request['updatedt']    = date("Y-m-d H:i:s");
        $request['updateby']    = $user->id;
        $flag = $this->mapel_model->update_mapel($request);
        if(empty($flag)){
            logging('error', '/api/mapel [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/mapel [PUT] - Update mapel success', $request);
        $resp->set_response(200, "success", "Update mapel success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/mapel/delete/$id [DELETE]
    function delete_mapel($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/mapel/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get mapel by id
        $mapel = $this->mapel_model->get_mapel_by_id($id);
        if(is_null($mapel)){
            logging('error', '/api/mapel/delete/'.$id.' [DELETE] - mapel not found');
            $resp->set_response(404, "failed", "mapel not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->mapel_model->delete_mapel($id);
        if(!$flag){
            logging('error', '/api/mapel/delete/'.$id.' [DELETE] - Delete mapel failed');
            $resp->set_response(404, "failed", "Delete mapel failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/mapel/delete/'.$id.' [GET] - Delete mapel success');
        $resp->set_response(200, "success", "Delete mapel success");
        set_output($resp->get_response());
        return;
    }
}