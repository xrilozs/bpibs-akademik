<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Siswa_mutasi extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/siswa-mutasi/list/$id_siswa [GET]
    function get_siswa_mutasi($id_siswa){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-mutasi/by-siswa/$id_siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get siswa mutasi
        $siswa_mutasi  = $this->siswa_mutasi_model->get_siswa_mutasi_by_siswa($id_siswa);
        logging('debug', '/api/siswa-mutasi/list/$id_siswa [GET] - Get siswa mutasi by siswa is success');
        $resp->set_response(200, "success", "Get siswa mutasi by siswa is success", $siswa_mutasi);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/siswa-mutasi/leave [POST]
    function siswa_mutasi_leave(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-mutasi/leave [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_siswa', 'tgl', 'keterangan');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa-mutasi/leave [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($request['id_siswa']);
        if(is_null($siswa)){
            logging('error', '/api/siswa-mutasi/leave [POST] - Siswa not found', $request);
            $resp->set_response(400, "failed", "Siswa not found");
            set_output($resp->get_response());
            return;
        }

        #create siswa mutasi
        $request['status']    = "keluar";  
        $request['createby']  = $user->id;
        $request['createdt']  = date("Y-m-d H:i:s");
        $flag1 = $this->siswa_mutasi_model->create_siswa_mutasi($request);
        $flag2 = $this->siswa_model->inactive($request['id_siswa']);
        
        #response
        if(!$flag1 || !$flag2){
            logging('error', '/api/siswa-mutasi/leave [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/siswa-mutasi/leave [POST] - Create siswa mutasi success', $request);
        $resp->set_response(200, "success", "Create siswa mutasi success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa-mutasi/return [POST]
    function siswa_mutasi_return(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-mutasi/return [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_siswa', 'tgl', 'keterangan');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa-mutasi/return [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($request['id_siswa']);
        if(is_null($siswa)){
            logging('error', '/api/siswa-mutasi/return [POST] - Siswa not found', $request);
            $resp->set_response(400, "failed", "Siswa not found");
            set_output($resp->get_response());
            return;
        }

        #create siswa mutasi
        $request['status']    = "masuk";  
        $request['createby']  = $user->id;
        $request['createdt']  = date("Y-m-d H:i:s");
        $flag1 = $this->siswa_mutasi_model->create_siswa_mutasi($request);
        $flag2 = $this->siswa_model->active($request['id_siswa']);
        
        #response
        if(!$flag1 || !$flag2){
            logging('error', '/api/siswa-mutasi/return [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/siswa-mutasi/leave [POST] - Create siswa mutasi success', $request);
        $resp->set_response(200, "success", "Create siswa mutasi success", $request);
        set_output($resp->get_response());
        return;
    }
}
