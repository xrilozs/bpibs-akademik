<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class User extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/user/login [POST]
    function login(){
        #init req & resp
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('email', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user/login [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #init variable
        $email      = $request['email'];
        $password   = $request['password'];

        #get user
        $user = $this->user_model->get_user_by_email($email);
        if(is_null($user)){
            $user = $this->user_model->get_user_by_username($email);
            if(is_null($user)){
                logging('error', '/api/user/login [POST] - user not found', $request);
                $resp->set_response(404, "failed", "user not found");
                set_output($resp->get_response());
                return;
            }
            $email = $user->email;
        }

        #check user
        $verify = password_verify($password, $user->password);
        if(!$verify){
            logging('error', '/api/user/login [POST] - Invalid password', $request);
            $resp->set_response(400, "failed", "Invalid password");
            set_output($resp->get_response());
            return;
        }
        if(!$user->is_active){
            logging('error', '/api/user/login [POST] - Account is Inactive', $request);
            $resp->set_response(400, "failed", "Account is Inactive");
            set_output($resp->get_response());
            return;
        }

        #Create user token      
        $token_expired = time() + (4 * 60 * 60);
        $payload = array(
          'email'   => $email,
          'exp'     => $token_expired
        );
        #access token
        $access_token   = JWT::encode($payload, ACCESS_TOKEN_SALT);
        #refresh token
        $payload['exp'] = time() + (8* 60 * 60);
        $refresh_token  = JWT::encode($payload, REFRESH_TOKEN_SALT);
      
        $response = array(
          'access_token'    => $access_token,
          'refresh_token'   => $refresh_token,
          'user'            => $user,
          'menu'            => guard_role($user)
        );
        
        logging('debug', '/api/user/login [POST] - user login success', $response);
        $resp->set_response(200, "success", "user login success", $response);
        set_output($resp->get_response());
        return;
    }

    #path: user/refresh [GET]
    function refresh(){
      $resp = new Response_api();

      #check header
      $header = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/api/user/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SALT, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/api/user/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $email = $jwt->email;
      $user  = $this->user_model->get_user_by_email($email);

      #check user exist
      if(is_null($user)){
        logging('debug', '/api/user/refresh [GET] - user not found');
        $resp->set_response(401, "failed", "user not found");
        set_output($resp->get_response());
        return;
      }
      if(!$user->is_active){
        logging('error', '/api/user/login [POST] - User is Inactive', $request);
        $resp->set_response(400, "failed", "User is Inactive");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token
      $token_expired = time() + (4 * 60 * 60);
      $payload       = array(
        'email' => $email,
        'exp'   => $token_expired
      );
      #access token
      $access_token     = JWT::encode($payload, ACCESS_TOKEN_SALT);
      #refresh token
      $payload['exp']   = time() + (8 * 60 * 60);
      $refresh_token    = JWT::encode($payload, REFRESH_TOKEN_SALT);

      $response = array(
        'access_token'  => $access_token,
        'refresh_token' => $refresh_token,
        'user'          => $user,
        'menu'          => guard_role($user)
      );
      
      logging('debug', '/api/user/refresh [GET] - Refresh user success', $response);
      $resp->set_response(200, "success", "Refresh user success", $response);
      set_output($resp->get_response());
      return;
    }

    #path: /api/user/change-password [PUT]
    function change_password(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #check request params
        $keys = array('password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user/change-password [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #update user
        $hash                   = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password']    = $hash;
        $request['id']          = $user->id;
        $flag                   = $this->user_model->change_password($user->id, $hash);
        
        #response
        if(empty($flag)){
            logging('error', '/api/user/change-password [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user/change-password [PUT] - Change password user success', $request);
        $resp->set_response(200, "success", "Change password user success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/user [GET]
    function get_user(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $role         = $this->input->get('role');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);
        $allowed_role = array(1);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/user [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>'name', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $users = $this->user_model->get_users($search, $role, $order, $limit);
        $total = $this->user_model->count_user($search, $role);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/user [GET] - Get user is success', $user);
          $resp->set_response(200, "success", "Get user is success", $user);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/user [GET] - Get user is success');
          $resp->set_response_datatable(200, $users, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/user/by-id/$id [GET]
    function get_user_by_id($id){
        $resp = new Response_api();

        #check token
        $allowed_role   = array(1);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get user by id
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/by-id/'.$id.' [GET] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }
        $user->full_image = base_url($user->image);
        unset($user->password);

        #response
        logging('debug', '/api/user/by-id/'.$id.' [GET] - Get user by id success', $user);
        $resp->set_response(200, "success", "Get user by id success", $user);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/profile [GET]
    function get_profile(){
        $resp = new Response_api();
  
        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user               = $verify_resp['data']['user'];
        $user->full_image   = base_url($user->image);
        unset($user->password);
  
        #response
        logging('debug', '/api/user/profile [GET] - Get profile success', $user);
        $resp->set_response(200, "success", "Get profile success", $user);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/user [POST]
    function create_user(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $allowed_role = array(1);
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('name', 'email', 'image', 'username', 'role_id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $user = $this->user_model->get_user_by_email($request['email']);
        if($user){
            logging('error', '/api/user [POST] - Email already registered', $request);
            $resp->set_response(400, "failed", "Email already registered");
            set_output($resp->get_response());
            return;
        }

        #create user
        $request['is_active']    = 1;
        $request['password']     = password_hash("user123", PASSWORD_DEFAULT);
        $request['date_created'] = date("Y-m-d H:i:s");
        $flag                    = $this->user_model->create_user($request);
        
        #response
        if(!$flag){
            logging('error', '/api/user [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/user [POST] - Create user success', $request);
        $resp->set_response(200, "success", "Create user success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user [PUT]
    function update_user(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $allowed_role   = array(1);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'email', 'image', 'username', 'role_id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($request['id']);
        if(is_null($user)){
            logging('error', '/api/user [PUT] - user not found', $request);
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #check changes of email
        if($user->email != $request['email']){
            #check duplicate email
            $user_exist = $this->user_model->get_user_by_email($request['email']);
            if($user_exist){
                logging('error', '/api/user [POST] - New email already registered', $request);
                $resp->set_response(400, "failed", "New email already registered");
                set_output($resp->get_response());
                return;
            }
        }

        #update user
        $flag = $this->user_model->update_user($request);
        if(empty($flag)){
            logging('error', '/api/user [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/user [PUT] - Update user success', $request);
        $resp->set_response(200, "success", "Update user success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/inactive/$id [PUT]
    function inactive($id){
        $resp = new Response_api();

        #check token
        $allowed_role   = array(1);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #inactive user
        $flag = $this->user_model->inactive($id);
        
        #response
        if(!$flag){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/user/inactive/'.$id.' [PUT] - inactive user success');
        $resp->set_response(200, "success", "inactive user success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/active/$id [PUT]
    function active($id){
        $resp = new Response_api();

        #check token
        $allowed_role   = array(1);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/active/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/active/'.$id.' [PUT] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #active user
        $flag = $this->user_model->active($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/user/active/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/user/active/'.$id.' [PUT] - active user success');
        $resp->set_response(200, "success", "active user success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/upload [POST]
    function upload_file(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/user/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/user/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/user/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/user/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_file_url'] = base_url($data['file_url']);
        logging('debug', '/api/user/upload [POST] - Upload file success', $data);
        $resp_obj->set_response(200, "success", "Upload file success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}