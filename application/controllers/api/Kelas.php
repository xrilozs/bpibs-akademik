<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Kelas extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/kelas [GET]
    function get_kelas(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $id_ajaran    = $this->input->get('id_ajaran');
        $tipe         = $this->input->get('tipe');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/kelas [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get kelas
        $start = $page_number * $page_size;
        $order = array('field'=>'kelas', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $kelas = $this->kelas_model->get_kelas($search, $id_ajaran, $tipe, $order, $limit);
        $total = $this->kelas_model->count_kelas($search, $id_ajaran, $tipe);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/kelas [GET] - Get kelas is success');
          $resp->set_response(200, "success", "Get kelas is success", $kelas);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/kelas [GET] - Get kelas is success');
          $resp->set_response_datatable(200, $kelas, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/kelas/all [GET]
    function get_kelas_all(){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        $id_ajaran    = $this->input->get('id_ajaran');
        if(!$id_ajaran){
            $ajaran     = $this->ajaran_model->get_ajaran_all(1);
            $id_ajaran  = sizeof($ajaran) > 0 ? $ajaran[0]->id_ajaran : null;
        }
        
        #get kelas
        $kelas = $this->kelas_model->get_kelas(null, $id_ajaran);
        
        #response
        logging('debug', '/api/kelas [GET] - Get kelas is success');
        $resp->set_response(200, "success", "Get kelas is success", $kelas);
        set_output($resp->get_response());
        return;
    }

    #path: /api/kelas/by-id/$id [GET]
    function get_kelas_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get kelas by id
        $kelas = $this->kelas_model->get_kelas_by_id($id);
        if(is_null($kelas)){
            logging('error', '/api/kelas/by-id/'.$id.' [GET] - kelas not found');
            $resp->set_response(404, "failed", "kelas not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/kelas/by-id/'.$id.' [GET] - Get kelas by id success', $kelas);
        $resp->set_response(200, "success", "Get kelas by id success", $kelas);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/kelas [POST]
    function create_kelas(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_ajaran', 'kdkelas', 'kelas', 'tipe');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kelas [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create kelas
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                    = $this->kelas_model->create_kelas($request);
        
        #response
        if(!$flag){
            logging('error', '/api/kelas [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/kelas [POST] - Create kelas success', $request);
        $resp->set_response(200, "success", "Create kelas success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/kelas [PUT]
    function update_kelas(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelas [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_kelas', 'id_ajaran', 'kdkelas', 'kelas', 'tipe');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kelas [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check kelas
        $kelas = $this->kelas_model->get_kelas_by_id($request['id_kelas']);
        if(is_null($kelas)){
            logging('error', '/api/kelas [PUT] - kelas not found', $request);
            $resp->set_response(404, "failed", "kelas not found");
            set_output($resp->get_response());
            return;
        }

        #update kelas
        $flag = $this->kelas_model->update_kelas($request);
        if(empty($flag)){
            logging('error', '/api/kelas [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/kelas [PUT] - Update kelas success', $request);
        $resp->set_response(200, "success", "Update kelas success", $request);
        set_output($resp->get_response());
        return;
    }
}