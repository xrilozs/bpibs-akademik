<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Guru_edu_lvl extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }

  #path: /api/guru-edu-lvl [GET]
  function get_guru_edu_lvl_all(){
    $resp = new Response_api();

    #check token
    $header      = $this->input->request_headers();
    $verify_resp = verify_user_token($header);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/api/guru-edu-lvl [GET] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #get guru_edu_lvl
    $guru_edu_lvl  = $this->guru_edu_lvl_model->get_guru_edu_lvl_all();
    
    #response
    logging('debug', '/api/guru-edu-lvl [GET] - Get guru edu lvl is success', $guru_edu_lvl);
    $resp->set_response(200, "success", "Get guru edu lvl is success", $guru_edu_lvl);
    set_output($resp->get_response());
    return;
  }
}