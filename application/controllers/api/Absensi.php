<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Absensi extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/absensi/search [POST]
    function get_jadwal_mapel(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        $keys = array('id_ajaran', 'id_semester', 'id_kelas', 'id_mapel', 'tanggal');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/absensi/search [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }
        
        // $day_ID         = convert_day_ID($request['tanggal']);
        // $hari           = $this->hari_model->get_hari_by_hari($day_ID);
        // if(is_null($hari)){
        //     logging('error', "/api/absensi/search [POST] - Hari $day_ID not found");
        //     $resp->set_response(400, "failed", "Hari $day_ID not found");
        //     set_output($resp->get_response());
        //     return;
        // }

        #get jadwal mapel
        $jadwal_mapel_list = $this->jadwal_mapel_model->get_jadwal_mapel(null, $request['id_ajaran'], $request['id_semester'], $request['id_kelas'], $request['id_mapel']);

        if(sizeof($jadwal_mapel_list) <= 0){
            logging('error', '/api/absensi/search [POST] - jadwal mapel is not found', $request);
            $resp->set_response(400, "failed", "jadwal mapel is not found");
            set_output($resp->get_response());
            return;
        }

        $jadwal_mapel   = $jadwal_mapel_list[0];
        $siswa          = $this->siswa_model->get_siswa(null, $request['id_kelas']);
        foreach ($siswa as &$item) {
            $item->id_jadwal    = $jadwal_mapel->id_jadwal;
            $item->tgl          = $request['tanggal'];

            $absensi_list = $this->absensi_model->get_absensi(null, $jadwal_mapel->id_jadwal, $item->id_siswa, $request['tanggal']);
            if(sizeof($absensi_list) > 0){
                $absensi = $absensi_list[0];
                $item->id_absensi       = $absensi->id;
                $item->hadir            = $absensi->hadir;
                $item->deskripsi        = $absensi->deskripsi;
                $item->keterangan_absen = $absensi->keterangan_absen;
            }
        }

        logging('error', '/api/absensi/search [POST] - check absensi success');
        $resp->set_response(200, "success", "check absensi success", $siswa);
        set_output($resp->get_response());
        return;
    }

    #path: /api/absensi/siswa [POST]
    function save_absensi_siswa(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/absensi/siswa [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        $keys = array('list_absensi');
        $check_param = check_parameter_by_keysV2($request, $keys);
        if(!$check_param['success']){
            logging('error', "/api/absensi/siswa [POST] - ".$check_param['message'], $request);
            $resp->set_response(400, "failed", $check_param['message']);
            set_output($resp->get_response());
            return;
        }

        $list_absensi = $request['list_absensi'];
        foreach ($list_absensi as $absensi) {
            $keys = array('id_jadwal', 'id_siswa', 'tgl', 'hadir', 'deskripsi', 'keterangan_absen');
            $check_param = check_parameter_by_keysV2($absensi, $keys);
            if(!$check_param['success']){
                logging('error', "/api/absensi/siswa [POST] - ".$check_param['message'], $absensi);
                $resp->set_response(400, "failed", $check_param['message']);
                set_output($resp->get_response());
                return;
            }

            $item = array(
                'id_jadwal'         => $absensi['id_jadwal'], 
                'id_siswa'          => $absensi['id_siswa'], 
                'tgl'               => $absensi['tgl'], 
                'hadir'             => $absensi['hadir'], 
                'deskripsi'         => $absensi['deskripsi'],
                'keterangan_absen'  => $absensi['keterangan_absen'],
            );

            if(!isset($absensi['id_absensi'])){
                $item['createdt']    = date("Y-m-d H:i:s");
                $item['createby']    = $user->id;
                $flag = $this->absensi_model->create_absensi($item);
            }else{
                $item['id']          = $absensi['id_absensi'];
                $item['updatedt']    = date("Y-m-d H:i:s");
                $item['updateby']    = $user->id;
                $flag = $this->absensi_model->update_absensi($item);
            }
        }

        logging('error', '/api/absensi/siswa [POST] - save absensi success');
        $resp->set_response(200, "success", "save absensi success");
        set_output($resp->get_response());
        return;
    }
}