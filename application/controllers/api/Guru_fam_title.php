<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Guru_fam_title extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }

  #path: /api/guru-fam-title [GET]
  function get_guru_fam_title_all(){
    $resp = new Response_api();

    #check token
    $header      = $this->input->request_headers();
    $verify_resp = verify_user_token($header);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/api/guru-fam-title [GET] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #get guru_fam_title
    $guru_fam_title  = $this->guru_fam_title_model->get_guru_fam_title_all();
    
    #response
    logging('debug', '/api/guru-edu-lvl [GET] - Get guru fam title is success', $guru_fam_title);
    $resp->set_response(200, "success", "Get guru fam title is success", $guru_fam_title);
    set_output($resp->get_response());
    return;
  }
}