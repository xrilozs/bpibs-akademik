<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Jadwal_mapel extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/jadwal-mapel/calendar [GET]
    function get_jadwal_mapel_calendar(){
        $resp = new Response_api();

        #init variable
        $id_ajaran    = $this->input->get('id_ajaran');
        $id_semester  = $this->input->get('id_semester');
        $id_kelas     = $this->input->get('id_kelas');
        $params       = array($id_ajaran, $id_semester, $id_kelas);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel/calendar [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/jadwal-mapel/calendar [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get jadwal mapel
        $jadwal_mapel   = $this->jadwal_mapel_model->get_jadwal_mapel(null, $id_ajaran, $id_semester, $id_kelas);
        $calendar = array(
            "senin" => [],
            "selasa" => [],
            "rabu" => [],
            "kamis" => [],
            "jumat" => [],
            "sabtu" => [],
        );

        foreach ($jadwal_mapel as $item) {
            switch ($item->id_hari) {
                case 1:
                    array_push($calendar['senin'], $item);
                    break;
                case 2:
                    array_push($calendar['selasa'], $item);
                    break;
                case 3:
                    array_push($calendar['rabu'], $item);
                    break;
                case 4:
                    array_push($calendar['kamis'], $item);
                    break;
                case 5:
                    array_push($calendar['jumat'], $item);
                    break;
                case 6:
                    array_push($calendar['sabtu'], $item);
                    break;
                default:
                    break;
            }
        }
        
        #response
        logging('debug', '/api/jadwal-mapel/calendar [GET] - Get jadwal mapel calendar is success');
        $resp->set_response(200, "success", "Get jadwal mapel calendar is success", $calendar);
        set_output($resp->get_response());
        return;
    }

    #path: /api/jadwal-mapel [GET]
    function get_jadwal_mapel(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $id_ajaran    = $this->input->get('id_ajaran');
        $id_kelas     = $this->input->get('id_kelas');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/jadwal-mapel [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get jadwal mapel
        $start          = $page_number * $page_size;
        $order          = array('field'=>'id_jadwal', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $jadwal_mapel   = $this->jadwal_mapel_model->get_jadwal_mapel($search, $id_ajaran, null, $id_kelas, null, null, $order, $limit);
        $total          = $this->jadwal_mapel_model->count_jadwal_mapel($search, $id_ajaran, null, $id_kelas);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/jadwal-mapel [GET] - Get jadwal mapel is success');
          $resp->set_response(200, "success", "Get jadwal mapel is success", $jadwal_mapel);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/jadwal-mapel [GET] - Get jadwal mapel is success');
          $resp->set_response_datatable(200, $jadwal_mapel, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/jadwal-mapel/by-id/$id [GET]
    function get_jadwal_mapel_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jadwal mapel by id
        $jadwal_mapel = $this->jadwal_mapel_model->get_jadwal_mapel_by_id($id);
        if(is_null($jadwal_mapel)){
            logging('error', '/api/jadwal-mapel/by-id/'.$id.' [GET] - jadwal mapel not found');
            $resp->set_response(404, "failed", "jadwal mapel not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/jadwal-mapel/by-id/'.$id.' [GET] - Get jadwal mapel by id success', $jadwal_mapel);
        $resp->set_response(200, "success", "Get jadwal mapel by id success", $jadwal_mapel);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/jadwal-mapel [POST]
    function create_jadwal_mapel(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_ajaran', 'id_semester', 'id_kelas', 'id_mapel', 'id_hari', 'awal', 'akhir');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/jadwal-mapel [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $ajaran = $this->ajaran_model->get_ajaran_by_id($request['id_ajaran']);
        if(is_null($ajaran)){
            logging('error', '/api/jadwal-mapel [POST] - tahun ajaran is not found');
            $resp->set_response(400, "failed", "tahun ajaran is not found");
            set_output($resp->get_response());
            return;
        }

        $semester = $this->semester_model->get_semester_by_id($request['id_semester']);
        if(is_null($semester)){
            logging('error', '/api/jadwal-mapel [POST] - semester is not found');
            $resp->set_response(400, "failed", "semester is not found");
            set_output($resp->get_response());
            return;
        }

        $kelas = $this->kelas_model->get_kelas_by_id($request['id_kelas']);
        if(is_null($kelas)){
            logging('error', '/api/jadwal-mapel [POST] - Kelas is not found');
            $resp->set_response(400, "failed", "Kelas is not found");
            set_output($resp->get_response());
            return;
        }

        $hari = $this->hari_model->get_hari_by_id($request['id_hari']);
        if(is_null($hari)){
            logging('error', '/api/jadwal-mapel [POST] - hari is not found');
            $resp->set_response(400, "failed", "hari is not found");
            set_output($resp->get_response());
            return;
        }

        #create jadwal mapel
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                   = $this->jadwal_mapel_model->create_jadwal_mapel($request);
        
        #response
        if(!$flag){
            logging('error', '/api/jadwal-mapel [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/jadwal-mapel [POST] - Create jadwal mapel success', $request);
        $resp->set_response(200, "success", "Create jadwal mapel success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/jadwal-mapel [PUT]
    function update_jadwal_mapel(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_jadwal', 'id_ajaran', 'id_semester', 'id_kelas', 'id_mapel', 'id_hari', 'awal', 'akhir');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/jadwal-mapel [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $ajaran = $this->ajaran_model->get_ajaran_by_id($request['id_ajaran']);
        if(is_null($ajaran)){
            logging('error', '/api/jadwal-mapel [PUT] - tahun ajaran is not found');
            $resp->set_response(400, "failed", "tahun ajaran is not found");
            set_output($resp->get_response());
            return;
        }

        $semester = $this->semester_model->get_semester_by_id($request['id_semester']);
        if(is_null($semester)){
            logging('error', '/api/jadwal-mapel [PUT] - semester is not found');
            $resp->set_response(400, "failed", "semester is not found");
            set_output($resp->get_response());
            return;
        }

        $kelas = $this->kelas_model->get_kelas_by_id($request['id_kelas']);
        if(is_null($kelas)){
            logging('error', '/api/jadwal-mapel [PUT] - Kelas is not found');
            $resp->set_response(400, "failed", "Kelas is not found");
            set_output($resp->get_response());
            return;
        }

        $hari = $this->hari_model->get_hari_by_id($request['id_hari']);
        if(is_null($hari)){
            logging('error', '/api/jadwal-mapel [PUT] - hari is not found');
            $resp->set_response(400, "failed", "hari is not found");
            set_output($resp->get_response());
            return;
        }

        #update jadwal mapel
        $request['updatedt']    = date("Y-m-d H:i:s");
        $request['updateby']    = $user->id;
        $flag = $this->jadwal_mapel_model->update_jadwal_mapel($request);
        if(empty($flag)){
            logging('error', '/api/jadwal-mapel [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/jadwal-mapel [PUT] - Update jadwal mapel success', $request);
        $resp->set_response(200, "success", "Update jadwal mapel success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/jadwal-mapel/delete/$id [DELETE]
    function delete_jadwal_mapel($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/jadwal-mapel/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jadwal mapel by id
        $jadwal_mapel = $this->jadwal_mapel_model->get_jadwal_mapel_by_id($id);
        if(is_null($jadwal_mapel)){
            logging('error', '/api/jadwal-mapel/delete/'.$id.' [DELETE] - jadwal mapel not found');
            $resp->set_response(404, "failed", "mapel not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->jadwal_mapel_model->delete_mapel($id);
        if(!$flag){
            logging('error', '/api/jadwal-mapel/delete/'.$id.' [DELETE] - Delete jadwal mapel failed');
            $resp->set_response(404, "failed", "Delete jadwal mapel failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/jadwal-mapel/delete/'.$id.' [GET] - Delete jadwal mapel success');
        $resp->set_response(200, "success", "Delete jadwal mapel success");
        set_output($resp->get_response());
        return;
    }
}