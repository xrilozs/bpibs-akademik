<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Guru_edu extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/guru-edu/by-guru/$idguru [GET]
    function get_guru_edu_by_guru($idguru){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-edu [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get guru edu
        $guru_edu  = $this->guru_edu_model->get_guru_edu_by_idguru($idguru);
        logging('debug', '/api/guru-edu [GET] - Get guru edu is success');
        $resp->set_response(200, "success", "Get guru edu is success", $guru_edu);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-edu/by-id/$id [GET]
    function get_guru_edu_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-edu/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru edu by id
        $guru_edu = $this->guru_edu_model->get_guru_edu_by_id($id);
        if(is_null($guru_edu)){
            logging('error', '/api/guru-edu/by-id/'.$id.' [GET] - guru edu not found');
            $resp->set_response(404, "failed", "guru edu not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/guru-edu/by-id/'.$id.' [GET] - Get guru edu by id success');
        $resp->set_response(200, "success", "Get guru edu by id success", $guru_edu);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/guru-edu [POST]
    function create_guru_edu(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-edu [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('idguru', 'idlvl', 'sekolah', 'jurusan', 'thnlulus');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/guru-edu [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $guru = $this->guru_model->get_guru_by_id($request['idguru']);
        if(is_null($guru)){
            logging('error', '/api/guru-edu [POST] - Guru not found', $request);
            $resp->set_response(400, "failed", "Guru not found");
            set_output($resp->get_response());
            return;
        }

        $guru_edu_lvl = $this->guru_edu_lvl_model->get_guru_edu_lvl_by_id($request['idlvl']);
        if(is_null($guru_edu_lvl)){
            logging('error', '/api/guru-edu [POST] - Guru edu lvl not found', $request);
            $resp->set_response(400, "failed", "Guru edu lvl not found");
            set_output($resp->get_response());
            return;
        }

        #create guru_edu
        $request['is_deleted'] = 0;
        $flag                  = $this->guru_edu_model->create_guru_edu($request);
        
        #response
        if(!$flag){
            logging('error', '/api/guru-edu [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru-edu [POST] - Create guru edu success', $request);
        $resp->set_response(200, "success", "Create guru edu success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-edu [PUT]
    function update_guru_edu(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-edu [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id', 'idguru', 'idlvl', 'sekolah', 'jurusan', 'thnlulus');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/guru-edu [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check guru edu
        $guru_edu = $this->guru_edu_model->get_guru_edu_by_id($request['id']);
        if(is_null($guru_edu)){
            logging('error', '/api/guru-edu [PUT] - guru edu not found', $request);
            $resp->set_response(404, "failed", "guru edu not found");
            set_output($resp->get_response());
            return;
        }

        $guru = $this->guru_model->get_guru_by_id($request['idguru']);
        if(is_null($guru)){
            logging('error', '/api/guru-edu [POST] - Guru not found', $request);
            $resp->set_response(400, "failed", "Guru not found");
            set_output($resp->get_response());
            return;
        }

        $guru_edu_lvl = $this->guru_edu_lvl_model->get_guru_edu_lvl_by_id($request['idlvl']);
        if(is_null($guru_edu_lvl)){
            logging('error', '/api/guru-edu [POST] - Guru edu lvl not found', $request);
            $resp->set_response(400, "failed", "Guru edu lvl not found");
            set_output($resp->get_response());
            return;
        }

        #update guru_edu
        $flag = $this->guru_edu_model->update_guru_edu($request);
        if(empty($flag)){
            logging('error', '/api/guru-edu [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru-edu [PUT] - Update guru edu success', $request);
        $resp->set_response(200, "success", "Update guru edu success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-edu/delete/$id [DELETE]
    function delete_guru_edu($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-edu/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru_edu by id
        $guru_edu = $this->guru_edu_model->get_guru_edu_by_id($id);
        if(is_null($guru_edu)){
            logging('error', '/api/guru-edu/delete/'.$id.' [DELETE] - guru edu not found');
            $resp->set_response(404, "failed", "guru edu not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->guru_edu_model->delete_guru_edu($id);
        if(!$flag){
            logging('error', '/api/guru-edu/delete/'.$id.' [DELETE] - Delete guru_edu failed');
            $resp->set_response(404, "failed", "Delete guru_edu failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/guru-edu/delete/'.$id.' [GET] - Delete guru edu success');
        $resp->set_response(200, "success", "Delete guru edu success");
        set_output($resp->get_response());
        return;
    }
}
