<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Guru_fam extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/guru-fam/by-guru/$idguru [GET]
    function get_guru_fam_by_guru($idguru){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-fam [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get guru fam
        $guru_fam  = $this->guru_fam_model->get_guru_fam_by_idguru($idguru);
        logging('debug', '/api/guru-fam [GET] - Get guru fam is success');
        $resp->set_response(200, "success", "Get guru fam is success", $guru_fam);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-fam/by-id/$id [GET]
    function get_guru_fam_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-fam/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru fam by id
        $guru_fam = $this->guru_fam_model->get_guru_fam_by_id($id);
        if(is_null($guru_fam)){
            logging('error', '/api/guru-fam/by-id/'.$id.' [GET] - guru fam not found');
            $resp->set_response(404, "failed", "guru fam not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/guru-fam/by-id/'.$id.' [GET] - Get guru fam by id success');
        $resp->set_response(200, "success", "Get guru fam by id success", $guru_fam);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/guru-fam [POST]
    function create_guru_fam(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-fam [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('idguru', 'idfam', 'f_name', 'f_dob', 'f_ethnic', 'f_job', 'f_phone');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/guru-fam [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $guru = $this->guru_model->get_guru_by_id($request['idguru']);
        if(is_null($guru)){
            logging('error', '/api/guru-fam [POST] - Guru not found', $request);
            $resp->set_response(400, "failed", "Guru not found");
            set_output($resp->get_response());
            return;
        }

        $guru_fam_title = $this->guru_fam_title_model->get_guru_fam_title_by_id($request['idfam']);
        if(is_null($guru_fam_title)){
            logging('error', '/api/guru-fam [POST] - Guru fam title not found', $request);
            $resp->set_response(400, "failed", "Guru fam title not found");
            set_output($resp->get_response());
            return;
        }

        #create guru_fam
        $date_arr              = explode("-", $request['f_dob']);
        $request['f_yob']      = $date_arr[0];
        $request['is_deleted'] = 0;
        $flag                  = $this->guru_fam_model->create_guru_fam($request);
        
        #response
        if(!$flag){
            logging('error', '/api/guru-fam [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru-fam [POST] - Create guru fam success', $request);
        $resp->set_response(200, "success", "Create guru fam success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-fam [PUT]
    function update_guru_fam(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-fam [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id', 'idguru', 'idfam', 'f_name', 'f_dob', 'f_ethnic', 'f_job', 'f_phone');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/guru-fam [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check guru fam
        $guru_fam = $this->guru_fam_model->get_guru_fam_by_id($request['id']);
        if(is_null($guru_fam)){
            logging('error', '/api/guru-fam [PUT] - guru fam not found', $request);
            $resp->set_response(404, "failed", "guru fam not found");
            set_output($resp->get_response());
            return;
        }

        $guru = $this->guru_model->get_guru_by_id($request['idguru']);
        if(is_null($guru)){
            logging('error', '/api/guru-fam [POST] - Guru not found', $request);
            $resp->set_response(400, "failed", "Guru not found");
            set_output($resp->get_response());
            return;
        }

        $guru_fam_title = $this->guru_fam_title_model->get_guru_fam_title_by_id($request['idfam']);
        if(is_null($guru_fam_title)){
            logging('error', '/api/guru-fam [POST] - Guru fam title not found', $request);
            $resp->set_response(400, "failed", "Guru fam title not found");
            set_output($resp->get_response());
            return;
        }

        #update guru_fam
        $date_arr              = explode("-", $request['f_dob']);
        $request['f_yob']      = $date_arr[0];
        $flag = $this->guru_fam_model->update_guru_fam($request);
        if(empty($flag)){
            logging('error', '/api/guru-fam [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru-fam [PUT] - Update guru fam success', $request);
        $resp->set_response(200, "success", "Update guru fam success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru-fam/delete/$id [DELETE]
    function delete_guru_fam($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru-fam/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru_fam by id
        $guru_fam = $this->guru_fam_model->get_guru_fam_by_id($id);
        if(is_null($guru_fam)){
            logging('error', '/api/guru-fam/delete/'.$id.' [DELETE] - guru fam not found');
            $resp->set_response(404, "failed", "guru fam not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->guru_fam_model->delete_guru_fam($id);
        if(!$flag){
            logging('error', '/api/guru-fam/delete/'.$id.' [DELETE] - Delete guru fam failed');
            $resp->set_response(404, "failed", "Delete guru_fam failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/guru-fam/delete/'.$id.' [GET] - Delete guru fam success');
        $resp->set_response(200, "success", "Delete guru fam success");
        set_output($resp->get_response());
        return;
    }
}
