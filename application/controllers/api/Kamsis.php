<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Kamsis extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/kamsis/list/$id_siswa [GET]
    function get_kamsis($id_siswa){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamsis/list/$id_siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get siswa mutasi
        $kamsis  = $this->kamsis_model->get_kamsis_by_siswa($id_siswa);
        logging('debug', '/api/kamsis/list/$id_siswa [GET] - Get kamsis by siswa is success');
        $resp->set_response(200, "success", "Get kamsis by siswa is success", $kamsis);
        set_output($resp->get_response());
        return;
    }

    function create_kamsis(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamsis [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_ajaran', 'id_kamar', 'id_siswa');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kamsis [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kamsis_exist = $this->kamsis_model->get_kamsis_by_siswa_kamar_ajaran($request['id_siswa'], $request['id_kamar'], $request['id_ajaran']);
        if($kamsis_exist){
            logging('error', '/api/kamsis [POST] - Kamsis exist', $request);
            $resp->set_response(400, "failed", "kamsis exist");
            set_output($resp->get_response());
            return;
        }

        #create kamar
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                   = $this->kamsis_model->create_kamsis($request);
        if(!$flag){
            logging('error', '/api/kamsis [POST] - create kamsis failed', $request);
            $resp->set_response(500, "failed", "create kamsis failed");
            set_output($resp->get_response());
            return;
        }

        logging('error', '/api/kamsis [POST] - create kamsis success', $request);
        $resp->set_response(200, "success", "create kamsis success", $request);
        set_output($resp->get_response());
        return;
    }

    function update_kamsis(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kamsis [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_kamsis', 'id_ajaran', 'id_kamar', 'id_siswa');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kamsis [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kamsis = $this->kamsis_model->get_kamsis_by_id($request['id_kamsis']);
        if(is_null($kamsis)){
            logging('error', '/api/kamsis [PUT] - Kamsis not found', $request);
            $resp->set_response(400, "failed", "Kamsis not found");
            set_output($resp->get_response());
            return;
        }

        if($kamsis->id_siswa != $request['id_siswa'] || $kamsis->id_ajaran != $request['id_ajaran'] || $kamsis->id_kamar != $request['id_kamar']){
            $kamsis_exist = $this->kamsis_model->get_kamsis_by_siswa_kamar_ajaran($request['id_siswa'], $request['id_kamar'], $request['id_ajaran']);
            if($kamsis_exist){
                logging('error', '/api/kamsis [PUT] - Kamsis exist', $request);
                $resp->set_response(400, "failed", "kamsis exist");
                set_output($resp->get_response());
                return;
            }
        }

        #update kamar
        $request['updatedt']    = date("Y-m-d H:i:s");
        $request['updateby']    = $user->id;
        $flag                   = $this->kamsis_model->update_kamsis($request);
        if(!$flag){
            logging('error', '/api/kamsis [PUT] - update kamsis failed', $request);
            $resp->set_response(500, "failed", "update kamsis failed");
            set_output($resp->get_response());
            return;
        }

        logging('error', '/api/kamsis [PUT] - update kamsis success', $request);
        $resp->set_response(200, "success", "update kamsis success", $request);
        set_output($resp->get_response());
        return;
    }
}
?>
