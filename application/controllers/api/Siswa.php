<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Siswa extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/siswa [GET]
    function get_siswa(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $id_kelas     = $this->input->get('id_kelas');
        $id_kamar     = $this->input->get('id_kamar');
        $id_ajaran    = $this->input->get('id_ajaran');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/siswa [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get siswa
        $start = $page_number * $page_size;
        $order = array('field'=>'nama', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $siswa = $this->siswa_model->get_siswa($search, $id_kelas, $id_kamar, $id_ajaran, $order, $limit);
        $total = $this->siswa_model->count_siswa($search, $id_kelas, $id_kamar, $id_ajaran);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/siswa [GET] - Get siswa is success', $siswa);
          $resp->set_response(200, "success", "Get siswa is success", $siswa);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/siswa [GET] - Get siswa is success');
          $resp->set_response_datatable(200, $siswa, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/siswa/by-id/$id [GET]
    function get_siswa_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get siswa by id
        $siswa = $this->siswa_model->get_siswa_by_id($id);
        if(is_null($siswa)){
            logging('error', '/api/siswa/by-id/'.$id.' [GET] - siswa not found');
            $resp->set_response(404, "failed", "siswa not found");
            set_output($resp->get_response());
            return;
        }
        $siswa->full_foto = base_url($siswa->foto);

        $ortu = $this->ortu_model->get_ortu_by_id_siswa($id);
        // if(is_null($ortu)){
        //     logging('error', '/api/siswa/by-id/'.$id.' [GET] - ortu not found');
        //     $resp->set_response(404, "failed", "ortu not found");
        //     set_output($resp->get_response());
        //     return;
        // }

        $riwayat = $this->riwayatsiswa_model->get_riwayatsiswa_by_id_siswa($id);
        // if(is_null($riwayat)){
        //     logging('error', '/api/siswa/by-id/'.$id.' [GET] - riwayat not found');
        //     $resp->set_response(404, "failed", "riwayat not found");
        //     set_output($resp->get_response());
        //     return;
        // }

        $info = $this->infosiswa_model->get_infosiswa_by_id_siswa($id);
        // if(is_null($info)){
        //     logging('error', '/api/siswa/by-id/'.$id.' [GET] - info not found');
        //     $resp->set_response(404, "failed", "info not found");
        //     set_output($resp->get_response());
        //     return;
        // }

        $response = array(
            "siswa"     => $siswa,
            "ortu"      => $ortu,
            "info"      => $info,
            "riwayat"   => $riwayat
        );

        #response
        logging('debug', '/api/siswa/by-id/'.$id.' [GET] - Get siswa by id success');
        $resp->set_response(200, "success", "Get siswa by id success", $response);
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa/card [GET]
    function get_siswa_by_card_number(){
        $resp = new Response_api();

        #init variable
        $scan_type      = $this->input->get('scan_type');
        $scan_number    = $this->input->get('scan_number');
        $params         = array($scan_type, $scan_number);
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/siswa/card [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $siswa = null;
        if($scan_type == 'nis'){
            $siswa = $this->siswa_model->get_siswa_by_nis($scan_number);
        }else{
            $siswa = $this->siswa_model->get_siswa_by_card_number($scan_number);
        }

        if(is_null($siswa)){
            logging('error', '/api/siswa/card [GET] - siswa not found');
            $resp->set_response(404, "failed", "siswa not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/siswa/card [GET] - Get siswa by card number success');
        $resp->set_response(200, "success", "Get siswa by card number success", $siswa);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/siswa [POST]
    function create_siswa(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        $main_keys = array('siswa', 'ortu', 'riwayat', 'info');
        $check_res = check_parameter_by_keysV2($request, $main_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $siswa_request      = $request['siswa'];
        $ortu_request       = $request['ortu'];
        $riwayat_request    = $request['riwayat'];
        $info_request       = $request['info'];

        $siswa_keys = array('foto', 'nis', 'nisn', 'nva', 'nama', 'kelamin', 'tmplhr', 'tgllhr', 'agama', 'statdk', 'jmlsdr', 'anakke', 'alamat', 'kel', 'kec', 'kokab', 'prov', 'hp', 'email');
        $check_res = check_parameter_by_keysV2($siswa_request, $siswa_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $ortu_keys = array("ayah_nama", "ayah_pekerjaan", "ayah_hp", "ibu_nama", "ibu_pekerjaan", "ibu_hp", "penghasilan", "email", "alamat");
        $check_res = check_parameter_by_keysV2($ortu_request, $ortu_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #Riwayat is optional
        #Info is optional

        $this->db->trans_begin();

        try {
            #check duplicate
            $siswa = $this->siswa_model->get_siswa_by_nis($siswa_request['nis']);
            if($siswa){
                logging('error', '/api/siswa [POST] - NIS already registered', $siswa_request);
                $resp->set_response(400, "failed", "NIS already registered");
                set_output($resp->get_response());
                return;
            }
            $siswa = $this->siswa_model->get_siswa_by_nisn($siswa_request['nisn']);
            if($siswa){
                logging('error', '/api/siswa [POST] - NISN already registered', $siswa_request);
                $resp->set_response(400, "failed", "NISN already registered");
                set_output($resp->get_response());
                return;
            }

            #create siswa
            $siswa_request['is_active']    = 1;
            $siswa_request['createdt']     = date("Y-m-d H:i:s");
            $siswa_request['createby']     = $user->id;
            $id_siswa                      = $this->siswa_model->create_siswa($siswa_request);
            if(!$id_siswa){
                logging('error', '/api/siswa [POST] - Insert siswa failed', $siswa_request);
                $resp->set_response(400, "failed", "Insert siswa failed");
                set_output($resp->get_response());
                return;
            }

            #create ortu
            $ortu_request['id_siswa'] = $id_siswa;
            $ortu_request['createdt'] = date("Y-m-d H:i:s");
            $ortu_request['createby'] = $user->id;
            $flag                     = $this->ortu_model->create_ortu($ortu_request);
            if(!$flag){
                logging('error', '/api/siswa [POST] - Insert ortu failed', $ortu);
                $resp->set_response(400, "failed", "Insert ortu failed");
                set_output($resp->get_response());
                return;
            }

            #create riawayat siswa
            $riwayat_request['id_siswa'] = $id_siswa;
            $riwayat_request['createdt'] = date("Y-m-d H:i:s");
            $riwayat_request['createby'] = $user->id;
            $flag             = $this->riwayatsiswa_model->create_riwayatsiswa($riwayat_request);
            if(!$flag){
                logging('error', '/api/siswa [POST] - Insert riwayatsiswa failed', $riwayat_request);
                $resp->set_response(400, "failed", "Insert riwayatsiswa failed");
                set_output($resp->get_response());
                return;
            }

            #create info siswa
            $info_request['id_siswa'] = $id_siswa;
            $info_request['createdt'] = date("Y-m-d H:i:s");
            $info_request['createby'] = $user->id;
            $flag                     = $this->infosiswa_model->create_infosiswa($info_request);
            if(!$flag){
                logging('error', '/api/siswa [POST] - Insert infosiswa failed', $info_request);
                $resp->set_response(400, "failed", "Insert infosiswa failed");
                set_output($resp->get_response());
                return;
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                logging('error', '/api/siswa [POST] - Create siswa failed');
                $resp->set_response(400, "failed", "Create siswa failed");
                set_output($resp->get_response());
                return;
            }else{
                $this->db->trans_commit();
                logging('debug', '/api/siswa [POST] - Create siswa success', $request);
                $resp->set_response(200, "success", "Create siswa success", $request);
                set_output($resp->get_response());
                return;
            }
        }catch(Exception $e){
            logging('error', '/api/siswa [POST] - Create siswa failed', $e);
            $resp->set_response(400, "failed", "Create siswa failed", $e);
            set_output($resp->get_response());
            return;
        }
    }

    #path: /api/siswa [PUT]
    function update_siswa(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        $main_keys = array('siswa', 'ortu', 'riwayat', 'info');
        $check_res = check_parameter_by_keysV2($request, $main_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $siswa_request      = $request['siswa'];
        $ortu_request       = $request['ortu'];
        $riwayat_request    = $request['riwayat'];
        $info_request       = $request['info'];

        $siswa_keys = array('id_siswa', 'foto', 'nis', 'nisn', 'nva', 'nama', 'kelamin', 'tmplhr', 'tgllhr', 'agama', 'statdk', 'jmlsdr', 'anakke', 'alamat', 'kel', 'kec', 'kokab', 'prov', 'hp', 'email');
        $check_res = check_parameter_by_keysV2($siswa_request, $siswa_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [PUT] - Missing parameter. please check API documentation');
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $ortu_keys = array("ayah_nama", "ayah_pekerjaan", "ayah_hp", "ibu_nama", "ibu_pekerjaan", "ibu_hp", "penghasilan", "email", "alamat");
        $check_res = check_parameter_by_keysV2($ortu_request, $ortu_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [PUT] - Missing parameter. please check API documentation');
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $riwayat_keys = array("id_siswa");
        $check_res = check_parameter_by_keysV2($riwayat_request, $riwayat_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [PUT] - Missing parameter. please check API documentation');
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $info_keys = array("id_siswa");
        $check_res = check_parameter_by_keysV2($info_request, $info_keys);
        if(!$check_res['success']){
            logging('error', '/api/siswa [PUT] - Missing parameter. please check API documentation');
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $this->db->trans_begin();

        try {
            #check duplicate
            $siswa = $this->siswa_model->get_siswa_by_id($siswa_request['id_siswa']);
            if($siswa){
                if($siswa->nis != $siswa_request['nis']){
                    $nis_exist = $this->siswa_model->get_siswa_by_nis($siswa_request['nis']);
                    if($nis_exist){
                        logging('error', '/api/siswa [PUT] - NIS already registered', $siswa_request);
                        $resp->set_response(400, "failed", "NIS already registered");
                        set_output($resp->get_response());
                        return;
                    }
                }
                if($siswa->nisn != $siswa_request['nisn']){
                    $nisn_exist = $this->siswa_model->get_siswa_by_nisn($siswa_request['nisn']);
                    if($nisn_exist){
                        logging('error', '/api/siswa [PUT] - NISN already registered', $siswa_request);
                        $resp->set_response(400, "failed", "NISN already registered");
                        set_output($resp->get_response());
                        return;
                    }
                }
            }else{
                logging('error', '/api/siswa [PUT] - Siswa not found');
                $resp->set_response(400, "failed", "Siswa not found");
                set_output($resp->get_response());
                return;
            }

            #update siswa
            $siswa_request['is_active']    = 1;
            $siswa_request['createdt']     = date("Y-m-d H:i:s");
            $siswa_request['createby']     = $user->id;
            $id_siswa                      = $this->siswa_model->update_siswa($siswa_request);
            if(!$id_siswa){
                logging('error', '/api/siswa [PUT] - Update siswa failed', $siswa_request);
                $resp->set_response(400, "failed", "Update siswa failed");
                set_output($resp->get_response());
                return;
            }

            $flag = 0;
            if(!array_key_exists("id_ortu", $ortu_request)){#create ortu
                $ortu_request['createdt'] = date("Y-m-d H:i:s");
                $ortu_request['createby'] = $user->id;
                $flag                     = $this->ortu_model->create_ortu($ortu_request);
            }else{#update ortu
                $ortu_request['updatedt'] = date("Y-m-d H:i:s");
                $ortu_request['updateby'] = $user->id;
                $flag                     = $this->ortu_model->update_ortu($ortu_request);
            }
            
            if(!$flag){
                logging('error', '/api/siswa [PUT] - Update ortu failed', $ortu);
                $resp->set_response(400, "failed", "Update ortu failed");
                set_output($resp->get_response());
                return;
            }

            $flag = 0;
            if(!array_key_exists("id_rs", $riwayat_request)){
                $riwayat_request['createdt'] = date("Y-m-d H:i:s");
                $riwayat_request['createby'] = $user->id;
                $flag             = $this->riwayatsiswa_model->create_riwayatsiswa($riwayat_request);
            }else{#update riwayat siswa
                $riwayat_request['updatedt'] = date("Y-m-d H:i:s");
                $riwayat_request['updateby'] = $user->id;
                $flag             = $this->riwayatsiswa_model->update_riwayatsiswa($riwayat_request);
            }
            if(!$flag){
                logging('error', '/api/siswa [PUT] - Update riwayatsiswa failed', $riwayat_request);
                $resp->set_response(400, "failed", "Update riwayatsiswa failed");
                set_output($resp->get_response());
                return;
            }

            $flag = 0;
            if(!array_key_exists("id_is", $info_request)){#create info siswa
                $info_request['createdt'] = date("Y-m-d H:i:s");
                $info_request['createby'] = $user->id;
                $flag                     = $this->infosiswa_model->create_infosiswa($info_request);
            }else{#update info siswa
                $info_request['updatedt'] = date("Y-m-d H:i:s");
                $info_request['updateby'] = $user->id;
                $flag                     = $this->infosiswa_model->update_infosiswa($info_request);
            }
            
            if(!$flag){
                logging('error', '/api/siswa [PUT] - Update infosiswa failed', $info_request);
                $resp->set_response(400, "failed", "Update infosiswa failed");
                set_output($resp->get_response());
                return;
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                logging('error', '/api/siswa [PUT] - Update siswa failed');
                $resp->set_response(400, "failed", "Update siswa failed");
                set_output($resp->get_response());
                return;
            }else{
                $this->db->trans_commit();
                logging('debug', '/api/siswa [PUT] - Update siswa success', $request);
                $resp->set_response(200, "success", "Update siswa success", $request);
                set_output($resp->get_response());
                return;
            }
        }catch(Exception $e){
            logging('error', '/api/siswa [PUT] - Update siswa failed', $e);
            $resp->set_response(400, "failed", "Update siswa failed", $e);
            set_output($resp->get_response());
            return;
        }
    }

    #path: /api/siswa/inactive/$id [PUT]
    function inactive($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa/inactive/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($id);
        if(is_null($siswa)){
            logging('error', '/api/siswa/inactive/'.$id.' [PUT] - siswa not found');
            $resp->set_response(404, "failed", "siswa not found");
            set_output($resp->get_response());
            return;
        }

        #inactive siswa
        $flag = $this->siswa_model->inactive($id);
        
        #response
        if(!$flag){
            logging('error', '/api/siswa/inactive/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/siswa/inactive/'.$id.' [PUT] - inactive siswa success');
        $resp->set_response(200, "success", "inactive siswa success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa/active/$id [PUT]
    function active($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa/active/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($id);
        if(is_null($siswa)){
            logging('error', '/api/siswa/active/'.$id.' [PUT] - siswa not found');
            $resp->set_response(404, "failed", "siswa not found");
            set_output($resp->get_response());
            return;
        }

        #active siswa
        $flag = $this->siswa_model->active($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/siswa/active/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/siswa/active/'.$id.' [PUT] - active siswa success');
        $resp->set_response(200, "success", "active siswa success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa/upload [POST]
    function upload_file(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/siswa/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/siswa/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/siswa/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/siswa/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_file_url'] = base_url($data['file_url']);
        logging('debug', '/api/siswa/upload [POST] - Upload file success', $data);
        $resp_obj->set_response(200, "success", "Upload file success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}