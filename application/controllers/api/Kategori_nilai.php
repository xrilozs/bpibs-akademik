<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Kategori_nilai extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }

  #path: /api/kategori-nilai/all [GET]
  function get_kategori_nilai_all(){
    $resp = new Response_api();

    #check token
    $header      = $this->input->request_headers();
    $verify_resp = verify_user_token($header);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/api/kategori-nilai/all [GET] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #get kategori nilai
    $kategori_nilai  = $this->kategori_nilai_model->get_kategori_nilai_all();
    
    #response
    logging('debug', '/api/kategori-nilai/all [GET] - Get kategori nilai is success');
    $resp->set_response(200, "success", "Get kategori nilai is success", $kategori_nilai);
    set_output($resp->get_response());
    return;
  }
}