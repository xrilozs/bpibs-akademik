<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Card extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/siswa/card/$id_siswa [GET]
    function get_card_by_siswa($id_siswa){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/card/by-siswa/$id_siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get card
        $card  = $this->card_model->get_card_by_id_siswa($id_siswa);
        logging('debug', '/api/card/by-siswa/$id_siswa [GET] - Get card by siswa is success');
        $resp->set_response(200, "success", "Get card by siswa is success", $card);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/siswa/card [POST]
    function add_card_siswa(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/card [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_siswa', 'card_no');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/card [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        $siswa = $this->siswa_model->get_siswa_by_id($request['id_siswa']);
        if(is_null($siswa)){
            logging('error', '/api/card [POST] - Siswa not found', $request);
            $resp->set_response(400, "failed", "Siswa not found");
            set_output($resp->get_response());
            return;
        }

        $card = $this->card_model->get_card_by_id_siswa($request['id_siswa']);
        if($card){
            #inactive previous card
            $flag = $this->card_model->inactive_card($card->id);
            if(!$flag){
                logging('error', '/api/card [POST] - Update siswa card failed', $request);
                $resp->set_response(400, "failed", "Update siswa card failed");
                set_output($resp->get_response());
                return;
            }
        }

        #create card
        $request['stat'] = 1;
        $flag            = $this->card_model->create_card($request);
        
        #response
        if(!$flag){
            logging('error', '/api/card [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/card [POST] - Create card success', $request);
        $resp->set_response(200, "success", "Create card success", $request);
        set_output($resp->get_response());
        return;
    }
}
