<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Report_siswa extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/report-siswa/total [GET]
    function get_total_siswa(){
        $resp = new Response_api();

        #init variable
        $id_ajaran    = $this->input->get('id_ajaran');
        $params       = array($id_ajaran);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/report-siswa/total [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/report-siswa/total [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get siswa
        $total = $this->siswa_model->count_siswa(null, null, null, $id_ajaran);
        
        #response
        logging('debug', '/api/report-siswa/total [GET] - Get total siswa is success', $total);
        $resp->set_response(200, "success", "Get total siswa is success", $total);
        set_output($resp->get_response());
        return;
    }

    #path: /api/report-siswa/total-per-gender [GET]
    function get_total_siswa_per_gender(){
        $resp = new Response_api();

        #init variable
        $id_ajaran    = $this->input->get('id_ajaran');
        $params       = array($id_ajaran);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/report-siswa/total-per-gender [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/report-siswa/total-per-gender [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get siswa
        $total_male     = $this->siswa_model->count_siswa_by_gender($id_ajaran, "L");
        $total_female   = $this->siswa_model->count_siswa_by_gender($id_ajaran, "P");

        $data = array(
            "total_male" => $total_male,
            "total_female" => $total_female
        );
        
        #response
        logging('debug', '/api/report-siswa/total-per-gender [GET] - Get total siswa per gender is success', $data);
        $resp->set_response(200, "success", "Get total siswa per gender is success", $data);
        set_output($resp->get_response());
        return;
    }

    #path: /api/report-siswa/total-per-kelas [GET]
    function get_total_siswa_per_kelas(){
        $resp = new Response_api();

        #init variable
        $id_ajaran    = $this->input->get('id_ajaran');
        $params       = array($id_ajaran);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/report-siswa/total-per-kelas [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/report-siswa/total-per-kelas [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $data = array();
        for($i=7; $i<=12; $i++){
            $kelas_arr = $this->kelas_model->get_kelas_like_kelas(strval($i));
            if(sizeof($kelas_arr)){
                $list   = array();
                $total  = 0;
                foreach ($kelas_arr as $kelas) {
                    $total_siswa    = $this->siswa_model->count_siswa_by_kelas($id_ajaran, $kelas->id_kelas);
                    $total          += $total_siswa;
                    array_push($list, array(
                        "name"      => $kelas->kelas,
                        "total"     => $total_siswa,
                        "gender"    => $kelas->tipe
                    ));
                }
                array_push($data, array(
                    "name"  => strval($i),
                    "total" => $total,
                    "list"  => $list
                ));
            }else{
                array_push($data, array(
                    "name"  => strval($i),
                    "total" => 0,
                    "list"  => []
                ));
            }
        }
        
        #response
        logging('debug', '/api/report-siswa/total-per-kelas [GET] - Get total siswa per kelas is success', $data);
        $resp->set_response(200, "success", "Get total siswa per kelas is success", $data);
        set_output($resp->get_response());
        return;
    }

    #path: /api/report-siswa/nilai [GET]
    function get_report_nilai_siswa(){
        $resp       = new Response_api();
        $id_ajaran  = $this->input->get('id_ajaran');
        $id_semester= $this->input->get('id_semester');
        $id_siswa   = $this->input->get('id_siswa');
        $params     = array($id_ajaran, $id_semester, $id_siswa);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/report-siswa/nilai [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/report-siswa/nilai [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }
        
        $siswa = $this->siswa_model->get_siswa_by_id($id_siswa);
        if(is_null($siswa)){
            logging('error', "/api/report-siswa/nilai [GET] - siswa not found");
            $resp->set_response(400, "failed", "siswa not found");
            set_output($resp->get_response());
            return;
        }

        $nilai_siswa    = $this->nilai_model->get_report_nilai_siswa($id_ajaran, $id_semester, $id_siswa);
        $mapel_list     = $this->mapel_model->get_mapel(null, null, $id_ajaran, array("field" => "urutan", "order" => "ASC"));
        $task_list      = $this->kategori_nilai_model->get_kategori_nilai_all();

        $grouped_nilai = array();
        foreach ($nilai_siswa as $item) {
            $mapel_nama = $item->mapel_nama;
            if (!isset($grouped_nilai[$mapel_nama])) {
                $grouped_nilai[$mapel_nama] = array();
            }
            $grouped_nilai[$mapel_nama][] = $item;
        }
        
        $response = array(
            "nilai" => $grouped_nilai,
            "mapel" => $mapel_list,
            "task"  => $task_list
        );

        logging('error', '/api/report-siswa/nilai [GET] - get report nilai siswa success');
        $resp->set_response(200, "success", "get report nilai siswa success", $response);
        set_output($resp->get_response());
        return;
    }
}