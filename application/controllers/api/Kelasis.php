<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Kelasis extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/kelasis/list/$id_siswa [GET]
    function get_kelasis($id_siswa){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelasis/list/$id_siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get siswa mutasi
        $kelasis  = $this->kelasis_model->get_kelasis_by_siswa($id_siswa);
        logging('debug', '/api/kelasis/list/$id_siswa [GET] - Get kelasis by siswa is success');
        $resp->set_response(200, "success", "Get kelasis by siswa is success", $kelasis);
        set_output($resp->get_response());
        return;
    }

    function create_kelasis(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelasis [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_ajaran', 'id_kelas', 'id_siswa');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kelasis [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kelasis_exist = $this->kelasis_model->get_kelasis_by_siswa_kelas_ajaran($request['id_siswa'], $request['id_kelas'], $request['id_ajaran']);
        if($kelasis_exist){
            logging('error', '/api/kelasis [POST] - Kelasis exist', $request);
            $resp->set_response(400, "failed", "kelasis exist");
            set_output($resp->get_response());
            return;
        }

        #create kelas
        $request['createdt']    = date("Y-m-d H:i:s");
        $request['createby']    = $user->id;
        $flag                   = $this->kelasis_model->create_kelasis($request);
        if(!$flag){
            logging('error', '/api/kelasis [POST] - create kelasis failed', $request);
            $resp->set_response(500, "failed", "create kelasis failed");
            set_output($resp->get_response());
            return;
        }

        logging('error', '/api/kelasis [POST] - create kelasis success', $request);
        $resp->set_response(200, "success", "create kelasis success", $request);
        set_output($resp->get_response());
        return;
    }

    function update_kelasis(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/kelasis [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_kelasis', 'id_ajaran', 'id_kelas', 'id_siswa');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/kelasis [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $kelasis = $this->kelasis_model->get_kelasis_by_id($request['id_kelasis']);
        if(is_null($kelasis)){
            logging('error', '/api/kelasis [PUT] - Kelasis not found', $request);
            $resp->set_response(400, "failed", "Kelasis not found");
            set_output($resp->get_response());
            return;
        }

        if($kelasis->id_siswa != $request['id_siswa'] || $kelasis->id_ajaran != $request['id_ajaran'] || $kelasis->id_kelas != $request['id_kelas']){
            $kelasis_exist = $this->kelasis_model->get_kelasis_by_siswa_kelas_ajaran($request['id_siswa'], $request['id_kelas'], $request['id_ajaran']);
            if($kelasis_exist){
                logging('error', '/api/kelasis [PUT] - Kelasis exist', $request);
                $resp->set_response(400, "failed", "kelasis exist");
                set_output($resp->get_response());
                return;
            }
        }

        #update kelas
        $request['updatedt']    = date("Y-m-d H:i:s");
        $request['updateby']    = $user->id;
        $flag                   = $this->kelasis_model->update_kelasis($request);
        if(!$flag){
            logging('error', '/api/kelasis [PUT] - update kelasis failed', $request);
            $resp->set_response(500, "failed", "update kelasis failed");
            set_output($resp->get_response());
            return;
        }

        logging('error', '/api/kelasis [PUT] - update kelasis success', $request);
        $resp->set_response(200, "success", "update kelasis success", $request);
        set_output($resp->get_response());
        return;
    }
}
?>
