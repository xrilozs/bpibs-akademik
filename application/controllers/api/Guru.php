<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Guru extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/guru [GET]
    function get_guru(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/guru [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get guru
        $start = $page_number * $page_size;
        $order = array('field'=>'id', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $guru  = $this->guru_model->get_guru($search, $order, $limit);
        $total = $this->guru_model->count_guru($search);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/guru [GET] - Get guru is success');
          $resp->set_response(200, "success", "Get guru is success", $guru);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/guru [GET] - Get guru is success');
          $resp->set_response_datatable(200, $guru, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/guru/by-id/$id [GET]
    function get_guru_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru by id
        $guru = $this->guru_model->get_guru_by_id($id);
        if(is_null($guru)){
            logging('error', '/api/guru/by-id/'.$id.' [GET] - guru not found');
            $resp->set_response(404, "failed", "guru not found");
            set_output($resp->get_response());
            return;
        }

        $guru_edu = $this->guru_edu_model->get_guru_edu_by_idguru($id);
        $guru_fam = $this->guru_fam_model->get_guru_fam_by_idguru($id);

        $guru->full_foto = base_url($guru->foto);

        $response = array(
            "guru"  => $guru,
            "edu"   => $guru_edu,
            "fam"   => $guru_fam
        );

        #response
        logging('debug', '/api/guru/by-id/'.$id.' [GET] - Get guru by id success');
        $resp->set_response(200, "success", "Get guru by id success", $response);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/guru [POST]
    function create_guru(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('nid', 'npp', 'nama', 'tlhr', 'tgllhr', 'kelamin', 'email', 'foto', 'hp', 'nikah', 'tb', 'bb', 'goldar', 'bj', 'tgljoin');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/guru [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #create guru
        // $request['createdt']    = date("Y-m-d H:i:s");
        // $request['createby']    = $user->id;
        $request['is_deleted']  = 0;
        $flag                   = $this->guru_model->create_guru($request);
        
        #response
        if(!$flag){
            logging('error', '/api/guru [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru [POST] - Create guru success', $request);
        $resp->set_response(200, "success", "Create guru success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru [PUT]
    function update_guru(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header             = $this->input->request_headers();
        $verify_resp        = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id', 'nid', 'npp', 'nama', 'tlhr', 'tgllhr', 'kelamin', 'email', 'foto', 'hp', 'nikah', 'tb', 'bb', 'goldar', 'bj', 'tgljoin');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/guru [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check guru
        $guru = $this->guru_model->get_guru_by_id($request['id']);
        if(is_null($guru)){
            logging('error', '/api/guru [PUT] - guru not found', $request);
            $resp->set_response(404, "failed", "guru not found");
            set_output($resp->get_response());
            return;
        }

        #update guru
        $flag = $this->guru_model->update_guru($request);
        if(empty($flag)){
            logging('error', '/api/guru [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/guru [PUT] - Update guru success', $request);
        $resp->set_response(200, "success", "Update guru success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru/delete/$id [DELETE]
    function delete_guru($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/guru/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get guru by id
        $guru = $this->guru_model->get_guru_by_id($id);
        if(is_null($guru)){
            logging('error', '/api/guru/delete/'.$id.' [DELETE] - guru not found');
            $resp->set_response(404, "failed", "guru not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->guru_model->delete_guru($id);
        if(!$flag){
            logging('error', '/api/guru/delete/'.$id.' [DELETE] - Delete guru failed');
            $resp->set_response(404, "failed", "Delete guru failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/guru/delete/'.$id.' [GET] - Delete guru success');
        $resp->set_response(200, "success", "Delete guru success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/guru/upload [POST]
    function upload_file(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/guru/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/img/guru/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/guru/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/guru/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_file_url'] = base_url($data['file_url']);
        logging('debug', '/api/guru/upload [POST] - Upload file success', $data);
        $resp_obj->set_response(200, "success", "Upload file success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
