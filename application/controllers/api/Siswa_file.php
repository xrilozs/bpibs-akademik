<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Siswa_file extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/siswa-file/by-siswa/$id_siswa [GET]
    function get_siswa_file_by_siswa($id_siswa){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-file/by-siswa/$id_siswa [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #get siswa file
        $siswa_file  = $this->siswa_file_model->get_siswa_file_by_id_siswa($id_siswa);
        logging('debug', '/api/siswa-file/by-siswa/$id_siswa [GET] - Get siswa file by siswa is success');
        $resp->set_response(200, "success", "Get siswa file by siswa is success", $siswa_file);
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa-file/by-id/$id [GET]
    function get_siswa_file_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-file/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get siswa file by id
        $siswa_file = $this->siswa_file_model->get_siswa_file_by_id($id);
        if(is_null($siswa_file)){
            logging('error', '/api/siswa-file/by-id/'.$id.' [GET] - siswa file not found');
            $resp->set_response(404, "failed", "siswa file not found");
            set_output($resp->get_response());
            return;
        }
        $siswa_file->full_url = base_url("assets/document/$siswa_file->file_name");

        #response
        logging('debug', '/api/siswa-file/by-id/'.$id.' [GET] - Get siswa file by id success');
        $resp->set_response(200, "success", "Get siswa file by id success", $siswa_file);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/siswa-file [POST]
    function create_siswa_file(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-file [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id_file', 'id_siswa', 'file_name', 'file_size', 'file_type');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/siswa-file [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($request['id_siswa']);
        if(is_null($siswa)){
            logging('error', '/api/siswa-file [POST] - Siswa not found', $request);
            $resp->set_response(400, "failed", "Siswa not found");
            set_output($resp->get_response());
            return;
        }

        #create siswa file
        $request['created_by']  = $user->id;
        $request['created_at']  = date("Y-m-d H:i:s");
        $flag = $this->siswa_file_model->create_siswa_file($request);
        
        #response
        if(!$flag){
            logging('error', '/api/siswa-file [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/siswa-file [POST] - Create siswa file success', $request);
        $resp->set_response(200, "success", "Create siswa file success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa-file [PUT]
    function update_siswa_file(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-file [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('id', 'id_file', 'id_siswa', 'file_name', 'file_size', 'file_type');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/siswa-file [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", $check_res['message']);
            set_output($resp->get_response());
            return;
        }

        #check siswa file
        $siswa_file = $this->siswa_file_model->get_siswa_file_by_id($request['id']);
        if(is_null($siswa_file)){
            logging('error', '/api/siswa-file [PUT] - Siswa file not found', $request);
            $resp->set_response(400, "failed", "Siswa file not found");
            set_output($resp->get_response());
            return;
        }

        #check siswa
        $siswa = $this->siswa_model->get_siswa_by_id($request['id_siswa']);
        if(is_null($siswa)){
            logging('error', '/api/siswa-file [PUT] - Siswa not found', $request);
            $resp->set_response(400, "failed", "Siswa not found");
            set_output($resp->get_response());
            return;
        }

        #create siswa file
        $request['updated_by']  = $user->id;
        $request['updated_at']  = date("Y-m-d H:i:s");
        $flag = $this->siswa_file_model->update_siswa_file($request);
        
        #response
        if(!$flag){
            logging('error', '/api/siswa-file [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/siswa-file [PUT] - Update siswa file success', $request);
        $resp->set_response(200, "success", "Update siswa file success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa-file/delete/$id [DELETE]
    function delete_siswa_file($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/siswa-file/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get siswa_file by id
        $siswa_file = $this->siswa_file_model->get_siswa_file_by_id($id);
        if(is_null($siswa_file)){
            logging('error', '/api/siswa-file/delete/'.$id.' [DELETE] - siswa file not found');
            $resp->set_response(404, "failed", "siswa file not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->siswa_file_model->delete_siswa_file($id);
        if(!$flag){
            logging('error', '/api/siswa-file/delete/'.$id.' [DELETE] - Delete siswa file failed');
            $resp->set_response(404, "failed", "Delete siswa file failed");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/siswa-file/delete/'.$id.' [GET] - Delete siswa file success');
        $resp->set_response(200, "success", "Delete siswa file success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/siswa-file/upload [POST]
    function upload_file(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/siswa-file/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/document/siswa/';
        if (empty($_FILES['document']['name'])) {
            logging('error', '/api/siswa-file/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['document'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/siswa-file/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data['full_file_url']  = base_url($resp['data']['file_url']);
        $data['file_name']      = $resp['data']['file_new'];
        $data['file_size']      = $file['size'];
        $data['file_type']      = $file['type'];
        logging('debug', '/api/siswa-file/upload [POST] - Upload file success', $data);
        $resp_obj->set_response(200, "success", "Upload file success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
