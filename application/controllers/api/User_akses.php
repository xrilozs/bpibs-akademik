<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class User_akses extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/user-akses [GET]
    function get_user_akses(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-akses [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/user-akses [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get user_akses
        $start      = $page_number * $page_size;
        $order      = array('field'=>'name', 'order'=>'DESC');
        $limit      = array('start'=>$start, 'size'=>$page_size);
        $user_akses = $this->user_akses_model->get_user_aksesV2($search, $order, $limit);
        $total      = $this->user_akses_model->count_user_aksesV2($search);

        foreach ($user_akses as &$item) {
            $user_menu  = array();
            $menu_arr   = explode(',', $item->menu_ids);
            if(sizeof($menu_arr) > 0){
                $user_menu = $this->menu_model->get_menu_by_ids($menu_arr);
            }
            $item->menu = $user_menu;
            unset($item->password);
        }
        
        #response
        if(empty($draw)){
          logging('debug', '/api/user-akses [GET] - Get user akses is success', $user_akses);
          $resp->set_response(200, "success", "Get user akses is success", $user_akses);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/user-akses [GET] - Get user akses is success');
          $resp->set_response_datatable(200, $user_akses, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/user-akses/by-id/$1 [GET]
    function get_user_akses_detail($id){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-akses/by-id/$1 [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #get user akses detail
        $user_akses = $this->user_akses_model->get_user_akses_by_user_id_and_appV2($id);
        if(is_null($user_akses)){
            logging('error', "/api/user-akses/by-id/$1 [GET] - User akses not found");
            $resp->set_response(400, "failed", "User akses not found");
            set_output($resp->get_response());
            return;
        }

        $user_menu  = array();
        $menu_arr   = explode(',', $user_akses->menu_ids);
        if(sizeof($menu_arr) > 0){
            $user_menu = $this->menu_model->get_menu_by_ids($menu_arr);
        }
        $user_akses->menu = $user_menu;
        unset($user_akses->password);

        #response
        logging('debug', '/api/user-akses/by-id/$1 [GET] - Get user akses detail is success', $user_akses);
        $resp->set_response(200, "success", "Get user akses detail is success", $user_akses);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/user-akses [POST]
    function save_user_akses(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-akses [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('user_id', 'app_id', 'role_id', 'menu_id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user-akses [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $flag = 0;
        if(array_key_exists("id", $request)){
            //update
            $flag                    = $this->user_akses_model->update_user_akses($request);
        }else{
            //create
            $request['date_created'] = date("Y-m-d H:i:s");
            $flag                    = $this->user_akses_model->create_user_akses($request);
        }
        
        #response
        if(!$flag){
            logging('error', '/api/user-akses [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user-akses [POST] - Save user akses success', $request);
        $resp->set_response(200, "success", "Save user akses success", $request);
        set_output($resp->get_response());
        return;
    }
}