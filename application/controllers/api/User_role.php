<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class User_role extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }

  #path: /api/user-role [GET]
  function get_user_role(){
    $resp = new Response_api();

    #check token
    $header      = $this->input->request_headers();
    $verify_resp = verify_user_token($header);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/api/user-role [GET] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #get user role
    $search     = $this->input->get('search');
    $draw       = $this->input->get('draw');
    $user_role  = $this->user_role_model->get_user_role_all($search);
    $total      = $this->user_role_model->count_user_role_all($search);
    
    #response
    if(empty($draw)){
      logging('debug', '/api/user-role [GET] - Get user role is success', $user_role);
      $resp->set_response(200, "success", "Get user role is success", $user_role);
      set_output($resp->get_response());
      return;
    }else{
      logging('debug', '/api/user-role [GET] - Get user role is success');
      $resp->set_response_datatable(200, $user_role, $draw, $total, $total);
      set_output($resp->get_response_datatable());
      return;
    }
  }
}