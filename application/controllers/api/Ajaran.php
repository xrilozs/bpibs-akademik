<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Ajaran extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }

  #path: /api/ajaran [GET]
  function get_ajaran_all(){
    $resp = new Response_api();

    #check token
    $header      = $this->input->request_headers();
    $verify_resp = verify_user_token($header);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/api/ajaran [GET] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #get ajaran
    $ajaran  = $this->ajaran_model->get_ajaran_all();
    
    #response
    logging('debug', '/api/ajaran [GET] - Get tahun ajaran is success', $ajaran);
    $resp->set_response(200, "success", "Get tahun ajaran is success", $ajaran);
    set_output($resp->get_response());
    return;
  }
}