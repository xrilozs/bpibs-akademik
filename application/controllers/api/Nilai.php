<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Nilai extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/nilai/search [POST]
    function get_nilai_siswa(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/nilai/search [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        $keys = array('id_ajaran', 'id_semester', 'id_mapel', 'id_kelas', 'id_kat_nilai');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/nilai/search [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }
        
        $list_kelasis = $this->kelasis_model->get_kelasis_by_kelas_ajaran($request['id_kelas'], $request['id_ajaran']);
        if(sizeof($list_kelasis) <= 0){
            logging('error', "/api/nilai/search [POST] - Kelas siswa not found");
            $resp->set_response(400, "failed", "Kelas siswa not found");
            set_output($resp->get_response());
            return;
        }

        $siswa = $this->siswa_model->get_siswa(null, $request['id_kelas']);
        $query = $this->db->last_query();
        foreach ($siswa as &$item) {
            $kelasis = $this->kelasis_model->get_kelasis_by_siswa_kelas_ajaran($item->id_siswa, $request['id_kelas'], $request['id_ajaran']);
            if(is_null($kelasis)){
                logging('error', "/api/nilai/search [POST] - Kelas siswa not found");
                $resp->set_response(400, "failed", "Kelas siswa not found", $item);
                set_output($resp->get_response());
                return;
            }

            $nilai_list = $this->nilai_model->get_nilai(null, $request['id_ajaran'], $request['id_semester'], $request['id_mapel'], $request['id_kat_nilai'], $kelasis->id_kelasis, $item->id_siswa);
            if(sizeof($nilai_list) > 0){
                $nilai              = $nilai_list[0];
                $item->id_nilai     = $nilai->id;
                $item->id_kelasis   = $kelasis->id_kelasis;
                $item->kategori     = $nilai->kategori;
                $item->nilai        = $nilai->nilai;
                $item->deskripsi    = $nilai->deskripsi;
            }
        }

        logging('error', '/api/nilai/search [POST] - check nilai siswa success');
        $resp->set_response(200, "success", $query, $siswa);
        set_output($resp->get_response());
        return;
    }

    #path: /api/nilai/siswa [POST]
    function save_nilai_siswa(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/nilai/siswa [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        $keys = array('list_nilai');
        $check_param = check_parameter_by_keysV2($request, $keys);
        if(!$check_param['success']){
            logging('error', "/api/nilai/siswa [POST] - ".$check_param['message'], $request);
            $resp->set_response(400, "failed", $check_param['message']);
            set_output($resp->get_response());
            return;
        }

        $list_nilai = $request['list_nilai'];
        foreach ($list_nilai as $nilai) {
            $keys = array('id_ajaran', 'id_semester', 'id_mapel', 'id_kelasis', 'id_kat_nilai', 'id_siswa', 'nilai', 'deskripsi');

            $check_param = check_parameter_by_keysV2($nilai, $keys);
            if(!$check_param['success']){
                logging('error', "/api/nilai/siswa [POST] - ".$check_param['message'], $nilai);
                $resp->set_response(400, "failed", $check_param['message']);
                set_output($resp->get_response());
                return;
            }

            $item = array(
                'id_ajaran'     => $nilai['id_ajaran'], 
                'id_semester'   => $nilai['id_semester'], 
                'id_mapel'      => $nilai['id_mapel'], 
                'id_kelasis'    => $nilai['id_kelasis'], 
                'id_kat_nilai'  => $nilai['id_kat_nilai'], 
                'id_siswa'      => $nilai['id_siswa'], 
                'nilai'         => $nilai['nilai'], 
                'deskripsi'     => $nilai['deskripsi']
            );

            if(!isset($nilai['id_nilai'])){
                $item['createdt']    = date("Y-m-d H:i:s");
                $item['createby']    = $user->id;
                $flag = $this->nilai_model->create_nilai($item);
            }else{
                $item['id']          = $nilai['id_nilai'];
                $item['updatedt']    = date("Y-m-d H:i:s");
                $item['updateby']    = $user->id;
                $flag = $this->nilai_model->update_nilai($item);
            }
        }

        logging('error', '/api/nilai/siswa [POST] - save nilai success');
        $resp->set_response(200, "success", "save nilai success");
        set_output($resp->get_response());
        return;
    }
}