<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/dashboard.js?v=').time()
			),
			"title"		=> "Dashboard"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/index');
		$this->load->view('layout/main_footer', $data);
	}

	public function dashboard()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/dashboard.js?v=').time()
			),
			"title"		=> "Dashboard"
		);
		
		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/index');
		$this->load->view('layout/main_footer', $data);
	}

	public function profile()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/profile.js?v=').time()
			),
			"title"		=> "Profilku"
		);
		
		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/profile');
		$this->load->view('layout/main_footer', $data);
	}

	public function login()
	{
		$this->load->view('pages/login');
	}
}
