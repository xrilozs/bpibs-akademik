<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
#------- WEB ROUTES -------#
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login']     = 'common/login';
$route['dashboard'] = 'common/dashboard';
$route['profilku']  = 'common/profile';

/* User */
$route['user-login/list']           = 'user_login/user_login_list';
$route['user-login/create']         = 'user_login/user_login_create';
$route['user-login/update/(:any)']  = 'user_login/user_login_update';

$route['user-akses/list']           = 'user_akses/user_akses_list';
$route['user-akses/update/(:any)']  = 'user_akses/user_akses_update';

/* Siswa */
$route['siswa/list']           = 'siswa/siswa_list';
$route['siswa/create']         = 'siswa/siswa_create';
$route['siswa/update/(:any)']  = 'siswa/siswa_update';
$route['siswa/detail/(:any)']  = 'siswa/siswa_detail';
$route['siswa/report/(:any)']  = 'siswa/siswa_report';

/* Kelas */
$route['kelas/list']           = 'kelas/kelas_list';
$route['kelas/create']         = 'kelas/kelas_create';
$route['kelas/update/(:any)']  = 'kelas/kelas_update';

/* Kamar */
$route['kamar/list']           = 'kamar/kamar_list';
$route['kamar/create']         = 'kamar/kamar_create';
$route['kamar/update/(:any)']  = 'kamar/kamar_update';

/* Guru */
$route['guru/list']           = 'guru/guru_list';
$route['guru/create']         = 'guru/guru_create';
$route['guru/update/(:any)']  = 'guru/guru_update';
$route['guru/detail/(:any)']  = 'guru/guru_detail';

/* Mapel */
$route['mapel/list']           = 'mapel/mapel_list';
$route['mapel/create']         = 'mapel/mapel_create';
$route['mapel/update/(:any)']  = 'mapel/mapel_update';

/* Jadwal Mapel */
$route['jadwal-mapel/list']           = 'jadwal_mapel/jadwal_mapel_list';
$route['jadwal-mapel/detail/(:any)']  = 'jadwal_mapel/jadwal_mapel_detail';
$route['jadwal-mapel/create']         = 'jadwal_mapel/jadwal_mapel_create';
$route['jadwal-mapel/update/(:any)']  = 'jadwal_mapel/jadwal_mapel_update';
$route['jadwal-mapel/calendar']       = 'jadwal_mapel/jadwal_mapel_calendar';

/* Absensi */
$route['absensi']   = 'absensi/page';

/* Nilai */
$route['nilai']     = 'nilai/page';

/* Frontend */
$route['attendance/student']  = 'attendance/student';
#------- WEB ROUTES -------#

#------- API ROUTES -------#
/* User Login */
$route['api/user/login']['POST']          = 'api/user/login';
$route['api/user/refresh']['GET']         = 'api/user/refresh';
$route['api/user/change-password']['PUT'] = 'api/user/change_password';
$route['api/user']['GET']                 = 'api/user/get_user';
$route['api/user/by-id/(:any)']['GET']    = 'api/user/get_user_by_id/$1';
$route['api/user/profile']['GET']         = 'api/user/get_profile';
$route['api/user']['POST']                = 'api/user/create_user';
$route['api/user']['PUT']                 = 'api/user/update_user';
$route['api/user/active/(:any)']['PUT']   = 'api/user/active/$1';
$route['api/user/inactive/(:any)']['PUT'] = 'api/user/inactive/$1';
$route['api/user/change-password']['PUT'] = 'api/user/change_password';
$route['api/user/upload']['POST']         = 'api/user/upload_file';

/* User Akses */
$route['api/user-akses']['GET']                 = 'api/user_akses/get_user_akses';
$route['api/user-akses/by-id/(:any)']['GET']    = 'api/user_akses/get_user_akses_detail/$1';
$route['api/user-akses']['POST']                = 'api/user_akses/save_user_akses';

/* Siswa */
$route['api/siswa']['GET']                 = 'api/siswa/get_siswa';
$route['api/siswa/by-id/(:any)']['GET']    = 'api/siswa/get_siswa_by_id/$1';
$route['api/siswa']['POST']                = 'api/siswa/create_siswa';
$route['api/siswa']['PUT']                 = 'api/siswa/update_siswa';
$route['api/siswa/upload']['POST']         = 'api/siswa/upload_file';
$route['api/siswa/active/(:any)']['PUT']   = 'api/siswa/active/$1';
$route['api/siswa/inactive/(:any)']['PUT'] = 'api/siswa/inactive/$1';
$route['api/siswa/upload']['POST']         = 'api/siswa/upload_file';
$route['api/siswa/card']['GET']            = 'api/siswa/get_siswa_by_card_number';
$route['api/siswa/card']['POST']           = 'api/card/add_card_siswa';

/* Siswa Dokumen */
$route['api/siswa-file/by-siswa/(:any)']['GET']   = 'api/siswa_file/get_siswa_file_by_siswa/$1';
$route['api/siswa-file/by-id/(:any)']['GET']      = 'api/siswa_file/get_siswa_file_by_id/$1';
$route['api/siswa-file']['POST']                  = 'api/siswa_file/create_siswa_file';
$route['api/siswa-file']['PUT']                   = 'api/siswa_file/update_siswa_file';
$route['api/siswa-file/delete/(:any)']['DELETE']  = 'api/siswa_file/delete_siswa_file/$1';
$route['api/siswa-file/upload']['POST']           = 'api/siswa_file/upload_file';

/* Siswa Mutasi */
$route['api/siswa-mutasi/list/(:any)']['GET']   = 'api/siswa_mutasi/get_siswa_mutasi/$1';
$route['api/siswa-mutasi/leave']['POST']        = 'api/siswa_mutasi/siswa_mutasi_leave';
$route['api/siswa-mutasi/return']['POST']       = 'api/siswa_mutasi/siswa_mutasi_return';

/* Guru */
$route['api/guru']['GET']                    = 'api/guru/get_guru';
$route['api/guru/by-id/(:any)']['GET']       = 'api/guru/get_guru_by_id/$1';
$route['api/guru']['POST']                   = 'api/guru/create_guru';
$route['api/guru']['PUT']                    = 'api/guru/update_guru';
$route['api/guru/upload']['POST']            = 'api/guru/upload_file';
$route['api/guru/delete/(:any)']['DELETE']   = 'api/guru/delete_guru/$1';

/* Guru Edu */
$route['api/guru-edu/by-guru/(:any)']['GET']    = 'api/guru_edu/get_guru_edu_by_guru/$1';
$route['api/guru-edu/by-id/(:any)']['GET']      = 'api/guru_edu/get_guru_edu_by_id/$1';
$route['api/guru-edu']['POST']                  = 'api/guru_edu/create_guru_edu';
$route['api/guru-edu']['PUT']                   = 'api/guru_edu/update_guru_edu';
$route['api/guru-edu/delete/(:any)']['DELETE']  = 'api/guru_edu/delete_guru_edu/$1';

/* Guru Fam */
$route['api/guru-fam/by-guru/(:any)']['GET']    = 'api/guru_fam/get_guru_fam_by_guru/$1';
$route['api/guru-fam/by-id/(:any)']['GET']      = 'api/guru_fam/get_guru_fam_by_id/$1';
$route['api/guru-fam']['POST']                  = 'api/guru_fam/create_guru_fam';
$route['api/guru-fam']['PUT']                   = 'api/guru_fam/update_guru_fam';
$route['api/guru-fam/delete/(:any)']['DELETE']  = 'api/guru_fam/delete_guru_fam/$1';

/* Kelas */
$route['api/kelas']['GET']                 = 'api/kelas/get_kelas';
$route['api/kelas/all']['GET']             = 'api/kelas/get_kelas_all';
$route['api/kelas/by-id/(:any)']['GET']    = 'api/kelas/get_kelas_by_id/$1';
$route['api/kelas']['POST']                = 'api/kelas/create_kelas';
$route['api/kelas']['PUT']                 = 'api/kelas/update_kelas';

/* Kelas siswa */
$route['api/kelasis/list/(:any)']['GET']    = 'api/kelasis/get_kelasis/$1';
$route['api/kelasis']['POST']               = 'api/kelasis/create_kelasis';
$route['api/kelasis']['PUT']                = 'api/kelasis/update_kelasis';

/* Kamar */
$route['api/kamar']['GET']                 = 'api/kamar/get_kamar';
$route['api/kamar/all']['GET']             = 'api/kamar/get_kamar_all';
$route['api/kamar/by-id/(:any)']['GET']    = 'api/kamar/get_kamar_by_id/$1';
$route['api/kamar']['POST']                = 'api/kamar/create_kamar';
$route['api/kamar']['PUT']                 = 'api/kamar/update_kamar';

/* Kamar siswa */
$route['api/kamsis/list/(:any)']['GET'] = 'api/kamsis/get_kamsis/$1';
$route['api/kamsis']['POST']            = 'api/kamsis/create_kamsis';
$route['api/kamsis']['PUT']             = 'api/kamsis/update_kamsis';

/* Mapel */
$route['api/mapel']['GET']                  = 'api/mapel/get_mapel';
$route['api/mapel/by-id/(:any)']['GET']     = 'api/mapel/get_mapel_by_id/$1';
$route['api/mapel']['POST']                 = 'api/mapel/create_mapel';
$route['api/mapel']['PUT']                  = 'api/mapel/update_mapel';
$route['api/mapel/delete/(:any)']['DELETE'] = 'api/mapel/delete_mapel/$1';
$route['api/mapel/all']['GET']              = 'api/mapel/get_mapel_all';

/* Jadwal Mapel */
$route['api/jadwal-mapel']['GET']                 = 'api/jadwal_mapel/get_jadwal_mapel';
$route['api/jadwal-mapel/calendar']['GET']        = 'api/jadwal_mapel/get_jadwal_mapel_calendar';
$route['api/jadwal-mapel/by-id/(:any)']['GET']    = 'api/jadwal_mapel/get_jadwal_mapel_by_id/$1';
$route['api/jadwal-mapel']['POST']                = 'api/jadwal_mapel/create_jadwal_mapel';
$route['api/jadwal-mapel']['PUT']                 = 'api/jadwal_mapel/update_jadwal_mapel';
$route['api/jadwal-mapel/delete/(:any)']['DELETE']= 'api/jadwal_mapel/delete_jadwal_mapel/$1';

/* Absensi */
$route['api/absensi/search']['POST']    = 'api/absensi/get_jadwal_mapel';
$route['api/absensi/siswa']['POST']     = 'api/absensi/save_absensi_siswa';

/* Nilai */
$route['api/nilai/search']['POST']    = 'api/nilai/get_nilai_siswa';
$route['api/nilai/siswa']['POST']     = 'api/nilai/save_nilai_siswa';

/* Report Siswa */
$route['api/report-siswa/total']['GET']             = 'api/report_siswa/get_total_siswa';
$route['api/report-siswa/total-per-gender']['GET']  = 'api/report_siswa/get_total_siswa_per_gender';
$route['api/report-siswa/total-per-kelas']['GET']   = 'api/report_siswa/get_total_siswa_per_kelas';
$route['api/report-siswa/nilai']['GET']             = 'api/report_siswa/get_report_nilai_siswa';

/* User Role */
$route['api/user-role']['GET']              = 'api/user_role/get_user_role';
/* Ajaran */
$route['api/ajaran/all']['GET']             = 'api/ajaran/get_ajaran_all';
/* Guru Fam Title */
$route['api/guru-fam-title/all']['GET']     = 'api/guru_fam_title/get_guru_fam_title_all';
/* Guru Edu Lv */
$route['api/guru-edu-lvl/all']['GET']       = 'api/guru_edu_lvl/get_guru_edu_lvl_all';
/* Kategori Mapel */
$route['api/kategori-mapel/all']['GET']     = 'api/kategori_mapel/get_kategori_mapel_all';
/* Semester */
$route['api/semester/all']['GET']           = 'api/semester/get_semester_all';
/* Semester */
$route['api/hari/all']['GET']               = 'api/hari/get_hari_all';
/* Kategori Nilai */
$route['api/kategori-nilai/all']['GET']     = 'api/kategori_nilai/get_kategori_nilai_all';
/* Menu */
$route['api/menu/all']['GET']               = 'api/menu/get_menu_all';
/* File Kat */
$route['api/file-kat/all']['GET']           = 'api/file_kat/get_file_kat_all';
#------- API ROUTES -------#

